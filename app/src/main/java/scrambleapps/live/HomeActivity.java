package scrambleapps.live;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Toast;

import com.mla.wwl.CameraHelper;
import com.mla.wwl.WWLCamcorderProfile;
import com.mla.wwl.WWLCamera;
import com.mla.wwl.WWLCameraListener;
import com.mla.wwl.WWLCameraPrivateListener;
import com.mla.wwl.WWLVars;
import com.mla.wwl.WWLView;
import com.mla.wwl.Size;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private WWLView mCameraPreview;
    private WWLCamera mWWLCamera;
    private CameraHelper mCameraHelper;

    private AppCompatButton mRecButton;
    private AppCompatButton mPhotoButton;
    private AppCompatButton mCropButton;
    private AppCompatButton mFlipCameraButton;
    private AppCompatButton mFlashCameraButton;

    private File mVideoFile;
    private File mPhotoFile;
    public static final String PARAMS_FILENAME = "cached_params";

    private final String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQ_CODE = 1;
    private boolean mHasPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Create and configure WWLCamera
        mWWLCamera = new WWLCamera(getApplicationContext());
        mWWLCamera.setListener(new MyListener());
        int activityRotation = getWindowManager().getDefaultDisplay().getRotation();
        mWWLCamera.setScreenRotation(activityRotation);

        // Configure and attach WWLView
        mCameraPreview = findViewById(R.id.camera_preview);
        mWWLCamera.attachPreviewView(mCameraPreview);

        mWWLCamera.setLevelerCropMode(WWLVars.WWLLevelerCropMode.ROTATE);
//        alternateConfiguration();

        checkPermission();
    }

    private void onCreatePermissionGranted() {
        // We'll save photos and videos to the same file for simplicity.
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                .getAbsolutePath() + File.separator + "WWL");
        directory.mkdirs();
        mVideoFile = new File(directory.getAbsolutePath(), "video.mp4");
        mPhotoFile = new File(directory.getAbsolutePath(), "photo.jpeg");

        // Select camera resolution
        File cachedCameraParams = new File(getFilesDir(), PARAMS_FILENAME);
        try {
            mCameraHelper = new CameraHelper(cachedCameraParams);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Camera could not be opened", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        int facing = mCameraHelper.hasCamera(Camera.CameraInfo.CAMERA_FACING_BACK) ?
                Camera.CameraInfo.CAMERA_FACING_BACK : Camera.CameraInfo.CAMERA_FACING_FRONT;
        Size[] sizes = mCameraHelper.getDefaultVideoAndPhotoSize(facing);
        mWWLCamera.setCamera(facing, sizes[0], sizes[1]);

        // Initialize UI
        initializeButtons();
    }

    private void alternateConfiguration() {
        mWWLCamera.setWatermarkEnabled(true);
        mWWLCamera.setOutputMovieSize(new Size(640, 480));
        mWWLCamera.setCameraMode(WWLVars.CameraMode.VIDEO);

        mCameraPreview.setViewType(WWLView.ViewType.LEVELED);
        mCameraPreview.setFillMode(WWLView.FillMode.ASPECT_FIT);
        mCameraPreview.setDoubleTapToChangeFillMode(false);
    }

    private void initializeButtons() {
        mRecButton = findViewById(R.id.rec_button);
        mPhotoButton = findViewById(R.id.photo_button);
        mCropButton = findViewById(R.id.crop_button);
        mFlipCameraButton = findViewById(R.id.flip_camera_button);
        mFlashCameraButton = findViewById(R.id.flash_button);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(mRecButton)) {
                    if (mWWLCamera.isRecording() && !mWWLCamera.isStartingRecording() && !mWWLCamera.isStoppingRecording()) {
                        stopRecording();
                    } else if (mWWLCamera.isRunningIdle()) {
                        startRecording();
                        mRecButton.setText("Stop");
                    }
                } else if (v.equals(mPhotoButton)) {
                    // We take either a full resolution photo or a video snapshot, depensing on the
                    // the current state.
                    if (mWWLCamera.isRunningIdle()) {
                        mWWLCamera.capturePhoto(mPhotoFile);
                    }
                    if (mWWLCamera.isRecording() && !mWWLCamera.isStoppingRecording()) {
                        mWWLCamera.captureSnapshot(mPhotoFile);
                    }
                } else if (v.equals(mCropButton)) {
                    changeCropMode();
                } else if (v.equals(mFlipCameraButton)) {
                    if (mWWLCamera.isRunningIdle()) {
                        flipCamera();
                    }
                } else if (v.equals(mFlashCameraButton)) {
                    if (mWWLCamera.isRunning()) {
                        toggleFlash();
                    }
                }
            }
        };

        mRecButton.setOnClickListener(listener);
        mPhotoButton.setOnClickListener(listener);
        mCropButton.setOnClickListener(listener);
        mFlipCameraButton.setOnClickListener(listener);
        mFlashCameraButton.setOnClickListener(listener);

        if (mCameraHelper.getNumberOfCameras() == 1) {
            mFlipCameraButton.setVisibility(View.INVISIBLE);
        }

        int facing = mWWLCamera.getCameraFacing();
        if (!supportsTorchMode(mCameraHelper.getSupportedFlashModes(facing))) {
            mFlashCameraButton.setVisibility(View.INVISIBLE);
        }
    }

    private static boolean supportsTorchMode(List<String> flashModes) {
        if (flashModes == null) {
            return false;
        }
        return flashModes.contains(Camera.Parameters.FLASH_MODE_TORCH);
    }

    private void rotateUI(int degrees) {
        mRecButton.setRotation(degrees);
        mPhotoButton.setRotation(degrees);
        mCropButton.setRotation(degrees);
        mFlipCameraButton.setRotation(degrees);
        mFlashCameraButton.setRotation(degrees);
    }

    private void startRecording() {
        // We create a recording profile for the current camera resolution. We could also change some of
        // the properties, such as the bitrate and audio settings.
        Size size = mWWLCamera.getOutputMovieSize();
        WWLCamcorderProfile recordingProfile = new WWLCamcorderProfile(size.getWidth(), size.getHeight());
        mWWLCamera.startRecording(mVideoFile, recordingProfile);
    }

    private void stopRecording() {
        // We request to stop recording. The actual recording completion may take a while.
        mWWLCamera.stopRecording();
    }

    private void flipCamera() {
        int newCameraFacing = (mWWLCamera.getCameraFacing() == Camera.CameraInfo.CAMERA_FACING_BACK) ?
                Camera.CameraInfo.CAMERA_FACING_FRONT : Camera.CameraInfo.CAMERA_FACING_BACK;
        Size[] sizes = mCameraHelper.getDefaultVideoAndPhotoSize(newCameraFacing);
        // We request the other camera. This will result in closing the current camera and opening
        // the other. We will notified of the time the camera actually opened, using the callback.
        mWWLCamera.setCamera(newCameraFacing, sizes[0], sizes[1]);
    }

    private void toggleFlash() {
        String flashMode = mWWLCamera.getFlashMode();
        flashMode = (flashMode.equals(Camera.Parameters.FLASH_MODE_TORCH)) ?
                Camera.Parameters.FLASH_MODE_OFF : Camera.Parameters.FLASH_MODE_TORCH;
        mWWLCamera.setFlashMode(flashMode);

        if (flashMode.equals(Camera.Parameters.FLASH_MODE_TORCH)) {
            mFlashCameraButton.setText("Flash on");
        } else {
            mFlashCameraButton.setText("Flash off");
        }
    }

    private void changeCropMode() {
        String text;
        WWLVars.WWLLevelerCropMode mode = mWWLCamera.getLevelerCropMode();
        if (mode == WWLVars.WWLLevelerCropMode.FLEX) {
            mode = WWLVars.WWLLevelerCropMode.ROTATE;
            text = "Rotate";
        } else if (mode == WWLVars.WWLLevelerCropMode.ROTATE) {
            mode = WWLVars.WWLLevelerCropMode.LOCKED;
            text = "Locked";
        } else {
            mode = WWLVars.WWLLevelerCropMode.FLEX;
            text = "Flex";
        }
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        mWWLCamera.setLevelerCropMode(mode);
    }

    //region Permission handling

    public static String[] getNotGrantedPermissions(Context context, String[] permissions) {
        List<String> notGrandedpermissions = new ArrayList<>();
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                notGrandedpermissions.add(permission);
            }
        }
        return notGrandedpermissions.toArray(new String[notGrandedpermissions.size()]);
    }

    public static boolean shouldShowRequestPermissionRationale(Activity activity, String[] permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                return true;
            }
        }
        return false;
    }

    private void checkPermission() {
        final String[] notGrandedPermisions = getNotGrantedPermissions(this, PERMISSIONS);
        if (notGrandedPermisions.length != 0) {
            mHasPermission = false;
            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(this, notGrandedPermisions)) {
                // Show an explnation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this, notGrandedPermisions, PERMISSIONS_REQ_CODE);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, notGrandedPermisions, PERMISSIONS_REQ_CODE);
            }
        } else {
            // We've already got permission
            mHasPermission = true;
            onCreatePermissionGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSIONS_REQ_CODE) {
            boolean fail = false;
            if (grantResults.length == permissions.length) {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        fail = true;
                        break;
                    }
                }
            } else {
                fail = true;
            }
            if (!fail) {
                mHasPermission = true;
                onCreatePermissionGranted();
            }
        }
    }

    //endregion

    //region Activity Lifecycle

    @Override
    protected void onResume() {
        super.onResume();

        if (mHasPermission) {
            mWWLCamera.startRunning();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mHasPermission) {
            mWWLCamera.stopRunning();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mWWLCamera.destroy();
        mWWLCamera = null;
    }

    /* If the activity can change orientation, we need to update WWLCamera so that the preview is
     * rendered correctly.
     */
    //TODO: This callback will not be called when switching from landscape to reverseLandscape and
    // vice versa. We should poll screen rotation for changes, if we want to account for that.
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (mWWLCamera != null) {
            int screenRotation = getWindowManager().getDefaultDisplay().getRotation();
            mWWLCamera.setScreenRotation(screenRotation);
        }
    }

    //endregion

    //region WWLCamera Listener
    private class MyListener implements WWLCameraPrivateListener, WWLCameraListener {

        @Override
        public void onFailedToStart() {
            Toast.makeText(HomeActivity.this, "Camera could not open. Try again later.", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStartedRunning(Camera.Parameters parameters, int i) {

        }

        @Override
        public void onPreviewHasBeenRunning(Camera.Parameters parameters, int i) {
            if (!supportsTorchMode(CameraHelper.getSupportedFlashModes(parameters))) {
                mFlashCameraButton.setVisibility(View.INVISIBLE);
            } else {
                mFlashCameraButton.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onWillStopRunning() {

        }

        @Override
        public void onStoppedRunning() {

        }

        @Override
        public void onRecordingHasStarted() {
        }

        @Override
        public void onRecordingWillStop() {

        }

        @Override
        public void onRecordingFinished(File file, boolean success) {
            if (success) {
                Toast.makeText(HomeActivity.this, "Recording finished", Toast.LENGTH_SHORT).show();
                // Make the media visible to other apps
                MediaScannerConnection.scanFile(HomeActivity.this, new String[]{file.getAbsolutePath()}, null, null);
            } else {
                Toast.makeText(HomeActivity.this, "Error saving video", Toast.LENGTH_SHORT).show();
            }
            mRecButton.setText("Rec");
        }

        @Override
        public void onPhotoCaptured(File file, boolean success) {
            if (success) {
                Toast.makeText(HomeActivity.this, "Photo captured", Toast.LENGTH_SHORT).show();
                // Make the media visible to other apps
                MediaScannerConnection.scanFile(HomeActivity.this, new String[]{file.getAbsolutePath()}, null, null);
            } else {
                Toast.makeText(HomeActivity.this, "Error saving photo", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSnapshotCaptured(File file) {
            Toast.makeText(HomeActivity.this, "Snapshot captured", Toast.LENGTH_SHORT).show();
            // Make the media visible to other apps
            MediaScannerConnection.scanFile(HomeActivity.this, new String[]{file.getAbsolutePath()}, null, null);
        }

        @Override
        public void onAngleUpdated(float angle, float scale) {
            rotateUI((int) angle);
        }

        @Override
        public void onSensorNotResponding() {
            Toast.makeText(HomeActivity.this, "Motion sensor not responding. Try again later.", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSensorResponded() {

        }

        @Override
        public void onFrameDropped() {

        }
    }

    //endregion
}

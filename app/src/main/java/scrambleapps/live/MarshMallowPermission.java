package scrambleapps.live;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

/**
 * Created by Ankush Badlas on 09/25/18.
 */
public class MarshMallowPermission {
    @TargetApi(Build.VERSION_CODES.M)
    public static boolean haveFingerPrintPermission(Activity activity) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void requestFingerPrintPermission(Activity activity, int code) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions(new String[]{Manifest.permission.USE_FINGERPRINT},
                    code);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void cameraAllowInMarshMallow(Activity activity, int reqCode) {
        if ((ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
            activity.requestPermissions(new String[]{Manifest.permission.CAMERA}, reqCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isCameraAllowed(Activity activity) {
        return ((ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED));
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isStorageAllowed(Activity activity) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isBackgroundProcessKillAllowed(Activity activity) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.KILL_BACKGROUND_PROCESSES) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void backgroundProcessKillAllowInMarshMallow(Activity activity, int requestCode) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.KILL_BACKGROUND_PROCESSES) != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions(new String[]{Manifest.permission.KILL_BACKGROUND_PROCESSES}, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void allowExternalStorageReadWrite(Activity activity, int storagecode) {
        activity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, storagecode);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isContactsAllowed(Activity activity) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void requestContacts(Activity activity, int reqCode) {
        activity.requestPermissions(new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS}, reqCode);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isCalendarAllowed(Activity activity) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void requestCalendar(Activity activity, int reqCode) {
        activity.requestPermissions(new String[]{Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR}, reqCode);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isSendSmsAllowed(Activity activity) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void requestSendSms(Activity activity, int reqCode) {
        activity.requestPermissions(new String[]{Manifest.permission.SEND_SMS}, reqCode);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isCallPhoneAllowed(Activity activity) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void requestCallPhone(Activity activity, int reqCode) {
        activity.requestPermissions(new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, reqCode);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isAccessNotificationPolicyAllowed(Activity activity) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_NOTIFICATION_POLICY) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void requestAccessNotificationPolicy(Activity activity, int reqCode) {
        activity.requestPermissions(new String[]{Manifest.permission.ACCESS_NOTIFICATION_POLICY}, reqCode);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isGalleryAllowed(Activity activity, int gallerycode) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isAudioRecordAllowed(Activity activity, int audioCode) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void recordAudio(Activity activity, int recordAudioCode) {
        activity.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, recordAudioCode);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void readLocation(Activity activity, int code) {
        activity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                code);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isReadLocationAllowed(Activity activity, int contactcode) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isBlueToothAllowed(Activity activity, int audioCode) {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(activity, Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void requestBlueTooth(Activity activity, int blueToothCode) {
        activity.requestPermissions(new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN},
                blueToothCode);
    }
}

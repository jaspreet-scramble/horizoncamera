package scrambleapps.live;

import android.support.multidex.MultiDexApplication;

import com.mla.wwl.MlaSDK;

/**
 * Created by ankushbadlas on 23/10/18.
 */

public class ScrambleAppLive extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        MlaSDK.init(getApplicationContext());
    }
}

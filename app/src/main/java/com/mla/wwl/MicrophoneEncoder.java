package com.mla.wwl;

import android.media.AudioRecord;
import android.media.MediaCodec;
import android.util.Log;
import com.mla.wwl.av.AudioEncoderCore;
import com.mla.wwl.av.Muxer;
import java.nio.ByteBuffer;

public class MicrophoneEncoder implements Runnable {
  protected static final int SAMPLES_PER_FRAME = 1024;
  protected static final int BYTES_PER_MONO_FRAME = 2048;
  protected static final int AUDIO_FORMAT = 2;
  private int e;
  private int f;
  private final Object g = new Object();
  private boolean h;
  private boolean i;
  private boolean j;
  private AudioRecord k = null;
  private AudioEncoderCore l;
  private int m;
  private MicrophoneEncoder.a n;
  private boolean o;
  private boolean p;
  private final Object q = new Object();
  MediaCodec a;
  int b;
  int c;
  long d;

  public MicrophoneEncoder() {
    this.a();
  }

  private void a() {
    this.j = false;
    this.a = null;
    this.h = false;
    this.i = false;
    this.o = false;
  }

  public void prepareRecording(Muxer muxer, WWLCamcorderProfile profile) {
    Object var3 = this.g;
    synchronized(this.g) {
      while(this.i) {
        try {
          this.g.wait();
        } catch (InterruptedException var6) {
          var6.printStackTrace();
        }
      }

      this.a();
      this.a(muxer, profile);
    }
  }

  private void a(Muxer var1, WWLCamcorderProfile var2) {
    if(this.l != null) {
      throw new RuntimeException("Encoder wasn't released");
    } else {
      this.e = var2.audioChannels;
      this.l = new AudioEncoderCore(this.e, var2.audioBitrate, var2.audioSampleRate, var1);
      this.b();
      this.j = true;
    }
  }

  private void b() {
    int var1 = AudioRecord.getMinBufferSize(this.l.mSampleRate, this.l.mChannelConfig, 2);
    this.f = this.e * 2048;
    int var2 = this.f * 16;
    if(var2 < var1) {
      var2 = (var1 / this.f + 1) * this.f * 4;
    }

    this.m = this.f * 4;
    if(this.m > var2) {
      throw new RuntimeException("Read size can't be larger the AudioRecord's buffer size.");
    } else if(this.m > this.l.mMax_input_size) {
      throw new RuntimeException("Read size can't be larger the MediaCodec's max input size.");
    } else if(this.k != null) {
      throw new RuntimeException("AudioRecord wasn't released");
    } else {
      this.k = new AudioRecord(5, this.l.mSampleRate, this.l.mChannelConfig, 2, var2);
    }
  }

  public void startRecording() {
    this.o = true;
    this.p = false;
    this.c();
  }

  public void stopRecording() {
    this.o = false;
  }

  private void c() {
    Object var1 = this.g;
    synchronized(this.g) {
      if(this.i) {
        Log.w("MicrophoneEncoder", "Audio thread running already!");
      } else {
        Thread var2 = new Thread(this, "com.mla.wwl.MicrophoneEncoder");
        var2.setPriority(10);
        var2.start();

        while(!this.h) {
          try {
            this.g.wait();
          } catch (InterruptedException var5) {
            ;
          }
        }

      }
    }
  }

  public void run() {
    this.n = new MicrophoneEncoder.a((long)this.l.mSampleRate, (long)this.e);
    this.n.a(-17000L, 0L, 0L);
    this.k.startRecording();
    Object var1 = this.g;
    synchronized(this.g) {
      this.h = true;
      this.g.notify();
    }

    if(!this.j) {
      throw new RuntimeException("prepareEncoder() was not called before startRecording()");
    } else {
      while(this.o) {
        this.a(false);
      }

      this.a(true);
      this.k.stop();
      this.k.release();
      this.k = null;
      this.p = true;
      this.l.waitUntilReleased();
      this.l = null;
      this.j = false;
      var1 = this.g;
      synchronized(this.g) {
        this.i = this.h = false;
        this.g.notify();
      }
    }
  }

  private void a(boolean var1) {
    if(this.a == null) {
      this.a = this.l.getMediaCodec();
    }

    try {
      ByteBuffer[] var2 = this.a.getInputBuffers();
      this.b = this.a.dequeueInputBuffer(-1L);
      if(this.b >= 0) {
        ByteBuffer var3 = var2[this.b];
        var3.clear();
        this.c = this.k.read(var3, this.m);
        this.d = System.nanoTime() / 1000L;
        this.d = this.n.a(this.d, (long)this.c);
        if(this.c == -3) {
          Log.e("MicrophoneEncoder", "Audio read error: invalid operation");
        }

        if(this.c == -2) {
          Log.e("MicrophoneEncoder", "Audio read error: bad value");
        }

        if(var1) {
          this.a.queueInputBuffer(this.b, 0, this.c, this.d, 4);
        } else {
          this.a.queueInputBuffer(this.b, 0, this.c, this.d, 0);
        }
      }
    } catch (Throwable var4) {
      Log.e("MicrophoneEncoder", "_offerAudioEncoder exception");
      var4.printStackTrace();
    }

  }

  private static class a {
    private long a;
    private long b;
    private long c;
    private long d;
    private long e;
    private long f;
    private final long g = 100000L;
    private int h;
    private final long i = 10L;
    private MicrophoneEncoder.a j;

    public a(long var1, long var3) {
      this.a = var1;
      this.b = var3;
      this.a(0L, 0L, 0L);
    }

    private void a(long var1, long var3, long var5) {
      this.c = var1;
      this.d = var3;
      this.e = var5;
      this.h = 0;
      this.f = 0L;
    }

    public long a(long var1, long var3) {
      long var5 = var3 / this.b / 2L;
      long var9 = 1000000L * var5 / this.a;
      long var11 = var1 - var9;
      if(this.d == 0L) {
        this.d = var11;
      }

      long var7 = this.c + this.d + 1000000L * this.e / this.a;
      this.e += var5;
      this.b(var11, var7);
      if(var11 > var7 + 100000L) {
        ++this.h;
        if(this.h == 1) {
          this.j = new MicrophoneEncoder.a(this.a, this.b);
        }

        long var13 = var5 * 2L * this.b;
        this.j.a(var1, var13);
        if((long)this.h > 10L) {
          Log.w("MicrophoneEncoder", "Audio drifted more than " + (var11 - var7) + " us. Resetting start: " + this.j.d + " offset: " + this.j.f);
          this.a(this.j.f, this.j.d, this.j.e);
        }
      } else {
        this.h = 0;
      }

      return var7;
    }

    private void b(long var1, long var3) {
      long var5 = var1 - var3;
      this.f = Math.min(var5, this.f);
    }
  }
}
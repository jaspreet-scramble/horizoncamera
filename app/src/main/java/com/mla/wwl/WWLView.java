package com.mla.wwl;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera.Parameters;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.SurfaceView;
import android.widget.RelativeLayout;

import com.mla.wwl.Utils.CameraUtils;
import com.mla.wwl.state_machine.Event;
import com.mla.wwl.state_machine.EventHandler;
import com.mla.wwl.state_machine.WWLStateMachine;
import com.mla.wwl.state_machine.SimpleEventHandler;
import com.mla.wwl.state_machine.StateEnum;

import scrambleapps.live.R;

public class WWLView extends RelativeLayout implements com.mla.wwl.a {
    private boolean b = true;
    private boolean c = true;
    private SurfaceView d;
    private WWLPreviewSurfaceRenderer e;
    private b f;
    private boolean g;
    private boolean h;
    private boolean i;
    private ScaleGestureDetector j;
    private GestureDetector k;
    private int l;
    private int m;
    private FocusView n;
    private final int o = 400;
    WWLStateMachine a;
    private final Event<MotionEvent> p = new Event("ON_SINGLE_TAP");
    private final Event<MotionEvent> q = new Event("ON_LONG_PRESS");
    private final Event<Boolean> r = new Event("ON_AUTO_FOCUS_SUCCESSFUL");
    private final Event s = new Event("onFocusSetToDefault");
    private SimpleOnScaleGestureListener t = new SimpleOnScaleGestureListener() {
        int a = 0;
        int b = 0;

        public boolean onScale(ScaleGestureDetector detector) {
            if (WWLView.this.f != null && WWLView.this.g && WWLView.this.h) {
                float var2 = detector.getScaleFactor();
                float var3 = detector.getScaleFactor() >= 1.0F ? var2 - 1.0F : -(1.0F / var2 - 1.0F);
                this.b = (int) ((float) this.a + (float) WWLView.this.l * var3);
                this.b = Math.min(this.b, WWLView.this.l);
                this.b = Math.max(0, this.b);
                WWLView.this.m = this.b;
                WWLView.this.f.requestZoom(this.b);
                return false;
            } else {
                return false;
            }
        }

        public boolean onScaleBegin(ScaleGestureDetector detector) {
            if (WWLView.this.f != null && WWLView.this.g && WWLView.this.h) {
                this.a = WWLView.this.m;
                return true;
            } else {
                return false;
            }
        }

        public void onScaleEnd(ScaleGestureDetector detector) {
        }
    };

    public WWLView(Context context) {
        super(context);
        this.a(context);
        this.a();
    }

    public WWLView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.a(context);
        this.a();
    }

    private void a() {
        this.a = new WWLStateMachine(WWLView.FocusState.DEFAULT_FOCUS);
        this.a.addStateTransitions(new StateEnum[]{WWLView.FocusState.DEFAULT_FOCUS, WWLView.FocusState.FOCUSING, WWLView.FocusState.IN_FOCUS}, this.p, WWLView.FocusState.FOCUSING, new SimpleEventHandler<MotionEvent>() {
            @Override
            public void onSuccess(StateEnum var1, StateEnum var2, MotionEvent var3) {
                WWLView.this.a(var3, false);
            }
        });
        this.a.addStateTransitions(new StateEnum[]{WWLView.FocusState.DEFAULT_FOCUS, WWLView.FocusState.FOCUSING, WWLView.FocusState.IN_FOCUS, WWLView.FocusState.FOCUS_LOCKED}, this.q, WWLView.FocusState.FOCUSING, new SimpleEventHandler<MotionEvent>() {
            @Override
            public void onSuccess(StateEnum var1, StateEnum var2, MotionEvent var3) {
                WWLView.this.a(var3, true);
            }
        });
        this.a.addStateTransition(WWLView.FocusState.FOCUSING, this.r, null, new EventHandler<Boolean>() {
            @Override
            public StateEnum handleEvent(StateEnum var1, StateEnum var2, Boolean var3) {
                return var3.booleanValue() ? WWLView.FocusState.FOCUS_LOCKED : WWLView.FocusState.IN_FOCUS;
            }

            @Override
            public void onSuccess(StateEnum var1, StateEnum var2, Boolean var3) {
                WWLView.this.n.setCircleForSuccess(var3.booleanValue());
            }

            @Override
            public void onFail(StateEnum var1, StateEnum var2, Boolean var3) {

            }
        });
        this.a.addStateTransition(WWLView.FocusState.FOCUSING, this.s, WWLView.FocusState.DEFAULT_FOCUS, new SimpleEventHandler() {
            public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                WWLView.this.n.removeCircle(false);
            }
        });
        this.a.addStateTransitions(new StateEnum[]{WWLView.FocusState.IN_FOCUS, WWLView.FocusState.FOCUS_LOCKED}, this.s, WWLView.FocusState.DEFAULT_FOCUS, new SimpleEventHandler() {
            public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                WWLView.this.n.removeCircle(true);
            }
        });
        this.a.addStateTransition(WWLView.FocusState.FOCUS_LOCKED, this.p, WWLView.FocusState.DEFAULT_FOCUS, new SimpleEventHandler<MotionEvent>() {
            @Override
            public void onSuccess(StateEnum var1, StateEnum var2, MotionEvent var3) {
                WWLView.this.b();
                WWLView.this.n.removeCircle(true);
            }
        });
    }

    private void a(Context var1) {
        LayoutInflater var2 = (LayoutInflater) var1.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        var2.inflate(R.layout.wwl_preview_view, this, true);
        this.d = this.findViewById(R.id.wwl_preview_surface_view);
        this.n = this.findViewById(R.id.wwl_focus_view);
        this.e = new WWLPreviewSurfaceRenderer(this.d);
    }

    public void setViewType(WWLView.ViewType viewType) {
        this.e.setViewType(viewType);
    }

    public WWLView.ViewType getViewType() {
        return this.e.getViewType();
    }

    public void setEnabled(boolean enabled) {
        this.e.setEnabled(enabled);
    }

    public boolean isEnabled() {
        return this.e.isEnabled();
    }

    public void setHUDVisible(boolean visible) {
        this.e.setHUDVisible(visible);
    }

    public boolean isHUDVisible() {
        return this.e.isHUDVisible();
    }

    public void setCropRegionTintColor(int color) {
        this.e.setCropRegionTintColor(color);
    }

    public int getCropRegionTintColor() {
        return this.e.getCropRegionTintColor();
    }

    public void setFillMode(WWLView.FillMode fillMode) {
        this.e.setFillMode(fillMode);
    }

    public WWLView.FillMode getFillMode() {
        return this.e.getFillMode();
    }

    public void setDoubleTapToChangeFillMode(boolean change) {
        this.b = change;
    }

    public boolean doubleTapChangesFillMode() {
        return this.b;
    }

    public void setTapToFocus(boolean enabled) {
        this.c = enabled;
    }

    public boolean isTapToFocusEnabled() {
        return this.c;
    }

    public float[] getOutputFramePlacement() {
        return this.e.getOutputFramePlacement();
    }

    public void onViewAttached(b delegate) {
        this.f = delegate;
    }

    public void onViewDetached() {
        this.f = null;
        this.onCameraReleased();
    }

    public c getRenderer() {
        return this.e;
    }

    public void setCameraParams(Parameters params) {
        this.g = true;
        this.h = params.isZoomSupported();
        this.i = CameraUtils.isTapToFocusSupported(params);
        if (this.h) {
            this.m = 0;
            this.l = params.getMaxZoom();
            this.j = new ScaleGestureDetector(this.getContext(), this.t);
        }

        this.k = new GestureDetector(this.getContext(), new WWLView.a());
        this.a.setCurrentState(WWLView.FocusState.DEFAULT_FOCUS);
    }

    public void onCameraReleased() {
        this.g = false;
        this.h = false;
        this.i = false;
        this.n.removeCircle(false);
        this.j = null;
        this.k = null;
    }

    public void onAutoFocusSuccessful(boolean isFocusLocked) {
        this.a.processEvent(this.r, Boolean.valueOf(isFocusLocked));
    }

    public void onFocusSetToDefault() {
        this.a.processEvent(this.s);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        super.onTouchEvent(ev);
        if (this.j != null && this.j.onTouchEvent(ev)) {
        }

        if (this.k != null && this.k.onTouchEvent(ev)) {
        }

        return true;
    }

    private void a(MotionEvent var1, boolean var2) {
        float var3 = var1.getX();
        float var4 = var1.getY();
        Point var5 = new Point((int) var3, (int) var4);
        Rect var6 = new Rect();
        boolean var7 = this.a(var5, var6);
        if (var7) {
            this.f.requestTapToFocus(var6, var2);
            this.n.setCircleForInitialization(true, var3, var4);
        }
    }

    private void b() {
        this.f.requestDefaultFocusMode();
    }

    private boolean a(Point var1, Rect var2) {
        Point var3 = new Point();
        boolean var4 = this.e.mapTouchToCamera(var1, var3);
        if (!var4) {
            return false;
        } else {
            var3.x = this.a(var3.x, -800, 800);
            var3.y = this.a(var3.y, -800, 800);
            var2.left = var3.x - 200;
            var2.right = var3.x + 200;
            var2.top = var3.y - 200;
            var2.bottom = var3.y + 200;
            return true;
        }
    }

    private int a(int var1, int var2, int var3) {
        return var1 > var3 ? var3 : (var1 < var2 ? var2 : var1);
    }

    private class a extends SimpleOnGestureListener {
        private a() {
        }

        public boolean onDoubleTap(MotionEvent ev) {
            if (WWLView.this.b && WWLView.this.e.isEnabled()) {
                WWLView.FillMode var2 = WWLView.this.e.getFillMode();
                if (var2 == WWLView.FillMode.ASPECT_FIT) {
                    var2 = WWLView.FillMode.ASPECT_FILL;
                } else {
                    var2 = WWLView.FillMode.ASPECT_FIT;
                }

                WWLView.this.e.setFillMode(var2);
                return true;
            } else {
                return super.onDoubleTap(ev);
            }
        }

        public boolean onSingleTapConfirmed(MotionEvent ev) {
            if (WWLView.this.g && WWLView.this.i && WWLView.this.c && WWLView.this.e.isEnabled()) {
                WWLView.this.a.processEvent(WWLView.this.p, ev);
                return super.onSingleTapConfirmed(ev);
            } else {
                return super.onSingleTapConfirmed(ev);
            }
        }

        public void onLongPress(MotionEvent ev) {
            if (WWLView.this.g && WWLView.this.i && WWLView.this.c && WWLView.this.e.isEnabled()) {
                WWLView.this.a.processEvent(WWLView.this.q, ev);
                super.onLongPress(ev);
            } else {
                super.onLongPress(ev);
            }
        }
    }

    protected enum FocusState implements StateEnum {
        DEFAULT_FOCUS,
        FOCUSING,
        IN_FOCUS,
        FOCUS_LOCKED;

        FocusState() {
        }
    }

    public enum FillMode {
        ASPECT_FIT,
        ASPECT_FILL;

        FillMode() {
        }
    }

    public enum ViewType {
        NORMAL,
        LEVELED;

        ViewType() {
        }
    }
}

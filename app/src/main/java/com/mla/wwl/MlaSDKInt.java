package com.mla.wwl;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public class MlaSDKInt {
  public static final boolean INFO = false;
  public static final boolean DEBUG = false;
  public static final boolean VERBOSE = false;
  public static final boolean TRACE = false;
  public static boolean SHOW_DROPPED_FRAMES = false;
  public static Drawable WATERMARK_DRAWABLE;
  public static Bitmap WATERMARK_BTMP;
  public static float WATERMARK_SCALE;
  public static float WATERMARK_OPACITY;
  public static float SCALE_FILTER_FREQUENCY;

  public MlaSDKInt() {
  }
}

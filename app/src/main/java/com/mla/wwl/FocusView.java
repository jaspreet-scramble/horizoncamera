package com.mla.wwl;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.Animation.AnimationListener;

import scrambleapps.live.R;

public class FocusView extends View implements AnimationListener {
  private Handler a = new Handler();
  private boolean b = false;
  private float c;
  private float d;
  private Paint e;
  private boolean f;
  private int[] g;
  private final float h;
  private final float i;
  private int j = 0;
  private int k = 0;
  private boolean l;
  private boolean m;
  private boolean n;
  private long o = 50L;
  private AnimationSet p = new AnimationSet(true);
  private float q = 0.8F;
  private float r = 1.0F;
  private Runnable s = new Runnable() {
    public void run() {
      FocusView.this.k = FocusView.this.k == 0?1:0;
      FocusView.this.invalidate();
      FocusView.this.a.postDelayed(FocusView.this.s, FocusView.this.o);
    }
  };

  public FocusView(Context context, AttributeSet attrs) {
    super(context, attrs);
    TypedArray var3 = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MlaSDK, 0, 0);
    this.g = new int[]{var3.getColor(R.styleable.MlaSDK_wwl_focus_circle_secondary, this.getResources().getColor(R.color.wwl_almost_white)), var3.getColor(R.styleable.MlaSDK_wwl_focus_circle_primary, this.getResources().getColor(R.color.wwl_orange))};
    this.h = var3.getDimension(R.styleable.MlaSDK_wwl_focus_rect_init_stroke_width, this.getResources().getDimension(R.dimen.wwl_stroke_width_small));
    this.i = var3.getDimension(R.styleable.MlaSDK_wwl_focus_rect_success_stroke_width, this.getResources().getDimension(R.dimen.wwl_stroke_width_big));
    var3.recycle();
    this.e = new Paint();
    this.e.setStyle(Style.STROKE);
    this.e.setStrokeWidth(this.h);
    this.b = false;
    this.f = false;
    this.p.setDuration(500L);
    this.p.setFillAfter(false);
    this.p.setInterpolator(new AccelerateInterpolator());
    this.p.setAnimationListener(this);
  }

  public void setCircleForSuccess(boolean isFocusLocked) {
    this.e.setColor(this.g[1]);
    this.e.setStrokeWidth(this.i);
    this.f = true;
    this.m = isFocusLocked;
    if(this.m) {
      this.e.setAlpha((int)(this.q * 255.0F));
    } else {
      this.e.setAlpha((int)(this.r * 255.0F));
    }

    this.a.removeCallbacks(this.s);
    this.postInvalidate();
  }

  public void setCircleForInitialization(boolean hTouch, float tapX, float tapY) {
    this.b = hTouch;
    this.c = tapX;
    this.d = tapY;
    int var4 = Math.min(this.getWidth(), this.getHeight());
    this.j = (int)(0.2F * (float)var4 / 2.0F);
    this.e.setColor(this.g[0]);
    this.e.setStrokeWidth(this.h);
    this.e.setAlpha(255);
    this.f = this.n = this.l = false;
    this.k = 0;
    this.a.removeCallbacks(this.s);
    this.s.run();
  }

  public void removeCircle(boolean rmvUsingAnim) {
    this.b = false;
    this.a.removeCallbacks(this.s);
    this.l = rmvUsingAnim;
    if(rmvUsingAnim) {
      this.a(this.c, this.d);
    }

    this.postInvalidate();
  }

  public void onDraw(Canvas canvas) {
    if(this.b || this.n || this.l) {
      if(this.getAnimation() != null && !this.n) {
        this.clearAnimation();
        this.invalidate();
      } else {
        if(this.b && !this.f) {
          this.e.setColor(this.g[this.k]);
        } else if(this.l) {
          this.n = true;
          this.l = false;
          this.startAnimation(this.p);
        }

        canvas.drawCircle(this.c, this.d, (float)this.j, this.e);
      }
    }
  }

  private void a(float var1, float var2) {
    ScaleAnimation var3 = new ScaleAnimation(1.0F, 3.0F, 1.0F, 3.0F, 1, this.b(var1), 1, this.a(var2));
    AlphaAnimation var4 = new AlphaAnimation(1.0F, 0.0F);
    this.p.getAnimations().clear();
    this.p.addAnimation(var3);
    this.p.addAnimation(var4);
  }

  private float a(float var1) {
    return var1 / (float)this.getHeight();
  }

  private float b(float var1) {
    return var1 / (float)this.getWidth();
  }

  public boolean hasOverlappingRendering() {
    return false;
  }

  public void onAnimationStart(Animation animation) {
  }

  public void onAnimationEnd(Animation animation) {
    this.l = false;
    if(this.n) {
      this.n = false;
      this.postInvalidate();
    }

  }

  public void onAnimationRepeat(Animation animation) {
  }
}


package com.mla.wwl.Utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CameraUtils {
  public CameraUtils() {
  }

  public static int chooseFixedPreviewFps(Parameters parms, int desiredThousandFps) {
    List var2 = parms.getSupportedPreviewFpsRange();
    Iterator var3 = var2.iterator();

    int[] var4;
    do {
      if(!var3.hasNext()) {
        return -1;
      }

      var4 = (int[])var3.next();
    } while(var4[0] != var4[1] || var4[0] != desiredThousandFps);

    return var4[0];
  }

  public static int[] getMaxSupportedPreviewFpsRange(Parameters parms, boolean preferConstantFrameRate) {
    List var2 = parms.getSupportedPreviewFpsRange();
    ArrayList var3 = new ArrayList(10);
    Iterator var4 = var2.iterator();

    int[] var5;
    while(var4.hasNext()) {
      var5 = (int[])var4.next();
      if(var5[1] == 30000) {
        var3.add(var5);
      }
    }

    if(!var3.isEmpty()) {
      if(!preferConstantFrameRate) {
        return (int[])var3.get(0);
      } else {
        var4 = var3.iterator();

        do {
          if(!var4.hasNext()) {
            return (int[])var3.get(var3.size() - 1);
          }

          var5 = (int[])var4.next();
        } while(var5[0] != 30000);

        return var5;
      }
    } else {
      int var8 = 0;
      var5 = null;
      Iterator var6 = var2.iterator();

      while(var6.hasNext()) {
        int[] var7 = (int[])var6.next();
        if(var7[1] >= var8) {
          var8 = var7[1];
          var5 = var7;
        }
      }

      return var5;
    }
  }

  public static float getAspectForSize(int width, int height) {
    return (float)width / (float)height;
  }

  public static boolean hasFrontCamera(Context c) {
    PackageManager var1 = c.getPackageManager();
    return var1.hasSystemFeature("android.hardware.camera.front");
  }

  public static boolean hasBackCamera(Context c) {
    PackageManager var1 = c.getPackageManager();
    return var1.hasSystemFeature("android.hardware.camera");
  }

  public static boolean isValidFlashMode(List<String> flashModes, String flashMode) {
    return flashModes != null && flashModes.contains(flashMode);
  }

  public static boolean isTapToFocusSupported(Parameters params) {
    return params.getSupportedFocusModes().contains("auto");
  }

  public static int getCameraToSensorAngle(int cameraId) {
    byte var1 = 0;
    return getCameraToScreenAngle(cameraId, var1);
  }

  public static int getCameraToScreenAngle(int cameraId, int degrees) {
    int var2 = 0;
    CameraInfo var3 = new CameraInfo();
    Camera.getCameraInfo(cameraId, var3);
    if(var3.facing == 0) {
      var2 = (var3.orientation - degrees + 360) % 360;
    } else if(var3.facing == 1) {
      var2 = (var3.orientation + degrees) % 360;
      var2 = (360 - var2) % 360;
    }

    return var2;
  }
}

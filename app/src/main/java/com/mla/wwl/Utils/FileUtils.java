package com.mla.wwl.Utils;

import com.google.gson.Gson;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;

public class FileUtils {
    public FileUtils() {
    }

    public static File getSafeFile(File requestedFile) {
        File var1;
        if (!requestedFile.exists()) {
            var1 = requestedFile;
        } else {
            String var2 = requestedFile.getPath();
            String var3 = FilenameUtils.getPath(var2);
            String var4 = FilenameUtils.getBaseName(var2);
            String var5 = FilenameUtils.getExtension(var2);
            int var6 = 1;

            while (true) {
                String var7 = var4 + "-" + var6;
                String var8 = var3 + var7 + "." + var5;
                File var9 = new File(var8);
                if (!var9.exists()) {
                    var1 = var9;
                    break;
                }

                ++var6;
            }
        }

        return var1;
    }

    public static <T> T loadObjectFromFile(File file) {
        if (file != null && file.exists()) {
            FileInputStream var1 = null;
            ObjectInputStream var2 = null;
            T var3 = null;

            try {
                var1 = new FileInputStream(file);
                var2 = new ObjectInputStream(var1);
                var3 = (T) var2.readObject();
            } catch (Exception var13) {
                var13.printStackTrace();
            } finally {
                if (var2 != null) {
                    try {
                        var2.close();
                    } catch (IOException var12) {
                        var12.printStackTrace();
                    }
                }

            }

            return var3;
        } else {
            return null;
        }
    }

    public static <T> T loadObjectFromGsonFile(File file, Type type, Gson gson) {
        T var3 = null;

        try {
            var3 = gson.fromJson((String) loadObjectFromFile(file), type);
        } catch (ClassCastException var5) {
            var5.printStackTrace();
        }

        return var3;
    }

    public static <T> void saveObjectToFile(T object, File file) {
        FileOutputStream var2 = null;
        ObjectOutputStream var3 = null;

        try {
            var2 = new FileOutputStream(file);
            var3 = new ObjectOutputStream(var2);
            var3.writeObject(object);
            var3.flush();
        } catch (Exception var13) {
            var13.printStackTrace();
        } finally {
            if (var3 != null) {
                try {
                    var3.close();
                } catch (IOException var12) {
                    var12.printStackTrace();
                }
            }

        }

    }

    public static <T> void saveObjectToGsonFile(T object, File file, Gson gson) {
        saveObjectToFile(gson.toJson(object, object.getClass()), file);
    }
}

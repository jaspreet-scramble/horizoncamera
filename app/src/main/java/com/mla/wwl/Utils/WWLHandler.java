package com.mla.wwl.Utils;

import android.os.Handler;
import android.os.Looper;

public class WWLHandler {
  public WWLHandler() {
  }

  public static void postAndWait(Handler handler, Runnable r) {
    if(handler.getLooper() == Looper.myLooper()) {
      r.run();
    } else {
      WWLHandler.NotifyRunnable var2 = new WWLHandler.NotifyRunnable(r);
      synchronized(var2) {
        handler.post(var2);

        while(!var2.isFinished()) {
          try {
            var2.wait();
          } catch (InterruptedException var6) {
            ;
          }
        }
      }
    }

  }

  public abstract static class ResultRunnable<T> implements Runnable {
    public T returnValue;
    private boolean a;

    public ResultRunnable() {
    }

    public abstract void doRun();

    public final void run() {
      synchronized(this) {
        this.doRun();
        this.a = true;
        this.notifyAll();
      }
    }

    public T get() {
      synchronized(this) {
        while(!this.a) {
          try {
            this.wait();
          } catch (InterruptedException var4) {
            ;
          }
        }
      }

      return this.returnValue;
    }
  }

  public static class NotifyRunnable implements Runnable {
    private final Runnable a;
    private boolean b = false;

    public NotifyRunnable(Runnable r) {
      this.a = r;
    }

    public boolean isFinished() {
      return this.b;
    }

    public void run() {
      synchronized(this) {
        this.a.run();
        this.b = true;
        this.notifyAll();
      }
    }
  }
}
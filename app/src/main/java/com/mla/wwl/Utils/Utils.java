package com.mla.wwl.Utils;

import android.content.Context;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.TypedValue;

public class Utils {
  public Utils() {
  }

  public static int degreesFromRotation(int surfaceRotation) {
    short var1 = 0;
    switch(surfaceRotation) {
      case 0:
        var1 = 0;
        break;
      case 1:
        var1 = 90;
        break;
      case 2:
        var1 = 180;
        break;
      case 3:
        var1 = 270;
    }

    return var1;
  }

  public static boolean isGravitySensorAvailable(SensorManager sensorManager) {
    return sensorManager.getDefaultSensor(4) != null && sensorManager.getDefaultSensor(9) != null;
  }

  public static float getDensity(Context ctx) {
    return ctx.getResources().getDisplayMetrics().density;
  }

  public static float getFloatFromDimension(Context context, int id) {
    TypedValue var2 = new TypedValue();
    context.getResources().getValue(id, var2, true);
    return var2.getFloat();
  }

  public static boolean isNexus6p() {
    return Build.MODEL.equals("Nexus 5X") || Build.MODEL.equals("Nexus 6P");
  }

  public static boolean isNexus5() {
    return Build.MODEL.equals("Nexus 5");
  }

  public static boolean isAmazonKindle() {
    return Build.MANUFACTURER.equals("Amazon");
  }

  public static boolean isKindleFireHD67() {
    return Build.MANUFACTURER.equals("Amazon") && (Build.MODEL.equals("KFASWI") || Build.MODEL.equals("KFARWI"));
  }

  public static boolean isKitKat() {
    return VERSION.SDK_INT >= 19;
  }

  public static boolean isLollipop() {
    return VERSION.SDK_INT >= 21;
  }
}

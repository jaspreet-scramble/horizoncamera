package com.mla.wwl.Utils;

import android.content.Context;
import android.media.ExifInterface;
import android.os.Build.VERSION;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class ExifUtils {
  public ExifUtils() {
  }

  public static void copyExifFromData(byte[] data, ExifInterface newExif, Context context) {
    FileOutputStream var3 = null;
    File var4 = null;

    try {
      var4 = File.createTempFile("tempImage", "tmp", context.getCacheDir());
      var3 = new FileOutputStream(var4);
      var3.write(data);
      var3.flush();
      var3.close();
      ExifInterface var5 = new ExifInterface(var4.getAbsolutePath());
      if(var5 != newExif) {
        copyExif(var5, newExif);
      }
    } catch (IOException var14) {
      ;
    } finally {
      if(var3 != null) {
        try {
          var3.close();
        } catch (IOException var13) {
          ;
        }
      }

      if(var4 != null) {
        var4.delete();
      }

    }

  }

  public static void copyExif(ExifInterface oldExif, ExifInterface newExif) throws IOException {
    if(oldExif != null && newExif != null) {
      String[] var2 = new String[]{"FNumber", "DateTime", "ExposureTime", "Flash", "FocalLength", "GPSAltitude", "GPSAltitudeRef", "GPSDateStamp", "GPSLatitude", "GPSLatitudeRef", "GPSLongitude", "GPSLongitudeRef", "GPSProcessingMethod", "GPSTimeStamp", "Make", "Model", "WhiteBalance"};
      String[] var3;
      if(VERSION.SDK_INT >= 24) {
        var3 = new String[]{"FNumber", "ISOSpeedRatings", "DateTimeDigitized", "SubSecTime", "SubSecTimeDigitized", "SubSecTimeOriginal"};
      } else if(VERSION.SDK_INT >= 23) {
        var3 = new String[]{"FNumber", "ISOSpeedRatings", "DateTimeDigitized", "SubSecTime", "SubSecTimeDigitized", "SubSecTimeOriginal"};
      } else {
        var3 = new String[]{"FNumber", "ISOSpeedRatings"};
      }

      ArrayList var4 = new ArrayList();
      var4.addAll(Arrays.asList(var2));
      var4.addAll(Arrays.asList(var3));
      Iterator var5 = var4.iterator();

      while(var5.hasNext()) {
        String var6 = (String)var5.next();
        String var7 = oldExif.getAttribute(var6);
        if(var7 != null) {
          newExif.setAttribute(var6, var7);
        }
      }

      newExif.saveAttributes();
    }
  }
}

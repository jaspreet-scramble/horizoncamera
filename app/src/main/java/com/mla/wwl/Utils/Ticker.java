package com.mla.wwl.Utils;

import android.util.Log;
import java.util.HashMap;

public class Ticker {
  static String a = "TICKER";
  static long b = 0L;
  static HashMap<String, Long> c = new HashMap(10);

  public Ticker() {
  }

  public static void tik() {
    b = System.currentTimeMillis();
  }

  public static void tik(String tag) {
    long var1 = System.currentTimeMillis();
    c.put(tag, Long.valueOf(var1));
  }

  public static float tok() {
    long var0 = System.currentTimeMillis();
    float var2 = (float)(var0 - b) / 1000.0F;
    Log.i(a, "Tok: " + var2 + " secs passed");
    return var2;
  }

  public static float tok(String tag) {
    long var1 = System.currentTimeMillis();
    if(!c.containsKey(tag)) {
      return 0.0F;
    } else {
      long var3 = ((Long)c.get(tag)).longValue();
      float var5 = (float)(var1 - var3) / 1000.0F;
      Log.i(a, tag + ": " + var5 + " secs passed");
      return var5;
    }
  }

  public static float tok(float thresholdSec) {
    long var1 = System.currentTimeMillis();
    float var3 = (float)(var1 - b) / 1000.0F;
    if(var3 > thresholdSec) {
      Log.i(a, "Tok: " + var3 + " secs passed");
    }

    return var3;
  }

  public static float tok(String tag, float thresholdSec) {
    long var2 = System.currentTimeMillis();
    if(!c.containsKey(tag)) {
      return 0.0F;
    } else {
      long var4 = ((Long)c.get(tag)).longValue();
      float var6 = (float)(var2 - var4) / 1000.0F;
      if(var6 > thresholdSec) {
        Log.i(a, tag + ": " + var6 + " secs passed");
      }

      return var6;
    }
  }
}

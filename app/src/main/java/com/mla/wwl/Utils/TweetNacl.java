package com.mla.wwl.Utils;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

public final class TweetNacl {
  private static final byte[] a = new byte[16];
  private static final byte[] b = new byte[32];
  private static final long[] c;
  private static final long[] d;
  private static final long[] e;
  private static final long[] f;
  private static final long[] g;
  private static final long[] h;
  private static final long[] i;
  private static final long[] j;
  private static final byte[] k;
  private static final long[] l;
  private static final byte[] m;
  private static final long[] n;
  private static final SecureRandom o;

  public TweetNacl() {
  }

  private static int a(int var0, int var1) {
    return var0 << var1 | (var0 & -1) >>> 32 - var1;
  }

  private static int a(byte[] var0, int var1, int var2) {
    int var3 = var0[3 + var1] & 255;
    var3 = var3 << 8 | var0[2 + var1] & 255;
    var3 = var3 << 8 | var0[1 + var1] & 255;
    return var3 << 8 | var0[0 + var1] & 255;
  }

  private static long b(byte[] var0, int var1, int var2) {
    long var4 = 0L;

    for(int var3 = 0; var3 < 8; ++var3) {
      var4 = var4 << 8 | (long)(var0[var3 + var1] & 255);
    }

    return var4;
  }

  private static void a(byte[] var0, int var1, int var2, int var3) {
    for(int var4 = 0; var4 < 4; ++var4) {
      var0[var4 + var1] = (byte)(var3 & 255);
      var3 >>>= 8;
    }

  }

  private static void a(byte[] var0, int var1, int var2, long var3) {
    for(int var5 = 7; var5 >= 0; --var5) {
      var0[var5 + var1] = (byte)((int)(var3 & 255L));
      var3 >>>= 8;
    }

  }

  private static int a(byte[] var0, int var1, int var2, byte[] var3, int var4, int var5, int var6) {
    int var8 = 0;

    for(int var7 = 0; var7 < var6; ++var7) {
      var8 |= (var0[var7 + var1] ^ var3[var7 + var4]) & 255;
    }

    return (1 & var8 - 1 >>> 8) - 1;
  }

  private static int a(byte[] var0, int var1, int var2, byte[] var3, int var4, int var5) {
    return a((byte[])var0, var1, var2, (byte[])var3, var4, var5, 16);
  }

  public static int crypto_verify_16(byte[] x, byte[] y) {
    return a((byte[])x, 0, x.length, (byte[])y, 0, y.length);
  }

  private static int b(byte[] var0, int var1, int var2, byte[] var3, int var4, int var5) {
    return a((byte[])var0, var1, var2, (byte[])var3, var4, var5, 32);
  }

  public static int crypto_verify_32(byte[] x, byte[] y) {
    return b((byte[])x, 0, x.length, (byte[])y, 0, y.length);
  }

  private static void a(byte[] var0, byte[] var1, byte[] var2, byte[] var3, int var4) {
    int[] var5 = new int[16];
    int[] var6 = new int[16];
    int[] var7 = new int[16];
    int[] var8 = new int[4];

    int var9;
    for(var9 = 0; var9 < 4; ++var9) {
      var6[5 * var9] = a((byte[])var3, 4 * var9, 4);
      var6[1 + var9] = a((byte[])var2, 4 * var9, 4);
      var6[6 + var9] = a((byte[])var1, 4 * var9, 4);
      var6[11 + var9] = a((byte[])var2, 16 + 4 * var9, 4);
    }

    for(var9 = 0; var9 < 16; ++var9) {
      var7[var9] = var6[var9];
    }

    for(var9 = 0; var9 < 20; ++var9) {
      int var11;
      for(int var10 = 0; var10 < 4; ++var10) {
        for(var11 = 0; var11 < 4; ++var11) {
          var8[var11] = var6[(5 * var10 + 4 * var11) % 16];
        }

        var8[1] ^= a(var8[0] + var8[3], 7);
        var8[2] ^= a(var8[1] + var8[0], 9);
        var8[3] ^= a(var8[2] + var8[1], 13);
        var8[0] ^= a(var8[3] + var8[2], 18);

        for(var11 = 0; var11 < 4; ++var11) {
          var5[4 * var10 + (var10 + var11) % 4] = var8[var11];
        }
      }

      for(var11 = 0; var11 < 16; ++var11) {
        var6[var11] = var5[var11];
      }
    }

    if(var4 != 0) {
      for(var9 = 0; var9 < 16; ++var9) {
        var6[var9] += var7[var9];
      }

      for(var9 = 0; var9 < 4; ++var9) {
        var6[5 * var9] -= a((byte[])var3, 4 * var9, 4);
        var6[6 + var9] -= a((byte[])var1, 4 * var9, 4);
      }

      for(var9 = 0; var9 < 4; ++var9) {
        a(var0, 4 * var9, 4, var6[5 * var9]);
        a(var0, 16 + 4 * var9, 4, var6[6 + var9]);
      }
    } else {
      for(var9 = 0; var9 < 16; ++var9) {
        a(var0, 4 * var9, 4, var6[var9] + var7[var9]);
      }
    }

  }

  public static int crypto_core_salsa20(byte[] out, byte[] in, byte[] k, byte[] c) {
    a(out, in, k, c, 0);
    return 0;
  }

  public static int crypto_core_hsalsa20(byte[] out, byte[] in, byte[] k, byte[] c) {
    a(out, in, k, c, 1);
    return 0;
  }

  private static int a(byte[] var0, byte[] var1, long var2, byte[] var4, int var5, int var6, byte[] var7) {
    byte[] var8 = new byte[16];
    byte[] var9 = new byte[64];
    if(0L == var2) {
      return 0;
    } else {
      int var11;
      for(var11 = 0; var11 < 16; ++var11) {
        var8[var11] = 0;
      }

      for(var11 = 0; var11 < 8; ++var11) {
        var8[var11] = var4[var11 + var5];
      }

      int var12 = 0;
      int var13 = 0;

      while(var2 >= 64L) {
        crypto_core_salsa20(var9, var8, var7, k);

        for(var11 = 0; var11 < 64; ++var11) {
          var0[var11 + var12] = (byte)(((var1 != null?var1[var11 + var13]:0) ^ var9[var11]) & 255);
        }

        int var10 = 1;

        for(var11 = 8; var11 < 16; ++var11) {
          var10 += var8[var11] & 255;
          var8[var11] = (byte)(var10 & 255);
          var10 >>>= 8;
        }

        var2 -= 64L;
        var12 += 64;
        if(var1 != null) {
          var13 += 64;
        }
      }

      if(var2 != 0L) {
        crypto_core_salsa20(var9, var8, var7, k);

        for(var11 = 0; (long)var11 < var2; ++var11) {
          var0[var11 + var12] = (byte)(((var1 != null?var1[var11 + var13]:0) ^ var9[var11]) & 255);
        }
      }

      return 0;
    }
  }

  public static int crypto_stream_salsa20_xor(byte[] c, byte[] m, long b, byte[] n, byte[] k) {
    return a(c, m, b, n, 0, n.length, k);
  }

  private static int a(byte[] var0, long var1, byte[] var3, int var4, int var5, byte[] var6) {
    return a(var0, (byte[])null, var1, var3, var4, var5, var6);
  }

  public static int crypto_stream_salsa20(byte[] c, long d, byte[] n, byte[] k) {
    return a(c, d, n, 0, n.length, k);
  }

  public static int crypto_stream(byte[] c, long d, byte[] n, byte[] k) {
    byte[] var5 = new byte[32];
    crypto_core_hsalsa20(var5, n, k, k);
    return a(c, d, n, 16, n.length - 16, var5);
  }

  public static int crypto_stream_xor(byte[] c, byte[] m, long d, byte[] n, byte[] k) {
    byte[] var6 = new byte[32];
    crypto_core_hsalsa20(var6, n, k, k);
    return a(c, m, d, n, 16, n.length - 16, var6);
  }

  private static int a(byte[] var0, int var1, int var2, byte[] var3, int var4, int var5, int var6, byte[] var7) {
    TweetNacl.poly1305 var8 = new TweetNacl.poly1305(var7);
    var8.update(var3, var4, var6);
    var8.finish(var0, var1);
    return 0;
  }

  public static int crypto_onetimeauth(byte[] out, byte[] m, int n, byte[] k) {
    return a(out, 0, out.length, m, 0, m.length, n, k);
  }

  private static int b(byte[] var0, int var1, int var2, byte[] var3, int var4, int var5, int var6, byte[] var7) {
    byte[] var8 = new byte[16];
    a(var8, 0, var8.length, var3, var4, var5, var6, var7);
    return a((byte[])var0, var1, var2, (byte[])var8, 0, var8.length);
  }

  public static int crypto_onetimeauth_verify(byte[] h, byte[] m, int n, byte[] k) {
    return b(h, 0, h.length, m, 0, m.length, n, k);
  }

  public static int crypto_onetimeauth_verify(byte[] h, byte[] m, byte[] k) {
    return crypto_onetimeauth_verify(h, m, m != null?m.length:0, k);
  }

  public static int crypto_secretbox(byte[] c, byte[] m, int d, byte[] n, byte[] k) {
    if(d < 32) {
      return -1;
    } else {
      crypto_stream_xor(c, m, (long)d, n, k);
      a(c, 16, c.length - 16, c, 32, c.length - 32, d - 32, c);
      return 0;
    }
  }

  public static int crypto_secretbox_open(byte[] m, byte[] c, int d, byte[] n, byte[] k) {
    byte[] var6 = new byte[32];
    if(d < 32) {
      return -1;
    } else {
      crypto_stream(var6, 32L, n, k);
      if(b(c, 16, 16, c, 32, c.length - 32, d - 32, var6) != 0) {
        return -1;
      } else {
        crypto_stream_xor(m, c, (long)d, n, k);
        return 0;
      }
    }
  }

  private static void a(long[] var0, long[] var1) {
    for(int var2 = 0; var2 < 16; ++var2) {
      var0[var2] = var1[var2];
    }

  }

  private static void a(long[] var0, int var1, int var2) {
    for(int var3 = 0; var3 < 16; ++var3) {
      var0[var3 + var1] += 65536L;
      long var4 = var0[var3 + var1] >> 16;
      var0[(var3 + 1) * (var3 < 15?1:0) + var1] += var4 - 1L + 37L * (var4 - 1L) * (long)(var3 == 15?1:0);
      var0[var3 + var1] -= var4 << 16;
    }

  }

  private static void a(long[] var0, int var1, int var2, long[] var3, int var4, int var5, int var6) {
    long var10 = (long)(~(var6 - 1));

    for(int var7 = 0; var7 < 16; ++var7) {
      long var8 = var10 & (var0[var7 + var1] ^ var3[var7 + var4]);
      var0[var7 + var1] ^= var8;
      var3[var7 + var4] ^= var8;
    }

  }

  private static void a(byte[] var0, long[] var1, int var2, int var3) {
    long[] var7 = new long[16];
    long[] var8 = new long[16];

    int var4;
    for(var4 = 0; var4 < 16; ++var4) {
      var8[var4] = var1[var4 + var2];
    }

    a((long[])var8, 0, var8.length);
    a((long[])var8, 0, var8.length);
    a((long[])var8, 0, var8.length);

    for(int var5 = 0; var5 < 2; ++var5) {
      var7[0] = var8[0] - 65517L;

      for(var4 = 1; var4 < 15; ++var4) {
        var7[var4] = var8[var4] - 65535L - (var7[var4 - 1] >> 16 & 1L);
        var7[var4 - 1] &= 65535L;
      }

      var7[15] = var8[15] - 32767L - (var7[14] >> 16 & 1L);
      int var6 = (int)(var7[15] >> 16 & 1L);
      var7[14] &= 65535L;
      a((long[])var8, 0, var8.length, (long[])var7, 0, var7.length, 1 - var6);
    }

    for(var4 = 0; var4 < 16; ++var4) {
      var0[2 * var4] = (byte)((int)(var8[var4] & 255L));
      var0[2 * var4 + 1] = (byte)((int)(var8[var4] >> 8));
    }

  }

  private static int b(long[] var0, long[] var1) {
    byte[] var2 = new byte[32];
    byte[] var3 = new byte[32];
    a((byte[])var2, (long[])var0, 0, var0.length);
    a((byte[])var3, (long[])var1, 0, var1.length);
    return b((byte[])var2, 0, var2.length, (byte[])var3, 0, var3.length);
  }

  private static byte a(long[] var0) {
    byte[] var1 = new byte[32];
    a((byte[])var1, (long[])var0, 0, var0.length);
    return (byte)(var1[0] & 1);
  }

  private static void a(long[] var0, byte[] var1) {
    for(int var2 = 0; var2 < 16; ++var2) {
      var0[var2] = (long)(var1[2 * var2] & 255) + (long)(var1[2 * var2 + 1] << 8 & '\uffff');
    }

    var0[15] &= 32767L;
  }

  private static void a(long[] var0, int var1, int var2, long[] var3, int var4, int var5, long[] var6, int var7, int var8) {
    for(int var9 = 0; var9 < 16; ++var9) {
      var0[var9 + var1] = var3[var9 + var4] + var6[var9 + var7];
    }

  }

  private static void b(long[] var0, int var1, int var2, long[] var3, int var4, int var5, long[] var6, int var7, int var8) {
    for(int var9 = 0; var9 < 16; ++var9) {
      var0[var9 + var1] = var3[var9 + var4] - var6[var9 + var7];
    }

  }

  private static void c(long[] var0, int var1, int var2, long[] var3, int var4, int var5, long[] var6, int var7, int var8) {
    long[] var11 = new long[31];

    int var9;
    for(var9 = 0; var9 < var3.length; ++var9) {
      long var10000 = var3[var9];
    }

    for(var9 = 0; var9 < 16; ++var9) {
      for(int var10 = 0; var10 < 16; ++var10) {
        var11[var9 + var10] += var3[var9 + var4] * var6[var10 + var7];
      }
    }

    for(var9 = 0; var9 < 15; ++var9) {
      var11[var9] += 38L * var11[var9 + 16];
    }

    for(var9 = 0; var9 < 16; ++var9) {
      var0[var9 + var1] = var11[var9];
    }

    a(var0, var1, var2);
    a(var0, var1, var2);
  }

  private static void a(long[] var0, int var1, int var2, long[] var3, int var4, int var5) {
    c(var0, var1, var2, var3, var4, var5, var3, var4, var5);
  }

  private static void b(long[] var0, int var1, int var2, long[] var3, int var4, int var5) {
    long[] var6 = new long[16];

    int var7;
    for(var7 = 0; var7 < 16; ++var7) {
      var6[var7] = var3[var7 + var4];
    }

    for(var7 = 253; var7 >= 0; --var7) {
      a((long[])var6, 0, var6.length, (long[])var6, 0, var6.length);
      if(var7 != 2 && var7 != 4) {
        c(var6, 0, var6.length, var6, 0, var6.length, var3, var4, var5);
      }
    }

    for(var7 = 0; var7 < 16; ++var7) {
      var0[var7 + var1] = var6[var7];
    }

  }

  private static void c(long[] var0, long[] var1) {
    long[] var2 = new long[16];

    int var3;
    for(var3 = 0; var3 < 16; ++var3) {
      var2[var3] = var1[var3];
    }

    for(var3 = 250; var3 >= 0; --var3) {
      a((long[])var2, 0, var2.length, (long[])var2, 0, var2.length);
      if(var3 != 1) {
        c(var2, 0, var2.length, var2, 0, var2.length, var1, 0, var1.length);
      }
    }

    for(var3 = 0; var3 < 16; ++var3) {
      var0[var3] = var2[var3];
    }

  }

  public static int crypto_scalarmult(byte[] q, byte[] n, byte[] p) {
    byte[] var3 = new byte[32];
    long[] var4 = new long[80];
    long[] var7 = new long[16];
    long[] var8 = new long[16];
    long[] var9 = new long[16];
    long[] var10 = new long[16];
    long[] var11 = new long[16];
    long[] var12 = new long[16];

    int var6;
    for(var6 = 0; var6 < 31; ++var6) {
      var3[var6] = n[var6];
    }

    var3[31] = (byte)((n[31] & 127 | 64) & 255);
    var3[0] = (byte)(var3[0] & 248);
    a(var4, p);

    for(var6 = 0; var6 < 16; ++var6) {
      var8[var6] = var4[var6];
      var10[var6] = var7[var6] = var9[var6] = 0L;
    }

    var7[0] = var10[0] = 1L;

    for(var6 = 254; var6 >= 0; --var6) {
      int var5 = var3[var6 >>> 3] >>> (var6 & 7) & 1;
      a((long[])var7, 0, var7.length, (long[])var8, 0, var8.length, var5);
      a((long[])var9, 0, var9.length, (long[])var10, 0, var10.length, var5);
      a(var11, 0, var11.length, var7, 0, var7.length, var9, 0, var9.length);
      b(var7, 0, var7.length, var7, 0, var7.length, var9, 0, var9.length);
      a(var9, 0, var9.length, var8, 0, var8.length, var10, 0, var10.length);
      b(var8, 0, var8.length, var8, 0, var8.length, var10, 0, var10.length);
      a((long[])var10, 0, var10.length, (long[])var11, 0, var11.length);
      a((long[])var12, 0, var12.length, (long[])var7, 0, var7.length);
      c(var7, 0, var7.length, var9, 0, var9.length, var7, 0, var7.length);
      c(var9, 0, var9.length, var8, 0, var8.length, var11, 0, var11.length);
      a(var11, 0, var11.length, var7, 0, var7.length, var9, 0, var9.length);
      b(var7, 0, var7.length, var7, 0, var7.length, var9, 0, var9.length);
      a((long[])var8, 0, var8.length, (long[])var7, 0, var7.length);
      b(var9, 0, var9.length, var10, 0, var10.length, var12, 0, var12.length);
      c(var7, 0, var7.length, var9, 0, var9.length, e, 0, e.length);
      a(var7, 0, var7.length, var7, 0, var7.length, var10, 0, var10.length);
      c(var9, 0, var9.length, var9, 0, var9.length, var7, 0, var7.length);
      c(var7, 0, var7.length, var10, 0, var10.length, var12, 0, var12.length);
      c(var10, 0, var10.length, var8, 0, var8.length, var4, 0, var4.length);
      a((long[])var8, 0, var8.length, (long[])var11, 0, var11.length);
      a((long[])var7, 0, var7.length, (long[])var8, 0, var8.length, var5);
      a((long[])var9, 0, var9.length, (long[])var10, 0, var10.length, var5);
    }

    for(var6 = 0; var6 < 16; ++var6) {
      var4[var6 + 16] = var7[var6];
      var4[var6 + 32] = var9[var6];
      var4[var6 + 48] = var8[var6];
      var4[var6 + 64] = var10[var6];
    }

    b((long[])var4, 32, var4.length - 32, (long[])var4, 32, var4.length - 32);
    c(var4, 16, var4.length - 16, var4, 16, var4.length - 16, var4, 32, var4.length - 32);
    a((byte[])q, (long[])var4, 16, var4.length - 16);
    return 0;
  }

  public static int crypto_scalarmult_base(byte[] q, byte[] n) {
    return crypto_scalarmult(q, n, b);
  }

  public static int crypto_box_keypair(byte[] y, byte[] x) {
    randombytes(x, 32);
    return crypto_scalarmult_base(y, x);
  }

  public static int crypto_box_beforenm(byte[] k, byte[] y, byte[] x) {
    byte[] var3 = new byte[32];
    crypto_scalarmult(var3, x, y);
    return crypto_core_hsalsa20(k, a, var3, k);
  }

  public static int crypto_box_afternm(byte[] c, byte[] m, int d, byte[] n, byte[] k) {
    return crypto_secretbox(c, m, d, n, k);
  }

  public static int crypto_box_open_afternm(byte[] m, byte[] c, int d, byte[] n, byte[] k) {
    return crypto_secretbox_open(m, c, d, n, k);
  }

  public static int crypto_box(byte[] c, byte[] m, int d, byte[] n, byte[] y, byte[] x) {
    byte[] var6 = new byte[32];
    crypto_box_beforenm(var6, y, x);
    return crypto_box_afternm(c, m, d, n, var6);
  }

  public static int crypto_box_open(byte[] m, byte[] c, int d, byte[] n, byte[] y, byte[] x) {
    byte[] var6 = new byte[32];
    crypto_box_beforenm(var6, y, x);
    return crypto_box_open_afternm(m, c, d, n, var6);
  }

  private static long a(long var0, int var2) {
    return var0 >>> var2 | var0 << 64 - var2;
  }

  private static long a(long var0, long var2, long var4) {
    return var0 & var2 ^ ~var0 & var4;
  }

  private static long b(long var0, long var2, long var4) {
    return var0 & var2 ^ var0 & var4 ^ var2 & var4;
  }

  private static long a(long var0) {
    return a(var0, 28) ^ a(var0, 34) ^ a(var0, 39);
  }

  private static long b(long var0) {
    return a(var0, 14) ^ a(var0, 18) ^ a(var0, 41);
  }

  private static long c(long var0) {
    return a(var0, 1) ^ a(var0, 8) ^ var0 >>> 7;
  }

  private static long d(long var0) {
    return a(var0, 19) ^ a(var0, 61) ^ var0 >>> 6;
  }

  private static int a(byte[] var0, byte[] var1, int var2, int var3, int var4) {
    long[] var5 = new long[8];
    long[] var6 = new long[8];
    long[] var7 = new long[8];
    long[] var8 = new long[16];

    int var11;
    for(var11 = 0; var11 < 8; ++var11) {
      var5[var11] = var7[var11] = b(var0, 8 * var11, var0.length - 8 * var11);
    }

    for(int var13 = var2; var4 >= 128; var4 -= 128) {
      for(var11 = 0; var11 < 16; ++var11) {
        var8[var11] = b(var1, 8 * var11 + var13, var3 - 8 * var11);
      }

      for(var11 = 0; var11 < 80; ++var11) {
        int var12;
        for(var12 = 0; var12 < 8; ++var12) {
          var6[var12] = var7[var12];
        }

        long var9 = var7[7] + b(var7[4]) + a(var7[4], var7[5], var7[6]) + l[var11] + var8[var11 % 16];
        var6[7] = var9 + a(var7[0]) + b(var7[0], var7[1], var7[2]);
        var6[3] += var9;

        for(var12 = 0; var12 < 8; ++var12) {
          var7[(var12 + 1) % 8] = var6[var12];
        }

        if(var11 % 16 == 15) {
          for(var12 = 0; var12 < 16; ++var12) {
            var8[var12] += var8[(var12 + 9) % 16] + c(var8[(var12 + 1) % 16]) + d(var8[(var12 + 14) % 16]);
          }
        }
      }

      for(var11 = 0; var11 < 8; ++var11) {
        var7[var11] += var5[var11];
        var5[var11] = var7[var11];
      }

      var13 += 128;
    }

    for(var11 = 0; var11 < 8; ++var11) {
      a(var0, 8 * var11, var0.length - 8 * var11, var5[var11]);
    }

    return var4;
  }

  public static int crypto_hashblocks(byte[] x, byte[] m, int n) {
    return a(x, m, 0, m.length, n);
  }

  private static int b(byte[] var0, byte[] var1, int var2, int var3, int var4) {
    byte[] var5 = new byte[64];
    byte[] var6 = new byte[256];
    long var7 = (long)var4;

    int var9;
    for(var9 = 0; var9 < 64; ++var9) {
      var5[var9] = m[var9];
    }

    a(var5, var1, var2, var3, var4);
    var4 &= 127;

    for(var9 = 0; var9 < 256; ++var9) {
      var6[var9] = 0;
    }

    for(var9 = 0; var9 < var4; ++var9) {
      var6[var9] = var1[var9 + var2];
    }

    var6[var4] = -128;
    var4 = 256 - 128 * (var4 < 112?1:0);
    var6[var4 - 9] = (byte)((int)(var7 >>> 61));
    a(var6, var4 - 8, var6.length - (var4 - 8), var7 << 3);
    a(var5, var6, 0, var6.length, var4);

    for(var9 = 0; var9 < 64; ++var9) {
      var0[var9] = var5[var9];
    }

    return 0;
  }

  public static int crypto_hash(byte[] out, byte[] m, int n) {
    return b(out, m, 0, m.length, n);
  }

  public static int crypto_hash(byte[] out, byte[] m) {
    return crypto_hash(out, m, m != null?m.length:0);
  }

  private static void a(long[][] var0, long[][] var1) {
    long[] var2 = new long[16];
    long[] var3 = new long[16];
    long[] var4 = new long[16];
    long[] var5 = new long[16];
    long[] var6 = new long[16];
    long[] var7 = new long[16];
    long[] var8 = new long[16];
    long[] var9 = new long[16];
    long[] var10 = new long[16];
    long[] var11 = var0[0];
    long[] var12 = var0[1];
    long[] var13 = var0[2];
    long[] var14 = var0[3];
    long[] var15 = var1[0];
    long[] var16 = var1[1];
    long[] var17 = var1[2];
    long[] var18 = var1[3];
    b(var2, 0, var2.length, var12, 0, var12.length, var11, 0, var11.length);
    b(var6, 0, var6.length, var16, 0, var16.length, var15, 0, var15.length);
    c(var2, 0, var2.length, var2, 0, var2.length, var6, 0, var6.length);
    a(var3, 0, var3.length, var11, 0, var11.length, var12, 0, var12.length);
    a(var6, 0, var6.length, var15, 0, var15.length, var16, 0, var16.length);
    c(var3, 0, var3.length, var3, 0, var3.length, var6, 0, var6.length);
    c(var4, 0, var4.length, var14, 0, var14.length, var18, 0, var18.length);
    c(var4, 0, var4.length, var4, 0, var4.length, g, 0, g.length);
    c(var5, 0, var5.length, var13, 0, var13.length, var17, 0, var17.length);
    a(var5, 0, var5.length, var5, 0, var5.length, var5, 0, var5.length);
    b(var7, 0, var7.length, var3, 0, var3.length, var2, 0, var2.length);
    b(var8, 0, var8.length, var5, 0, var5.length, var4, 0, var4.length);
    a(var9, 0, var9.length, var5, 0, var5.length, var4, 0, var4.length);
    a(var10, 0, var10.length, var3, 0, var3.length, var2, 0, var2.length);
    c(var11, 0, var11.length, var7, 0, var7.length, var8, 0, var8.length);
    c(var12, 0, var12.length, var10, 0, var10.length, var9, 0, var9.length);
    c(var13, 0, var13.length, var9, 0, var9.length, var8, 0, var8.length);
    c(var14, 0, var14.length, var7, 0, var7.length, var10, 0, var10.length);
  }

  private static void a(long[][] var0, long[][] var1, byte var2) {
    for(int var3 = 0; var3 < 4; ++var3) {
      a((long[])var0[var3], 0, var0[var3].length, (long[])var1[var3], 0, var1[var3].length, var2);
    }

  }

  private static void a(byte[] var0, long[][] var1) {
    long[] var2 = new long[16];
    long[] var3 = new long[16];
    long[] var4 = new long[16];
    b((long[])var4, 0, var4.length, (long[])var1[2], 0, var1[2].length);
    c(var2, 0, var2.length, var1[0], 0, var1[0].length, var4, 0, var4.length);
    c(var3, 0, var3.length, var1[1], 0, var1[1].length, var4, 0, var4.length);
    a((byte[])var0, (long[])var3, 0, var3.length);
    var0[31] = (byte)(var0[31] ^ a(var2) << 7);
  }

  private static void a(long[][] var0, long[][] var1, byte[] var2, int var3, int var4) {
    a(var0[0], c);
    a(var0[1], d);
    a(var0[2], d);
    a(var0[3], c);

    for(int var5 = 255; var5 >= 0; --var5) {
      byte var6 = (byte)(var2[var5 / 8 + var3] >> (var5 & 7) & 1);
      a(var0, var1, var6);
      a(var1, var0);
      a(var0, var0);
      a(var0, var1, var6);
    }

  }

  private static void a(long[][] var0, byte[] var1, int var2, int var3) {
    long[][] var4 = new long[][]{new long[16], new long[16], new long[16], new long[16]};
    a(var4[0], h);
    a(var4[1], i);
    a(var4[2], d);
    c(var4[3], 0, var4[3].length, h, 0, h.length, i, 0, i.length);
    a(var0, var4, var1, var2, var3);
  }

  public static int crypto_sign_keypair(byte[] pk, byte[] sk, boolean seeded) {
    byte[] var3 = new byte[64];
    long[][] var4 = new long[][]{new long[16], new long[16], new long[16], new long[16]};
    if(!seeded) {
      randombytes(sk, 32);
    }

    b(var3, sk, 0, sk.length, 32);
    var3[0] = (byte)(var3[0] & 248);
    var3[31] = (byte)(var3[31] & 127);
    var3[31] = (byte)(var3[31] | 64);
    a((long[][])var4, (byte[])var3, 0, var3.length);
    a(pk, var4);

    for(int var5 = 0; var5 < 32; ++var5) {
      sk[var5 + 32] = pk[var5];
    }

    return 0;
  }

  private static void a(byte[] var0, int var1, int var2, long[] var3) {
    long var4;
    int var6;
    int var7;
    for(var6 = 63; var6 >= 32; --var6) {
      var4 = 0L;

      for(var7 = var6 - 32; var7 < var6 - 12; ++var7) {
        var3[var7] += var4 - 16L * var3[var6] * n[var7 - (var6 - 32)];
        var4 = var3[var7] + 128L >> 8;
        var3[var7] -= var4 << 8;
      }

      var3[var7] += var4;
      var3[var6] = 0L;
    }

    var4 = 0L;

    for(var7 = 0; var7 < 32; ++var7) {
      var3[var7] += var4 - (var3[31] >> 4) * n[var7];
      var4 = var3[var7] >> 8;
      var3[var7] &= 255L;
    }

    for(var7 = 0; var7 < 32; ++var7) {
      var3[var7] -= var4 * n[var7];
    }

    for(var6 = 0; var6 < 32; ++var6) {
      var3[var6 + 1] += var3[var6] >> 8;
      var0[var6 + var1] = (byte)((int)(var3[var6] & 255L));
    }

  }

  private static void a(byte[] var0) {
    long[] var1 = new long[64];

    int var2;
    for(var2 = 0; var2 < 64; ++var2) {
      var1[var2] = (long)(var0[var2] & 255);
    }

    for(var2 = 0; var2 < 64; ++var2) {
      var0[var2] = 0;
    }

    a(var0, 0, var0.length, var1);
  }

  public static int crypto_sign(byte[] sm, long dummy, byte[] m, int n, byte[] sk) {
    byte[] var6 = new byte[64];
    byte[] var7 = new byte[64];
    byte[] var8 = new byte[64];
    long[] var11 = new long[64];
    long[][] var12 = new long[][]{new long[16], new long[16], new long[16], new long[16]};
    b(var6, sk, 0, sk.length, 32);
    var6[0] = (byte)(var6[0] & 248);
    var6[31] = (byte)(var6[31] & 127);
    var6[31] = (byte)(var6[31] | 64);

    int var9;
    for(var9 = 0; var9 < n; ++var9) {
      sm[64 + var9] = m[var9];
    }

    for(var9 = 0; var9 < 32; ++var9) {
      sm[32 + var9] = var6[32 + var9];
    }

    b(var8, sm, 32, sm.length - 32, n + 32);
    a(var8);
    a((long[][])var12, (byte[])var8, 0, var8.length);
    a(sm, var12);

    for(var9 = 0; var9 < 32; ++var9) {
      sm[var9 + 32] = sk[var9 + 32];
    }

    b(var7, sm, 0, sm.length, n + 64);
    a(var7);

    for(var9 = 0; var9 < 64; ++var9) {
      var11[var9] = 0L;
    }

    for(var9 = 0; var9 < 32; ++var9) {
      var11[var9] = (long)(var8[var9] & 255);
    }

    for(var9 = 0; var9 < 32; ++var9) {
      for(int var10 = 0; var10 < 32; ++var10) {
        var11[var9 + var10] += (long)(var7[var9] & 255) * (long)(var6[var10] & 255);
      }
    }

    a(sm, 32, sm.length - 32, var11);
    return 0;
  }

  private static int a(long[][] var0, byte[] var1) {
    long[] var2 = new long[16];
    long[] var3 = new long[16];
    long[] var4 = new long[16];
    long[] var5 = new long[16];
    long[] var6 = new long[16];
    long[] var7 = new long[16];
    long[] var8 = new long[16];
    a(var0[2], d);
    a(var0[1], var1);
    a((long[])var4, 0, var4.length, (long[])var0[1], 0, var0[1].length);
    c(var5, 0, var5.length, var4, 0, var4.length, f, 0, f.length);
    b(var4, 0, var4.length, var4, 0, var4.length, var0[2], 0, var0[2].length);
    a(var5, 0, var5.length, var0[2], 0, var0[2].length, var5, 0, var5.length);
    a((long[])var6, 0, var6.length, (long[])var5, 0, var5.length);
    a((long[])var7, 0, var7.length, (long[])var6, 0, var6.length);
    c(var8, 0, var8.length, var7, 0, var7.length, var6, 0, var6.length);
    c(var2, 0, var2.length, var8, 0, var8.length, var4, 0, var4.length);
    c(var2, 0, var2.length, var2, 0, var2.length, var5, 0, var5.length);
    c(var2, var2);
    c(var2, 0, var2.length, var2, 0, var2.length, var4, 0, var4.length);
    c(var2, 0, var2.length, var2, 0, var2.length, var5, 0, var5.length);
    c(var2, 0, var2.length, var2, 0, var2.length, var5, 0, var5.length);
    c(var0[0], 0, var0[0].length, var2, 0, var2.length, var5, 0, var5.length);
    a((long[])var3, 0, var3.length, (long[])var0[0], 0, var0[0].length);
    c(var3, 0, var3.length, var3, 0, var3.length, var5, 0, var5.length);
    if(b(var3, var4) != 0) {
      c(var0[0], 0, var0[0].length, var0[0], 0, var0[0].length, j, 0, j.length);
    }

    a((long[])var3, 0, var3.length, (long[])var0[0], 0, var0[0].length);
    c(var3, 0, var3.length, var3, 0, var3.length, var5, 0, var5.length);
    if(b(var3, var4) != 0) {
      return -1;
    } else {
      if(a(var0[0]) == (var1[31] & 255) >> 7) {
        b(var0[0], 0, var0[0].length, c, 0, c.length, var0[0], 0, var0[0].length);
      }

      c(var0[3], 0, var0[3].length, var0[0], 0, var0[0].length, var0[1], 0, var0[1].length);
      return 0;
    }
  }

  public static int crypto_sign_open(byte[] m, long dummy, byte[] sm, int n, byte[] pk) {
    byte[] var7 = new byte[32];
    byte[] var8 = new byte[64];
    long[][] var9 = new long[][]{new long[16], new long[16], new long[16], new long[16]};
    long[][] var10 = new long[][]{new long[16], new long[16], new long[16], new long[16]};
    if(n < 64) {
      return -1;
    } else if(a(var10, pk) != 0) {
      return -1;
    } else {
      int var6;
      for(var6 = 0; var6 < n; ++var6) {
        m[var6] = sm[var6];
      }

      for(var6 = 0; var6 < 32; ++var6) {
        m[var6 + 32] = pk[var6];
      }

      b(var8, m, 0, m.length, n);
      a(var8);
      a(var9, var10, var8, 0, var8.length);
      a((long[][])var10, (byte[])sm, 32, sm.length - 32);
      a(var9, var10);
      a(var7, var9);
      n -= 64;
      return b((byte[])sm, 0, sm.length, (byte[])var7, 0, var7.length) != 0?-1:0;
    }
  }

  public static byte[] crypto_sign_open(byte[] signed, byte[] publicSigningKey) {
    byte[] var2 = new byte[signed.length];
    int var3 = crypto_sign_open(var2, -1L, signed, signed.length, publicSigningKey);
    return var3 != 0?null:Arrays.copyOfRange(var2, 64, var2.length);
  }

  public static void randombytes(byte[] x, int len) {
    int var2 = len % 8;

    long var3;
    int var5;
    for(var5 = 0; var5 < len - var2; var5 += 8) {
      var3 = o.nextLong();
      x[var5 + 0] = (byte)((int)(var3 >>> 0));
      x[var5 + 1] = (byte)((int)(var3 >>> 8));
      x[var5 + 2] = (byte)((int)(var3 >>> 16));
      x[var5 + 3] = (byte)((int)(var3 >>> 24));
      x[var5 + 4] = (byte)((int)(var3 >>> 32));
      x[var5 + 5] = (byte)((int)(var3 >>> 40));
      x[var5 + 6] = (byte)((int)(var3 >>> 48));
      x[var5 + 7] = (byte)((int)(var3 >>> 56));
    }

    if(var2 > 0) {
      var3 = o.nextLong();

      for(var5 = len - var2; var5 < len; ++var5) {
        x[var5] = (byte)((int)(var3 >>> 8 * var5));
      }
    }

  }

  static {
    int var0;
    for(var0 = 0; var0 < a.length; ++var0) {
      a[var0] = 0;
    }

    for(var0 = 0; var0 < b.length; ++var0) {
      b[var0] = 0;
    }

    b[0] = 9;
    c = new long[16];
    d = new long[16];
    e = new long[16];

    for(var0 = 0; var0 < c.length; ++var0) {
      c[var0] = 0L;
    }

    for(var0 = 0; var0 < d.length; ++var0) {
      d[var0] = 0L;
    }

    d[0] = 1L;

    for(var0 = 0; var0 < e.length; ++var0) {
      e[var0] = 0L;
    }

    e[0] = 56129L;
    e[1] = 1L;
    f = new long[]{30883L, 4953L, 19914L, 30187L, 55467L, 16705L, 2637L, 112L, 59544L, 30585L, 16505L, 36039L, 65139L, 11119L, 27886L, 20995L};
    g = new long[]{61785L, 9906L, 39828L, 60374L, 45398L, 33411L, 5274L, 224L, 53552L, 61171L, 33010L, 6542L, 64743L, 22239L, 55772L, 9222L};
    h = new long[]{54554L, 36645L, 11616L, 51542L, 42930L, 38181L, 51040L, 26924L, 56412L, 64982L, 57905L, 49316L, 21502L, 52590L, 14035L, 8553L};
    i = new long[]{26200L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L, 26214L};
    j = new long[]{41136L, 18958L, 6951L, 50414L, 58488L, 44335L, 6150L, 12099L, 55207L, 15867L, 153L, 11085L, 57099L, 20417L, 9344L, 11139L};
    k = new byte[]{101, 120, 112, 97, 110, 100, 32, 51, 50, 45, 98, 121, 116, 101, 32, 107};
    l = new long[]{4794697086780616226L, 8158064640168781261L, -5349999486874862801L, -1606136188198331460L, 4131703408338449720L, 6480981068601479193L, -7908458776815382629L, -6116909921290321640L, -2880145864133508542L, 1334009975649890238L, 2608012711638119052L, 6128411473006802146L, 8268148722764581231L, -9160688886553864527L, -7215885187991268811L, -4495734319001033068L, -1973867731355612462L, -1171420211273849373L, 1135362057144423861L, 2597628984639134821L, 3308224258029322869L, 5365058923640841347L, 6679025012923562964L, 8573033837759648693L, -7476448914759557205L, -6327057829258317296L, -5763719355590565569L, -4658551843659510044L, -4116276920077217854L, -3051310485924567259L, 489312712824947311L, 1452737877330783856L, 2861767655752347644L, 3322285676063803686L, 5560940570517711597L, 5996557281743188959L, 7280758554555802590L, 8532644243296465576L, -9096487096722542874L, -7894198246740708037L, -6719396339535248540L, -6333637450476146687L, -4446306890439682159L, -4076793802049405392L, -3345356375505022440L, -2983346525034927856L, -860691631967231958L, 1182934255886127544L, 1847814050463011016L, 2177327727835720531L, 2830643537854262169L, 3796741975233480872L, 4115178125766777443L, 5681478168544905931L, 6601373596472566643L, 7507060721942968483L, 8399075790359081724L, 8693463985226723168L, -8878714635349349518L, -8302665154208450068L, -8016688836872298968L, -6606660893046293015L, -4685533653050689259L, -4147400797238176981L, -3880063495543823972L, -3348786107499101689L, -1523767162380948706L, -757361751448694408L, 500013540394364858L, 748580250866718886L, 1242879168328830382L, 1977374033974150939L, 2944078676154940804L, 3659926193048069267L, 4368137639120453308L, 4836135668995329356L, 5532061633213252278L, 6448918945643986474L, 6902733635092675308L, 7801388544844847127L};
    m = new byte[]{106, 9, -26, 103, -13, -68, -55, 8, -69, 103, -82, -123, -124, -54, -89, 59, 60, 110, -13, 114, -2, -108, -8, 43, -91, 79, -11, 58, 95, 29, 54, -15, 81, 14, 82, 127, -83, -26, -126, -47, -101, 5, 104, -116, 43, 62, 108, 31, 31, -125, -39, -85, -5, 65, -67, 107, 91, -32, -51, 25, 19, 126, 33, 121};
    n = new long[]{237L, 211L, 245L, 92L, 26L, 99L, 18L, 88L, 214L, 156L, 247L, 162L, 222L, 249L, 222L, 20L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 16L};
    o = new SecureRandom();
  }

  public static final class poly1305 {
    private byte[] a = new byte[16];
    private int[] b = new int[10];
    private int[] c = new int[10];
    private int[] d = new int[8];
    private int e = 0;
    private int f = 0;

    public poly1305(byte[] key) {
      int var2 = key[0] & 255 | (key[1] & 255) << 8;
      this.b[0] = var2 & 8191;
      int var3 = key[2] & 255 | (key[3] & 255) << 8;
      this.b[1] = (var2 >>> 13 | var3 << 3) & 8191;
      int var4 = key[4] & 255 | (key[5] & 255) << 8;
      this.b[2] = (var3 >>> 10 | var4 << 6) & 7939;
      int var5 = key[6] & 255 | (key[7] & 255) << 8;
      this.b[3] = (var4 >>> 7 | var5 << 9) & 8191;
      int var6 = key[8] & 255 | (key[9] & 255) << 8;
      this.b[4] = (var5 >>> 4 | var6 << 12) & 255;
      this.b[5] = var6 >>> 1 & 8190;
      int var7 = key[10] & 255 | (key[11] & 255) << 8;
      this.b[6] = (var6 >>> 14 | var7 << 2) & 8191;
      int var8 = key[12] & 255 | (key[13] & 255) << 8;
      this.b[7] = (var7 >>> 11 | var8 << 5) & 8065;
      int var9 = key[14] & 255 | (key[15] & 255) << 8;
      this.b[8] = (var8 >>> 8 | var9 << 8) & 8191;
      this.b[9] = var9 >>> 5 & 127;
      this.d[0] = key[16] & 255 | (key[17] & 255) << 8;
      this.d[1] = key[18] & 255 | (key[19] & 255) << 8;
      this.d[2] = key[20] & 255 | (key[21] & 255) << 8;
      this.d[3] = key[22] & 255 | (key[23] & 255) << 8;
      this.d[4] = key[24] & 255 | (key[25] & 255) << 8;
      this.d[5] = key[26] & 255 | (key[27] & 255) << 8;
      this.d[6] = key[28] & 255 | (key[29] & 255) << 8;
      this.d[7] = key[30] & 255 | (key[31] & 255) << 8;
    }

    public TweetNacl.poly1305 blocks(byte[] m, int mpos, int bytes) {
      int var4 = this.f != 0?0:2048;
      int var24 = this.c[0];
      int var25 = this.c[1];
      int var26 = this.c[2];
      int var27 = this.c[3];
      int var28 = this.c[4];
      int var29 = this.c[5];
      int var30 = this.c[6];
      int var31 = this.c[7];
      int var32 = this.c[8];
      int var33 = this.c[9];
      int var34 = this.b[0];
      int var35 = this.b[1];
      int var36 = this.b[2];
      int var37 = this.b[3];
      int var38 = this.b[4];
      int var39 = this.b[5];
      int var40 = this.b[6];
      int var41 = this.b[7];
      int var42 = this.b[8];

      for(int var43 = this.b[9]; bytes >= 16; bytes -= 16) {
        int var5 = m[mpos + 0] & 255 | (m[mpos + 1] & 255) << 8;
        var24 += var5 & 8191;
        int var6 = m[mpos + 2] & 255 | (m[mpos + 3] & 255) << 8;
        var25 += (var5 >>> 13 | var6 << 3) & 8191;
        int var7 = m[mpos + 4] & 255 | (m[mpos + 5] & 255) << 8;
        var26 += (var6 >>> 10 | var7 << 6) & 8191;
        int var8 = m[mpos + 6] & 255 | (m[mpos + 7] & 255) << 8;
        var27 += (var7 >>> 7 | var8 << 9) & 8191;
        int var9 = m[mpos + 8] & 255 | (m[mpos + 9] & 255) << 8;
        var28 += (var8 >>> 4 | var9 << 12) & 8191;
        var29 += var9 >>> 1 & 8191;
        int var10 = m[mpos + 10] & 255 | (m[mpos + 11] & 255) << 8;
        var30 += (var9 >>> 14 | var10 << 2) & 8191;
        int var11 = m[mpos + 12] & 255 | (m[mpos + 13] & 255) << 8;
        var31 += (var10 >>> 11 | var11 << 5) & 8191;
        int var12 = m[mpos + 14] & 255 | (m[mpos + 15] & 255) << 8;
        var32 += (var11 >>> 8 | var12 << 8) & 8191;
        var33 += var12 >>> 5 | var4;
        byte var13 = 0;
        int var14 = var13 + var24 * var34;
        var14 += var25 * 5 * var43;
        var14 += var26 * 5 * var42;
        var14 += var27 * 5 * var41;
        var14 += var28 * 5 * var40;
        int var44 = var14 >>> 13;
        var14 &= 8191;
        var14 += var29 * 5 * var39;
        var14 += var30 * 5 * var38;
        var14 += var31 * 5 * var37;
        var14 += var32 * 5 * var36;
        var14 += var33 * 5 * var35;
        var44 += var14 >>> 13;
        var14 &= 8191;
        int var15 = var44 + var24 * var35;
        var15 += var25 * var34;
        var15 += var26 * 5 * var43;
        var15 += var27 * 5 * var42;
        var15 += var28 * 5 * var41;
        var44 = var15 >>> 13;
        var15 &= 8191;
        var15 += var29 * 5 * var40;
        var15 += var30 * 5 * var39;
        var15 += var31 * 5 * var38;
        var15 += var32 * 5 * var37;
        var15 += var33 * 5 * var36;
        var44 += var15 >>> 13;
        var15 &= 8191;
        int var16 = var44 + var24 * var36;
        var16 += var25 * var35;
        var16 += var26 * var34;
        var16 += var27 * 5 * var43;
        var16 += var28 * 5 * var42;
        var44 = var16 >>> 13;
        var16 &= 8191;
        var16 += var29 * 5 * var41;
        var16 += var30 * 5 * var40;
        var16 += var31 * 5 * var39;
        var16 += var32 * 5 * var38;
        var16 += var33 * 5 * var37;
        var44 += var16 >>> 13;
        var16 &= 8191;
        int var17 = var44 + var24 * var37;
        var17 += var25 * var36;
        var17 += var26 * var35;
        var17 += var27 * var34;
        var17 += var28 * 5 * var43;
        var44 = var17 >>> 13;
        var17 &= 8191;
        var17 += var29 * 5 * var42;
        var17 += var30 * 5 * var41;
        var17 += var31 * 5 * var40;
        var17 += var32 * 5 * var39;
        var17 += var33 * 5 * var38;
        var44 += var17 >>> 13;
        var17 &= 8191;
        int var18 = var44 + var24 * var38;
        var18 += var25 * var37;
        var18 += var26 * var36;
        var18 += var27 * var35;
        var18 += var28 * var34;
        var44 = var18 >>> 13;
        var18 &= 8191;
        var18 += var29 * 5 * var43;
        var18 += var30 * 5 * var42;
        var18 += var31 * 5 * var41;
        var18 += var32 * 5 * var40;
        var18 += var33 * 5 * var39;
        var44 += var18 >>> 13;
        var18 &= 8191;
        int var19 = var44 + var24 * var39;
        var19 += var25 * var38;
        var19 += var26 * var37;
        var19 += var27 * var36;
        var19 += var28 * var35;
        var44 = var19 >>> 13;
        var19 &= 8191;
        var19 += var29 * var34;
        var19 += var30 * 5 * var43;
        var19 += var31 * 5 * var42;
        var19 += var32 * 5 * var41;
        var19 += var33 * 5 * var40;
        var44 += var19 >>> 13;
        var19 &= 8191;
        int var20 = var44 + var24 * var40;
        var20 += var25 * var39;
        var20 += var26 * var38;
        var20 += var27 * var37;
        var20 += var28 * var36;
        var44 = var20 >>> 13;
        var20 &= 8191;
        var20 += var29 * var35;
        var20 += var30 * var34;
        var20 += var31 * 5 * var43;
        var20 += var32 * 5 * var42;
        var20 += var33 * 5 * var41;
        var44 += var20 >>> 13;
        var20 &= 8191;
        int var21 = var44 + var24 * var41;
        var21 += var25 * var40;
        var21 += var26 * var39;
        var21 += var27 * var38;
        var21 += var28 * var37;
        var44 = var21 >>> 13;
        var21 &= 8191;
        var21 += var29 * var36;
        var21 += var30 * var35;
        var21 += var31 * var34;
        var21 += var32 * 5 * var43;
        var21 += var33 * 5 * var42;
        var44 += var21 >>> 13;
        var21 &= 8191;
        int var22 = var44 + var24 * var42;
        var22 += var25 * var41;
        var22 += var26 * var40;
        var22 += var27 * var39;
        var22 += var28 * var38;
        var44 = var22 >>> 13;
        var22 &= 8191;
        var22 += var29 * var37;
        var22 += var30 * var36;
        var22 += var31 * var35;
        var22 += var32 * var34;
        var22 += var33 * 5 * var43;
        var44 += var22 >>> 13;
        var22 &= 8191;
        int var23 = var44 + var24 * var43;
        var23 += var25 * var42;
        var23 += var26 * var41;
        var23 += var27 * var40;
        var23 += var28 * var39;
        var44 = var23 >>> 13;
        var23 &= 8191;
        var23 += var29 * var38;
        var23 += var30 * var37;
        var23 += var31 * var36;
        var23 += var32 * var35;
        var23 += var33 * var34;
        var44 += var23 >>> 13;
        var23 &= 8191;
        var44 = (var44 << 2) + var44 | 0;
        var44 = var44 + var14 | 0;
        var14 = var44 & 8191;
        var44 >>>= 13;
        var15 += var44;
        var24 = var14;
        var25 = var15;
        var26 = var16;
        var27 = var17;
        var28 = var18;
        var29 = var19;
        var30 = var20;
        var31 = var21;
        var32 = var22;
        var33 = var23;
        mpos += 16;
      }

      this.c[0] = var24;
      this.c[1] = var25;
      this.c[2] = var26;
      this.c[3] = var27;
      this.c[4] = var28;
      this.c[5] = var29;
      this.c[6] = var30;
      this.c[7] = var31;
      this.c[8] = var32;
      this.c[9] = var33;
      return this;
    }

    public TweetNacl.poly1305 finish(byte[] mac, int macpos) {
      int[] var3 = new int[10];
      int var7;
      if(this.e != 0) {
        var7 = this.e;

        for(this.a[var7++] = 1; var7 < 16; ++var7) {
          this.a[var7] = 0;
        }

        this.f = 1;
        this.blocks(this.a, 0, 16);
      }

      int var4 = this.c[1] >>> 13;
      this.c[1] &= 8191;

      for(var7 = 2; var7 < 10; ++var7) {
        this.c[var7] += var4;
        var4 = this.c[var7] >>> 13;
        this.c[var7] &= 8191;
      }

      this.c[0] += var4 * 5;
      var4 = this.c[0] >>> 13;
      this.c[0] &= 8191;
      this.c[1] += var4;
      var4 = this.c[1] >>> 13;
      this.c[1] &= 8191;
      this.c[2] += var4;
      var3[0] = this.c[0] + 5;
      var4 = var3[0] >>> 13;
      var3[0] &= 8191;

      for(var7 = 1; var7 < 10; ++var7) {
        var3[var7] = this.c[var7] + var4;
        var4 = var3[var7] >>> 13;
        var3[var7] &= 8191;
      }

      var3[9] -= 8192;
      var3[9] &= '\uffff';
      int var5 = (var3[9] >>> 15) - 1;
      var5 &= '\uffff';

      for(var7 = 0; var7 < 10; ++var7) {
        var3[var7] &= var5;
      }

      var5 = ~var5;

      for(var7 = 0; var7 < 10; ++var7) {
        this.c[var7] = this.c[var7] & var5 | var3[var7];
      }

      this.c[0] = (this.c[0] | this.c[1] << 13) & '\uffff';
      this.c[1] = (this.c[1] >>> 3 | this.c[2] << 10) & '\uffff';
      this.c[2] = (this.c[2] >>> 6 | this.c[3] << 7) & '\uffff';
      this.c[3] = (this.c[3] >>> 9 | this.c[4] << 4) & '\uffff';
      this.c[4] = (this.c[4] >>> 12 | this.c[5] << 1 | this.c[6] << 14) & '\uffff';
      this.c[5] = (this.c[6] >>> 2 | this.c[7] << 11) & '\uffff';
      this.c[6] = (this.c[7] >>> 5 | this.c[8] << 8) & '\uffff';
      this.c[7] = (this.c[8] >>> 8 | this.c[9] << 5) & '\uffff';
      int var6 = this.c[0] + this.d[0];
      this.c[0] = var6 & '\uffff';

      for(var7 = 1; var7 < 8; ++var7) {
        var6 = (this.c[var7] + this.d[var7] | 0) + (var6 >>> 16) | 0;
        this.c[var7] = var6 & '\uffff';
      }

      mac[macpos + 0] = (byte)(this.c[0] >>> 0 & 255);
      mac[macpos + 1] = (byte)(this.c[0] >>> 8 & 255);
      mac[macpos + 2] = (byte)(this.c[1] >>> 0 & 255);
      mac[macpos + 3] = (byte)(this.c[1] >>> 8 & 255);
      mac[macpos + 4] = (byte)(this.c[2] >>> 0 & 255);
      mac[macpos + 5] = (byte)(this.c[2] >>> 8 & 255);
      mac[macpos + 6] = (byte)(this.c[3] >>> 0 & 255);
      mac[macpos + 7] = (byte)(this.c[3] >>> 8 & 255);
      mac[macpos + 8] = (byte)(this.c[4] >>> 0 & 255);
      mac[macpos + 9] = (byte)(this.c[4] >>> 8 & 255);
      mac[macpos + 10] = (byte)(this.c[5] >>> 0 & 255);
      mac[macpos + 11] = (byte)(this.c[5] >>> 8 & 255);
      mac[macpos + 12] = (byte)(this.c[6] >>> 0 & 255);
      mac[macpos + 13] = (byte)(this.c[6] >>> 8 & 255);
      mac[macpos + 14] = (byte)(this.c[7] >>> 0 & 255);
      mac[macpos + 15] = (byte)(this.c[7] >>> 8 & 255);
      return this;
    }

    public TweetNacl.poly1305 update(byte[] m, int mpos, int bytes) {
      int var4;
      int var5;
      if(this.e != 0) {
        var5 = 16 - this.e;
        if(var5 > bytes) {
          var5 = bytes;
        }

        for(var4 = 0; var4 < var5; ++var4) {
          this.a[this.e + var4] = m[mpos + var4];
        }

        bytes -= var5;
        mpos += var5;
        this.e += var5;
        if(this.e < 16) {
          return this;
        }

        this.blocks(this.a, 0, 16);
        this.e = 0;
      }

      if(bytes >= 16) {
        var5 = bytes - bytes % 16;
        this.blocks(m, mpos, var5);
        mpos += var5;
        bytes -= var5;
      }

      if(bytes != 0) {
        for(var4 = 0; var4 < bytes; ++var4) {
          this.a[this.e + var4] = m[mpos + var4];
        }

        this.e += bytes;
      }

      return this;
    }
  }

  public static final class Signature {
    private byte[] a;
    private byte[] b;
    public static final int publicKeyLength = 32;
    public static final int secretKeyLength = 64;
    public static final int seedLength = 32;
    public static final int signatureLength = 64;

    public Signature(byte[] theirPublicKey, byte[] mySecretKey) {
      this.a = theirPublicKey;
      this.b = mySecretKey;
    }

    public byte[] sign(byte[] message) {
      byte[] var2 = new byte[message.length + 64];
      TweetNacl.crypto_sign(var2, -1L, message, message.length, this.b);
      return var2;
    }

    public byte[] open(byte[] signedMessage) {
      if(signedMessage != null && signedMessage.length > 64) {
        byte[] var2 = new byte[signedMessage.length];
        if(0 != TweetNacl.crypto_sign_open(var2, -1L, signedMessage, signedMessage.length, this.a)) {
          return null;
        } else {
          byte[] var3 = new byte[signedMessage.length - 64];

          for(int var4 = 0; var4 < var3.length; ++var4) {
            var3[var4] = signedMessage[var4 + 64];
          }

          return var3;
        }
      } else {
        return null;
      }
    }

    public byte[] detached(byte[] message) {
      byte[] var2 = this.sign(message);
      byte[] var3 = new byte[64];

      for(int var4 = 0; var4 < var3.length; ++var4) {
        var3[var4] = var2[var4];
      }

      return var3;
    }

    public boolean detached_verify(byte[] message, byte[] signature) {
      if(signature.length != 64) {
        return false;
      } else if(this.a.length != 32) {
        return false;
      } else {
        byte[] var3 = new byte[64 + message.length];
        byte[] var4 = new byte[64 + message.length];

        int var5;
        for(var5 = 0; var5 < 64; ++var5) {
          var3[var5] = signature[var5];
        }

        for(var5 = 0; var5 < message.length; ++var5) {
          var3[var5 + 64] = message[var5];
        }

        return TweetNacl.crypto_sign_open(var4, -1L, var3, var3.length, this.a) >= 0;
      }
    }

    public static TweetNacl.Signature.KeyPair keyPair() {
      TweetNacl.Signature.KeyPair var0 = new TweetNacl.Signature.KeyPair();
      TweetNacl.crypto_sign_keypair(var0.getPublicKey(), var0.getSecretKey(), false);
      return var0;
    }

    public static TweetNacl.Signature.KeyPair keyPair_fromSecretKey(byte[] secretKey) {
      TweetNacl.Signature.KeyPair var1 = new TweetNacl.Signature.KeyPair();
      byte[] var2 = var1.getPublicKey();
      byte[] var3 = var1.getSecretKey();

      int var4;
      for(var4 = 0; var4 < var1.getSecretKey().length; ++var4) {
        var3[var4] = secretKey[var4];
      }

      for(var4 = 0; var4 < var1.getPublicKey().length; ++var4) {
        var2[var4] = secretKey[32 + var4];
      }

      return var1;
    }

    public static TweetNacl.Signature.KeyPair keyPair_fromSeed(byte[] seed) {
      TweetNacl.Signature.KeyPair var1 = new TweetNacl.Signature.KeyPair();
      byte[] var2 = var1.getPublicKey();
      byte[] var3 = var1.getSecretKey();

      for(int var4 = 0; var4 < 32; ++var4) {
        var3[var4] = seed[var4];
      }

      TweetNacl.crypto_sign_keypair(var2, var3, true);
      return var1;
    }

    public static class KeyPair {
      private byte[] a = new byte[32];
      private byte[] b = new byte[64];

      public KeyPair() {
      }

      public byte[] getPublicKey() {
        return this.a;
      }

      public byte[] getSecretKey() {
        return this.b;
      }
    }
  }

  public static final class Hash {
    public static final int hashLength = 64;

    public Hash() {
    }

    public static byte[] sha512(byte[] message) {
      if(message != null && message.length > 0) {
        byte[] var1 = new byte[64];
        TweetNacl.crypto_hash(var1, message);
        return var1;
      } else {
        return null;
      }
    }

    public static byte[] sha512(String message) throws UnsupportedEncodingException {
      return sha512(message.getBytes("utf-8"));
    }
  }

  public static final class ScalarMult {
    public static final int scalarLength = 32;
    public static final int groupElementLength = 32;

    public ScalarMult() {
    }

    public static byte[] scalseMult(byte[] n, byte[] p) {
      if(n.length == 32 && p.length == 32) {
        byte[] var2 = new byte[32];
        TweetNacl.crypto_scalarmult(var2, n, p);
        return var2;
      } else {
        return null;
      }
    }

    public static byte[] scalseMult_base(byte[] n) {
      if(n.length != 32) {
        return null;
      } else {
        byte[] var1 = new byte[32];
        TweetNacl.crypto_scalarmult_base(var1, n);
        return var1;
      }
    }
  }

  public static final class SecretBox {
    private AtomicLong a;
    private byte[] b;
    public static final int keyLength = 32;
    public static final int nonceLength = 24;
    public static final int overheadLength = 16;
    public static final int zerobytesLength = 32;
    public static final int boxzerobytesLength = 16;

    public SecretBox(byte[] key) {
      this(key, 68L);
    }

    public SecretBox(byte[] key, long nonce) {
      this.b = key;
      this.a = new AtomicLong(nonce);
    }

    public void setNonce(long nonce) {
      this.a.set(nonce);
    }

    public long getNonce() {
      return this.a.get();
    }

    public long incrNonce() {
      return this.a.incrementAndGet();
    }

    private byte[] a() {
      long var1 = this.a.get();
      byte[] var3 = new byte[24];

      for(int var4 = 0; var4 < 24; var4 += 8) {
        var3[var4 + 0] = (byte)((int)(var1 >>> 0));
        var3[var4 + 1] = (byte)((int)(var1 >>> 8));
        var3[var4 + 2] = (byte)((int)(var1 >>> 16));
        var3[var4 + 3] = (byte)((int)(var1 >>> 24));
        var3[var4 + 4] = (byte)((int)(var1 >>> 32));
        var3[var4 + 5] = (byte)((int)(var1 >>> 40));
        var3[var4 + 6] = (byte)((int)(var1 >>> 48));
        var3[var4 + 7] = (byte)((int)(var1 >>> 56));
      }

      return var3;
    }

    public byte[] box(byte[] message) {
      return this.box(message, this.a());
    }

    public byte[] box(byte[] message, byte[] theNonce) {
      if(message != null && message.length > 0 && theNonce != null && theNonce.length == 24) {
        byte[] var3 = new byte[message.length + 32];
        byte[] var4 = new byte[var3.length];

        for(int var5 = 0; var5 < message.length; ++var5) {
          var3[var5 + 32] = message[var5];
        }

        if(0 != TweetNacl.crypto_secretbox(var4, var3, var3.length, theNonce, this.b)) {
          return null;
        } else {
          byte[] var7 = new byte[var4.length - 16];

          for(int var6 = 0; var6 < var7.length; ++var6) {
            var7[var6] = var4[var6 + 16];
          }

          return var7;
        }
      } else {
        return null;
      }
    }

    public byte[] open(byte[] box) {
      return this.open(box, this.a());
    }

    public byte[] open(byte[] box, byte[] theNonce) {
      if(box != null && box.length > 16 && theNonce != null && theNonce.length == 24) {
        byte[] var3 = new byte[box.length + 16];
        byte[] var4 = new byte[var3.length];

        for(int var5 = 0; var5 < box.length; ++var5) {
          var3[var5 + 16] = box[var5];
        }

        if(0 != TweetNacl.crypto_secretbox_open(var4, var3, var3.length, theNonce, this.b)) {
          return null;
        } else {
          byte[] var7 = new byte[var4.length - 32];

          for(int var6 = 0; var6 < var7.length; ++var6) {
            var7[var6] = var4[var6 + 32];
          }

          return var7;
        }
      } else {
        return null;
      }
    }
  }

  public static final class Box {
    private AtomicLong a;
    private byte[] b;
    private byte[] c;
    private byte[] d;
    public static final int publicKeyLength = 32;
    public static final int secretKeyLength = 32;
    public static final int sharedKeyLength = 32;
    public static final int nonceLength = 24;
    public static final int zerobytesLength = 32;
    public static final int boxzerobytesLength = 16;
    public static final int overheadLength = 16;

    public Box(byte[] theirPublicKey, byte[] mySecretKey) {
      this(theirPublicKey, mySecretKey, 68L);
    }

    public Box(byte[] theirPublicKey, byte[] mySecretKey, long nonce) {
      this.b = theirPublicKey;
      this.c = mySecretKey;
      this.a = new AtomicLong(nonce);
      this.before();
    }

    public void setNonce(long nonce) {
      this.a.set(nonce);
    }

    public long getNonce() {
      return this.a.get();
    }

    public long incrNonce() {
      return this.a.incrementAndGet();
    }

    private byte[] a() {
      long var1 = this.a.get();
      byte[] var3 = new byte[24];

      for(int var4 = 0; var4 < 24; var4 += 8) {
        var3[var4 + 0] = (byte)((int)(var1 >>> 0));
        var3[var4 + 1] = (byte)((int)(var1 >>> 8));
        var3[var4 + 2] = (byte)((int)(var1 >>> 16));
        var3[var4 + 3] = (byte)((int)(var1 >>> 24));
        var3[var4 + 4] = (byte)((int)(var1 >>> 32));
        var3[var4 + 5] = (byte)((int)(var1 >>> 40));
        var3[var4 + 6] = (byte)((int)(var1 >>> 48));
        var3[var4 + 7] = (byte)((int)(var1 >>> 56));
      }

      return var3;
    }

    public byte[] box(byte[] message) {
      return this.box(message, this.a());
    }

    public byte[] box(byte[] message, byte[] theNonce) {
      if(message != null && message.length > 0 && theNonce != null && theNonce.length == 24) {
        byte[] var3 = new byte[message.length + 32];
        byte[] var4 = new byte[var3.length];

        for(int var5 = 0; var5 < message.length; ++var5) {
          var3[var5 + 32] = message[var5];
        }

        if(0 != TweetNacl.crypto_box(var4, var3, var3.length, theNonce, this.b, this.c)) {
          return null;
        } else {
          byte[] var7 = new byte[var4.length - 16];

          for(int var6 = 0; var6 < var7.length; ++var6) {
            var7[var6] = var4[var6 + 16];
          }

          return var7;
        }
      } else {
        return null;
      }
    }

    public byte[] open(byte[] box) {
      return this.open(box, this.a());
    }

    public byte[] open(byte[] box, byte[] theNonce) {
      if(box != null && box.length > 16 && theNonce != null && theNonce.length == 24) {
        byte[] var3 = new byte[box.length + 16];
        byte[] var4 = new byte[var3.length];

        for(int var5 = 0; var5 < box.length; ++var5) {
          var3[var5 + 16] = box[var5];
        }

        if(0 != TweetNacl.crypto_box_open(var4, var3, var3.length, theNonce, this.b, this.c)) {
          return null;
        } else {
          byte[] var7 = new byte[var4.length - 32];

          for(int var6 = 0; var6 < var7.length; ++var6) {
            var7[var6] = var4[var6 + 32];
          }

          return var7;
        }
      } else {
        return null;
      }
    }

    public byte[] before() {
      if(this.d == null) {
        this.d = new byte[32];
        TweetNacl.crypto_box_beforenm(this.d, this.b, this.c);
      }

      return this.d;
    }

    public byte[] after(byte[] message) {
      return this.after(message, this.a());
    }

    public byte[] after(byte[] message, byte[] theNonce) {
      if(message != null && message.length > 0 && theNonce != null && theNonce.length == 24) {
        byte[] var3 = new byte[message.length + 32];
        byte[] var4 = new byte[var3.length];

        for(int var5 = 0; var5 < message.length; ++var5) {
          var3[var5 + 32] = message[var5];
        }

        if(0 != TweetNacl.crypto_secretbox(var4, var3, var3.length, theNonce, this.d)) {
          return null;
        } else {
          byte[] var7 = new byte[var4.length - 16];

          for(int var6 = 0; var6 < var7.length; ++var6) {
            var7[var6] = var4[var6 + 16];
          }

          return var7;
        }
      } else {
        return null;
      }
    }

    public byte[] open_after(byte[] box) {
      return this.open_after(box, this.a());
    }

    public byte[] open_after(byte[] box, byte[] theNonce) {
      if(box != null && box.length > 16 && theNonce != null && theNonce.length == 24) {
        byte[] var3 = new byte[box.length + 16];
        byte[] var4 = new byte[var3.length];

        for(int var5 = 0; var5 < box.length; ++var5) {
          var3[var5 + 16] = box[var5];
        }

        if(TweetNacl.crypto_secretbox_open(var4, var3, var3.length, theNonce, this.d) != 0) {
          return null;
        } else {
          byte[] var7 = new byte[var4.length - 32];

          for(int var6 = 0; var6 < var7.length; ++var6) {
            var7[var6] = var4[var6 + 32];
          }

          return var7;
        }
      } else {
        return null;
      }
    }

    public static TweetNacl.Box.KeyPair keyPair() {
      TweetNacl.Box.KeyPair var0 = new TweetNacl.Box.KeyPair();
      TweetNacl.crypto_box_keypair(var0.getPublicKey(), var0.getSecretKey());
      return var0;
    }

    public static TweetNacl.Box.KeyPair keyPair_fromSecretKey(byte[] secretKey) {
      TweetNacl.Box.KeyPair var1 = new TweetNacl.Box.KeyPair();
      byte[] var2 = var1.getSecretKey();
      byte[] var3 = var1.getPublicKey();

      for(int var4 = 0; var4 < var2.length; ++var4) {
        var2[var4] = secretKey[var4];
      }

      TweetNacl.crypto_scalarmult_base(var3, var2);
      return var1;
    }

    public static class KeyPair {
      private byte[] a = new byte[32];
      private byte[] b = new byte[32];

      public KeyPair() {
      }

      public byte[] getPublicKey() {
        return this.a;
      }

      public byte[] getSecretKey() {
        return this.b;
      }
    }
  }
}
package com.mla.wwl.Utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;

import com.mla.wwl.MlaSDKInt;

import java.nio.ByteBuffer;

public class BitmapUtils {
    public BitmapUtils() {
    }

    public static Bitmap rotate(Bitmap b, int degrees) {
        return rotateAndMirror(b, degrees, false);
    }

    public static Bitmap rotateAndMirror(Bitmap b, int degrees, boolean mirror) {
        if ((degrees != 0 || mirror) && b != null) {
            Matrix var3 = new Matrix();
            if (mirror) {
                var3.postScale(-1.0F, 1.0F);
                degrees = (degrees + 360) % 360;
                if (degrees != 0 && degrees != 180) {
                    if (degrees != 90 && degrees != 270) {
                        throw new IllegalArgumentException("Invalid degrees=" + degrees);
                    }

                    var3.postTranslate((float) b.getHeight(), 0.0F);
                } else {
                    var3.postTranslate((float) b.getWidth(), 0.0F);
                }
            }

            if (degrees != 0) {
                var3.postRotate((float) degrees, (float) b.getWidth() / 2.0F, (float) b.getHeight() / 2.0F);
            }

            try {
                Bitmap var4 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), var3, true);
                if (b != var4) {
                    b.recycle();
                    b = var4;
                }
            } catch (OutOfMemoryError var5) {
                ;
            }
        }

        return b;
    }

    public static Bitmap createCropCenteredBtmp(Bitmap srcBmp) {
        Bitmap var1;
        if (srcBmp.getWidth() >= srcBmp.getHeight()) {
            var1 = Bitmap.createBitmap(srcBmp, srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2, 0, srcBmp.getHeight(), srcBmp.getHeight());
        } else {
            var1 = Bitmap.createBitmap(srcBmp, 0, srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2, srcBmp.getWidth(), srcBmp.getWidth());
        }

        return var1;
    }

    public static Bitmap makeBitmap(byte[] jpegData, int maxNumOfPixels) {
        try {
            Options var2 = new Options();
            if (maxNumOfPixels != 0) {
                var2.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(jpegData, 0, jpegData.length, var2);
                var2.inSampleSize = computeSampleSize(var2, -1, maxNumOfPixels);
            }

            if (!var2.mCancel && var2.outWidth != -1 && var2.outHeight != -1) {
                var2.inJustDecodeBounds = false;
                var2.inDither = false;
                var2.inPreferredConfig = Config.ARGB_8888;
                return BitmapFactory.decodeByteArray(jpegData, 0, jpegData.length, var2);
            } else {
                return null;
            }
        } catch (OutOfMemoryError var3) {
            return null;
        }
    }

    public static Bitmap getBitmapFromPath(String photoFilePath) {
        Options var1 = new Options();
        var1.inPreferredConfig = Config.ARGB_8888;
        return BitmapFactory.decodeFile(photoFilePath, var1);
    }

    public static Bitmap getPhotoThumbnail(Bitmap sourceBtmp) {
        if (sourceBtmp == null) {
            return null;
        } else {
            int var1 = sourceBtmp.getWidth();
            int var2 = sourceBtmp.getHeight();
            int var3 = Math.max(var1, var2);
            if (var3 > 512) {
                float var4 = 512.0F / (float) var3;
                int var5 = Math.round(var4 * (float) var1);
                int var6 = Math.round(var4 * (float) var2);
                Bitmap var7 = Bitmap.createScaledBitmap(sourceBtmp, var5, var6, true);
                sourceBtmp.recycle();
                return var7;
            } else {
                return sourceBtmp;
            }
        }
    }

    public static int computeSampleSize(Options options, int minSideLength, int maxNumOfPixels) {
        int var3 = a(options, minSideLength, maxNumOfPixels);
        int var4;
        if (var3 <= 8) {
            for (var4 = 1; var4 < var3; var4 <<= 1) {
                ;
            }
        } else {
            var4 = (var3 + 7) / 8 * 8;
        }

        return var4;
    }

    private static int a(Options var0, int var1, int var2) {
        double var3 = (double) var0.outWidth;
        double var5 = (double) var0.outHeight;
        int var7 = var2 < 0 ? 1 : (int) Math.ceil(Math.sqrt(var3 * var5 / (double) var2));
        int var8 = var1 < 0 ? 128 : (int) Math.min(Math.floor(var3 / (double) var1), Math.floor(var5 / (double) var1));
        return var8 < var7 ? var7 : (var2 < 0 && var1 < 0 ? 1 : (var1 < 0 ? var7 : var8));
    }

    public static void addWatermark(Bitmap srcBtmp) {
        int var1 = srcBtmp.getWidth();
        int var2 = srcBtmp.getHeight();
        float var3 = (float) var1 / (float) var2;
        int var4 = var3 > 1.0F ? var2 : var1;
        Canvas var5 = new Canvas(srcBtmp);
        Bitmap var6 = MlaSDKInt.WATERMARK_BTMP;
        Matrix var7 = new Matrix();
        float var8 = (float) (0.675D * ((double) var4 * 110.0D / 1080.0D) / (double) var6.getHeight());
        var8 *= MlaSDKInt.WATERMARK_SCALE;
        var7.postScale(var8, var8);
        RectF var9 = new RectF(0.0F, 0.0F, (float) var6.getWidth(), (float) var6.getHeight());
        var7.mapRect(var9);
        float var10 = 0.06F * (float) var4;
        float var11 = 0.048F * (float) var4;
        var7.postTranslate((float) var1 - var9.right - var10, (float) var2 - var9.bottom - var11);
        Paint var12 = new Paint();
        var12.setAlpha((int) (255.0D * (double) MlaSDKInt.WATERMARK_OPACITY));
        var12.setFilterBitmap(true);
        var5.drawBitmap(var6, var7, var12);
    }

    public static Bitmap levelBitmap(Bitmap srcBtmp, float outputAspectRatio, float angle, float scaleFactor) {
        float var4 = (float) srcBtmp.getWidth();
        float var5 = (float) srcBtmp.getHeight();
        float var7 = outputAspectRatio * var5;
        float var8 = var7 / scaleFactor;
        float var9 = var5 / scaleFactor;
        Bitmap var10 = Bitmap.createBitmap((int) var8, (int) var9, srcBtmp.getConfig());
        Canvas var11 = new Canvas(var10);
        Matrix var12 = new Matrix();
        var12.postTranslate(-var4 / 2.0F, -var5 / 2.0F);
        var12.postRotate(-((float) Math.toDegrees((double) angle)));
        var12.postTranslate(var8 / 2.0F, var9 / 2.0F);
        Paint var13 = new Paint();
        var13.setFilterBitmap(true);
        var11.drawBitmap(srcBtmp, var12, var13);
        return var10;
    }

    public static Bitmap bitmapFromGLBuffer(ByteBuffer buffer, int width, int height) {
        Bitmap var3 = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        var3.copyPixelsFromBuffer(buffer);
        Matrix var4 = new Matrix();
        var4.preScale(1.0F, -1.0F);
        Bitmap var5 = Bitmap.createBitmap(var3, 0, 0, width, height, var4, true);
        if (var5 != var3) {
            var3.recycle();
        }

        return var5;
    }
}


package com.mla.wwl.events;

import java.io.File;



public class SnapshotCapturedEvent
{
  public File file;
  
  public SnapshotCapturedEvent(File file)
  {
    this.file = file;
  }
}

package com.mla.wwl.events;

import java.io.File;
public class MuxerFinishedEvent {
  public File file;

  public MuxerFinishedEvent(File file) {
    this.file = file;
  }
}

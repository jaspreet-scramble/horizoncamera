package com.mla.wwl.events;

import android.hardware.Camera;



public class PreviewStartedEvent
{
  public final int cameraId;
  public final Camera.Parameters params;
  public final int facing;
  
  public PreviewStartedEvent(int cameraId, Camera.Parameters params, int facing)
  {
    this.cameraId = cameraId;
    this.params = params;
    this.facing = facing;
  }
}

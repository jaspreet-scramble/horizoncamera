package com.mla.wwl.events;

import java.io.File;

public class PhotoCapturedEvent
{
  public File file;
  public boolean success;
  
  public PhotoCapturedEvent(File file, boolean success)
  {
    this.file = file;
    this.success = success;
  }
}

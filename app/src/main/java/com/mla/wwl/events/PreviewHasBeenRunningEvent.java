package com.mla.wwl.events;

import android.hardware.Camera;

public class PreviewHasBeenRunningEvent {
    public Camera.Parameters params;
    public int facing;

    public PreviewHasBeenRunningEvent(Camera.Parameters params, int facing) {
        this.params = params;
        this.facing = facing;
    }
}

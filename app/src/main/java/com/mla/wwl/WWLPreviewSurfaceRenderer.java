package com.mla.wwl;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.SurfaceHolder.Callback;
import com.mla.wwl.WWLView.FillMode;
import com.mla.wwl.WWLView.ViewType;
import com.mla.wwl.Utils.Utils;
import com.mla.wwl.Utils.WWLHandler.ResultRunnable;
import com.mla.wwl.gles.EglCore;
import com.mla.wwl.gles.Filters;
import com.mla.wwl.gles.FullFrameRect;
import com.mla.wwl.gles.Texture2dProgram;
import com.mla.wwl.gles.WindowSurface;
import com.mla.wwl.gles.Texture2dProgram.ProgramType;

public class WWLPreviewSurfaceRenderer implements c {
  private SurfaceView a;
  private WWLPreviewSurfaceRenderer.a b;
  private d c;
  private final float d;
  private int e;
  private int f;
  private boolean g;
  private int h;
  private int i;
  private boolean j = true;
  private int k;
  private int l;
  private boolean m;
  private WindowSurface n;
  private FullFrameRect o;
  private long p;
  private final Object q = new Object();
  private boolean r;
  private volatile boolean s;
  private volatile boolean t = true;
  private ViewType u;
  private boolean v;
  private FillMode w;
  private int x;
  private Callback y;

  public WWLPreviewSurfaceRenderer(SurfaceView surfaceView) {
    this.u = ViewType.NORMAL;
    this.v = true;
    this.w = FillMode.ASPECT_FIT;
    this.x = Color.rgb(128, 128, 128);
    this.y = new Callback() {
      public void surfaceCreated(SurfaceHolder holder) {
        if(WWLPreviewSurfaceRenderer.this.b != null) {
          WWLPreviewSurfaceRenderer.this.b.sendMessage(WWLPreviewSurfaceRenderer.this.b.obtainMessage(0));
        } else {
          WWLPreviewSurfaceRenderer.this.m = true;
        }

      }

      public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if(WWLPreviewSurfaceRenderer.this.b != null) {
          WWLPreviewSurfaceRenderer.this.b.sendMessage(WWLPreviewSurfaceRenderer.this.b.obtainMessage(1, width, height));
        } else {
          WWLPreviewSurfaceRenderer.this.e = width;
          WWLPreviewSurfaceRenderer.this.f = height;
        }

      }

      public void surfaceDestroyed(SurfaceHolder holder) {
        if(WWLPreviewSurfaceRenderer.this.b != null) {
          WWLPreviewSurfaceRenderer.this.r = false;
          WWLPreviewSurfaceRenderer.this.b.sendMessage(WWLPreviewSurfaceRenderer.this.b.obtainMessage(2));
          synchronized(WWLPreviewSurfaceRenderer.this.q) {
            while(!WWLPreviewSurfaceRenderer.this.r) {
              try {
                WWLPreviewSurfaceRenderer.this.q.wait();
              } catch (InterruptedException var5) {
                var5.printStackTrace();
              }
            }
          }
        } else {
          WWLPreviewSurfaceRenderer.this.m = false;
        }

      }
    };
    this.a = surfaceView;
    this.a.getHolder().addCallback(this.y);
    this.d = Utils.getDensity(surfaceView.getContext());
    this.e = this.f = this.h = this.i = -1;
    this.p = -1L;
    this.k = -1;
    this.l = 0;
  }

  private void a() {
    this.m = true;
    this.c.a(this);
  }

  private void a(int var1, int var2) {
    this.e = var1;
    this.f = var2;
    if(this.o != null) {
      this.o.setSize(var1, var2);
    }

  }

  private void b() {
    this.c.b(this);
    this.m = false;
    Object var1 = this.q;
    synchronized(this.q) {
      this.r = true;
      this.q.notifyAll();
    }
  }

  public void onRendererWillBeAttached(d callback, Looper looper) {
    this.c = callback;
    this.b = new WWLPreviewSurfaceRenderer.a(looper);
  }

  public void onRendererDetached() {
    this.c = null;
    this.b = null;
  }

  public void prepareGL(EglCore eglCore) {
    if(!this.m) {
      throw new RuntimeException("Can't create WindowSurface when surface isn't ready.");
    } else if(this.n != null) {
      throw new RuntimeException("WindowSurface already initialised.");
    } else {
      Surface var2 = this.a.getHolder().getSurface();
      this.n = new WindowSurface(eglCore, var2, false);
      this.n.makeCurrent();
      this.o = new FullFrameRect(new Texture2dProgram(ProgramType.TEXTURE_EXT), true, this.u, this.d);
      this.o.setHUDVisible(this.v);
      this.o.setFillMode(this.w);
      this.o.setTintColor(this.x);
      this.o.setMirroredFrontFace(true);
      if(this.e != -1) {
        this.o.setSize(this.e, this.f);
      }

      this.p = 0L;
      this.s = true;
    }
  }

  public void destroyGL() {
    if(this.n == null) {
      throw new RuntimeException("WindowSurface has been already released.");
    } else {
      this.s = false;
      this.n.makeCurrent();
      this.o.release();
      this.n.release();
      this.n = null;
      this.o = null;
    }
  }

  public boolean isSurfaceReady() {
    return this.m;
  }

  public boolean isRendererReady() {
    return this.s;
  }

  public void setSourceSize(int width, int height, boolean cameraFacingBack) {
    this.h = width;
    this.i = height;
    this.j = cameraFacingBack;
    this.g = true;
  }

  public void setOutputSize(int width, int height) {
    this.o.setOutputSize(width, height);
  }

  public void setCameraToScreenAngle(int degrees) {
    this.o.setCameraToScreenAngle(degrees);
  }

  public void handleFrameAvailable(int mCameraTextureId, long timestamp, float[] transformMatrix, float[] frameParams) {
    if(this.t) {
      this.n.makeCurrent();
      if(this.k != this.l) {
        Filters.updateFilter(this.o, this.l);
        this.k = this.l;
        this.g = true;
      }

      if(this.g) {
        this.o.setSourceSize(this.h, this.i, this.j);
        this.g = false;
      }

      this.o.drawFrame(mCameraTextureId, transformMatrix, frameParams[0], frameParams[1]);
      this.n.swapBuffers();
      ++this.p;
    }
  }

  protected boolean isEnabled() {
    return this.t;
  }

  protected void setEnabled(boolean enabled) {
    this.t = enabled;
  }

  protected ViewType getViewType() {
    return this.u;
  }

  protected void setViewType(ViewType viewType) {
    this.u = viewType;
    if(this.b != null) {
      this.b.post(new Runnable() {
        public void run() {
          if(WWLPreviewSurfaceRenderer.this.o != null) {
            WWLPreviewSurfaceRenderer.this.o.setViewType(WWLPreviewSurfaceRenderer.this.u);
          }
        }
      });
    }

  }

  protected boolean isHUDVisible() {
    return this.v;
  }

  protected void setHUDVisible(boolean visible) {
    this.v = visible;
    if(this.b != null) {
      this.b.post(new Runnable() {
        public void run() {
          if(WWLPreviewSurfaceRenderer.this.o != null) {
            WWLPreviewSurfaceRenderer.this.o.setHUDVisible(WWLPreviewSurfaceRenderer.this.v);
          }
        }
      });
    }

  }

  protected FillMode getFillMode() {
    return this.w;
  }

  protected void setFillMode(FillMode fillMode) {
    this.w = fillMode;
    if(this.b != null) {
      this.b.post(new Runnable() {
        public void run() {
          if(WWLPreviewSurfaceRenderer.this.o != null) {
            WWLPreviewSurfaceRenderer.this.o.setFillMode(WWLPreviewSurfaceRenderer.this.w);
          }
        }
      });
    }

  }

  protected int getCropRegionTintColor() {
    return this.x;
  }

  protected void setCropRegionTintColor(final int color) {
    this.x = color;
    if(this.b != null) {
      this.b.post(new Runnable() {
        public void run() {
          if(WWLPreviewSurfaceRenderer.this.o != null) {
            WWLPreviewSurfaceRenderer.this.o.setTintColor(color);
          }
        }
      });
    }

  }

  public float[] getOutputFramePlacement() {
    if(this.b != null) {
      ResultRunnable var1 = new ResultRunnable<float[]>() {
        public void doRun() {
          if(WWLPreviewSurfaceRenderer.this.o != null) {
            this.returnValue = WWLPreviewSurfaceRenderer.this.o.getOutputFramePlacement();
          } else {
            this.returnValue = null;
          }

        }
      };
      this.b.post(var1);
      return (float[])var1.get();
    } else {
      return null;
    }
  }

  public boolean mapTouchToCamera(final Point touchPoint, final Point mappedPoint) {
    if(this.b != null) {
      ResultRunnable var3 = new ResultRunnable<Boolean>() {
        public void doRun() {
          if(WWLPreviewSurfaceRenderer.this.o != null) {
            this.returnValue = Boolean.valueOf(WWLPreviewSurfaceRenderer.this.o.mapTouchToCamera(touchPoint, mappedPoint));
          } else {
            this.returnValue = Boolean.valueOf(false);
          }

        }
      };
      this.b.post(var3);
      return ((Boolean)var3.get()).booleanValue();
    } else {
      return false;
    }
  }

  private class a extends Handler {
    a(Looper var2) {
      super(var2);
    }

    public void handleMessage(Message msg) {
      int var2 = msg.what;
      switch(var2) {
        case 0:
          WWLPreviewSurfaceRenderer.this.a();
          break;
        case 1:
          WWLPreviewSurfaceRenderer.this.a(msg.arg1, msg.arg2);
          break;
        case 2:
          WWLPreviewSurfaceRenderer.this.b();
          break;
        default:
          throw new RuntimeException("unknown message " + var2);
      }

    }
  }
}

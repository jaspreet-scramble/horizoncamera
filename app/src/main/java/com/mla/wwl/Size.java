package com.mla.wwl;


public final class Size implements Comparable<Size> {
  private final int a;
  private final int b;
  private final float c;

  public Size(int width, int height) {
    this.a = width;
    this.b = height;
    this.c = (float)this.a / (float)this.b;
  }

  public Size(int[] dimensions) {
    this(dimensions[0], dimensions[1]);
  }

  public int getWidth() {
    return this.a;
  }

  public int getHeight() {
    return this.b;
  }

  public float getAspectRatio() {
    return this.c;
  }

  public int[] getDimensions() {
    return new int[]{this.a, this.b};
  }

  public boolean equals(Object obj) {
    if(obj == null) {
      return false;
    } else if(this == obj) {
      return true;
    } else if(!(obj instanceof Size)) {
      return false;
    } else {
      Size var2 = (Size)obj;
      return this.a == var2.a && this.b == var2.b;
    }
  }

  public String toString() {
    return this.a + "x" + this.b;
  }

  public int hashCode() {
    return this.b ^ (this.a << 16 | this.a >>> 16);
  }

  public int compareTo(Size size) {
    if(size.equals(this)) {
      return 0;
    } else {
      int var2 = this.getWidth() * this.getHeight() - size.getWidth() * size.getHeight();
      return var2 == 0?(this.getWidth() > size.getWidth()?1:-1):var2;
    }
  }
}

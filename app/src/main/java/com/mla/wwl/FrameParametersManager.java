package com.mla.wwl;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;

import com.google.common.eventbus.EventBus;
import com.mla.wwl.WWLVars.WWLLevelerCropMode;
import com.mla.wwl.WWLVars.WWLLevelerFlexSpeed;
import com.mla.wwl.WWLVars.WWLLevelerLockedOrientation;
import com.mla.wwl.Utils.CameraUtils;
import com.mla.wwl.Utils.Utils;
import com.mla.wwl.events.SensorNotRespondingEvent;
import com.mla.wwl.events.SensorRespondedEvent;
import com.mla.wwl.hEngine.AcceleSensorEvent;
import com.mla.wwl.hEngine.AngleQueue;
import com.mla.wwl.hEngine.CalibrationData;
import com.mla.wwl.hEngine.ClockConverter;
import com.mla.wwl.hEngine.FrameParamsCalculator;

import java.io.File;

public class FrameParametersManager {
    private AngleQueue a;
    private ClockConverter b;
    private FrameParamsCalculator c;
    private WWLLevelerCropMode d;
    private WWLLevelerFlexSpeed e;
    private WWLLevelerLockedOrientation f;
    private int g;
    private boolean h;
    private float i;
    private float j;
    private SensorManager k;
    private HandlerThread l;
    private boolean m;
    private Handler n;
    private boolean o;
    private boolean p;
    private int q;
    private File r;
    private WWLCamera s;
    private EventBus t;
    private SensorEventListener u;
    private Runnable v;

    public FrameParametersManager(Context context, WWLCamera WWLCameraInstance, EventBus eventBus) {
        this.d = WWLLevelerCropMode.FLEX;
        this.e = WWLLevelerFlexSpeed.SMOOTH;
        this.f = WWLLevelerLockedOrientation.AUTO;
        this.i = 1.0F;
        this.j = 1.0F;
        this.o = false;
        this.p = false;
        this.q = -1;
        this.u = new SensorEventListener() {
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }

            public void onSensorChanged(SensorEvent event) {
                FrameParametersManager.this.a(new AcceleSensorEvent(event));
                FrameParametersManager.this.o = true;
                if (!FrameParametersManager.this.p) {
                    FrameParametersManager.this.b();
                    FrameParametersManager.this.p = true;
                }

            }
        };
        this.v = new Runnable() {
            public void run() {
                if (!FrameParametersManager.this.o) {
                    FrameParametersManager.this.a();
                }

            }
        };
        this.s = WWLCameraInstance;
        this.t = eventBus;
        this.k = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        this.l = new HandlerThread("sensorThread");
        this.l.start();
        this.n = new Handler(this.l.getLooper());
        this.g = 0;
    }

    public void release() {
        this.stopRunning();
        this.l.quit();
        this.l = null;
        this.n = null;
        this.t = null;
        this.s = null;
    }

    public void stopRunning() {
        this.k.unregisterListener(this.u);
        this.n.removeCallbacks(this.v);
        this.m = false;
    }

    public synchronized void startRunning() {
        if (this.q == -1) {
            this.q = Utils.isGravitySensorAvailable(this.k) ? 9 : 1;
        }

        CalibrationData var1 = new CalibrationData(this.r);
        float var2 = (float) Math.toRadians((double) CameraUtils.getCameraToSensorAngle(this.g));
        var1.setCameraToSensorAngle(var2);
        this.a = new AngleQueue(var1);
        this.b = new ClockConverter();
        this.c = new FrameParamsCalculator(MlaSDKInt.SCALE_FILTER_FREQUENCY);
        this.o = false;
        this.p = false;
        this.n.postDelayed(this.v, 5000L);
        int var3 = Utils.isNexus6p() ? 1 : 10000;
        this.k.registerListener(this.u, this.k.getDefaultSensor(this.q), var3, this.n);
        this.m = true;
    }

    public synchronized void onCameraOpened(int cameraId) {
        this.g = cameraId;
        if (this.a != null) {
            float var2 = (float) Math.toRadians((double) CameraUtils.getCameraToSensorAngle(cameraId));
            this.a.setCameraToSensorAngle(var2);
        }
    }

    public synchronized void setCropMode(WWLLevelerCropMode cropMode) {
        this.d = cropMode;
    }

    public WWLLevelerCropMode getCropMode() {
        return this.d;
    }

    public synchronized void setFlexSpeed(WWLLevelerFlexSpeed flexSpeed) {
        this.e = flexSpeed;
    }

    public WWLLevelerFlexSpeed getFlexSpeed() {
        return this.e;
    }

    public synchronized void setLockedOrientation(WWLLevelerLockedOrientation orientation) {
        this.f = orientation;
    }

    public WWLLevelerLockedOrientation getLockedOrientation() {
        return this.f;
    }

    public synchronized void setIsRecording(boolean isRecording) {
        this.h = isRecording;
    }

    public synchronized void setCameraResolution(Size cameraSize) {
        this.i = cameraSize.getAspectRatio();
    }

    public synchronized void setOutputSize(Size outputSize) {
        this.j = outputSize.getAspectRatio();
    }

    public synchronized void setSensor(int sensorType, File calibrationFile) {
        if (sensorType == -1 && calibrationFile != null) {
            throw new RuntimeException("Sensor type must be specified for given calibration file.");
        } else {
            this.q = sensorType;
            this.r = calibrationFile;
            if (this.m) {
                this.stopRunning();
                this.startRunning();
            }

        }
    }

    private synchronized void a(AcceleSensorEvent var1) {
        var1.timestamp = this.b.convertToUnixTimeNano(var1.timestamp);
        this.a.enqueueEvent(var1);
    }

    public synchronized float[] getFrameParamsForTimestamp(long timestamp) {
        float[] var3;
        float[] var4;
        if (this.a == null) {
            var3 = new float[]{0.0F, 1.0F};
        } else {
            var4 = this.a.anglesForTimestamp(timestamp);
            boolean var5 = false;
            boolean var6 = this.e == WWLLevelerFlexSpeed.SMOOTH;
            var3 = this.c.getFrameParamsForData(timestamp, var4[0], var4[1], this.j, this.i, this.d, this.f, var5, var6, this.h);
        }

        var4 = new float[]{var3[0], var3[1]};
        return var4;
    }

    public synchronized float[] getPhotoFrameParamsForTimestamp(long timestamp) {
        if (this.a == null) {
            return new float[]{0.0F, 1.0F};
        } else {
            float[] var3 = this.a.anglesForTimestamp(timestamp);
            boolean var4 = false;
            boolean var5 = this.e == WWLLevelerFlexSpeed.SMOOTH;
            float[] var6 = this.c.getPhotoFrameParamsForData(timestamp, var3[0], var3[1], this.j, this.i, this.d, this.f, var4, var5, this.h);
            return var6;
        }
    }

    private void a() {
        this.t.post(new SensorNotRespondingEvent());
    }

    private void b() {
        this.t.post(new SensorRespondedEvent());
    }
}
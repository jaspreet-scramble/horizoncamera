package com.mla.wwl;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mla.wwl.Utils.FileUtils;
import com.mla.wwl.Utils.Utils;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class CameraHelper {
  private HashMap<Integer, Parameters> a;
  private final int b;
  private static Type c = (new TypeToken<HashMap<Integer, Parameters>>() {
  }).getType();

  public CameraHelper(File cacheFile) throws IOException {
    Gson var2 = (new GsonBuilder()).enableComplexMapKeySerialization().create();
    if(cacheFile != null && cacheFile.exists()) {
      this.a = (HashMap)FileUtils.loadObjectFromGsonFile(cacheFile, c, var2);
    }

    if(this.a == null) {
      this.a = new HashMap(2);
      int var3 = Camera.getNumberOfCameras();
      CameraInfo var4 = new CameraInfo();

      for(int var5 = 0; var5 < var3; ++var5) {
        Camera.getCameraInfo(var5, var4);
        Camera var6 = null;

        try {
          var6 = Camera.open(var5);
        } catch (RuntimeException var8) {
          ;
        }

        if(var6 == null) {
          throw new IOException("Camera could not be opened");
        }

        this.a.put(Integer.valueOf(var4.facing), var6.getParameters());
        var6.release();
      }

      if(cacheFile != null) {
        FileUtils.saveObjectToGsonFile(this.a, cacheFile, var2);
      }
    }

    this.b = Camera.getNumberOfCameras();
  }

  public void printCameraParameters() {
    System.out.println("Number of cameras: " + this.getNumberOfCameras());
    if(this.hasCamera(0)) {
      System.out.println("Back facing camera:");
      this.printCameraParameters(0);
    }

    if(this.hasCamera(1)) {
      System.out.println("Front facing camera:");
      this.printCameraParameters(1);
    }

  }

  public void printCameraParameters(int facing) {
    List var2 = this.getSupportedVideoSize(facing);
    List var3 = this.getSupportedPhotoSizes(facing);
    System.out.println("Video sizes: " + var2);
    System.out.println("Photo sizes: " + var3);
  }

  public int getNumberOfCameras() {
    return this.b;
  }

  public boolean hasCamera(int cameraFacing) {
    return this.a.get(Integer.valueOf(cameraFacing)) != null;
  }

  private static Size a(android.hardware.Camera.Size var0) {
    return new Size(var0.width, var0.height);
  }

  private static List<Size> a(List<android.hardware.Camera.Size> var0) {
    ArrayList var1 = new ArrayList(var0.size());
    Iterator var2 = var0.iterator();

    while(var2.hasNext()) {
      android.hardware.Camera.Size var3 = (android.hardware.Camera.Size)var2.next();
      var1.add(a(var3));
    }

    return var1;
  }

  public List<Size> getSupportedVideoSize(int cameraFacing) {
    return !this.hasCamera(cameraFacing)?null:getSupportedVideoSizes((Parameters)this.a.get(Integer.valueOf(cameraFacing)));
  }

  public static List<Size> getSupportedVideoSizes(Parameters params) {
    List var1 = params.getSupportedPreviewSizes();
    List var2 = params.getSupportedVideoSizes();
    Object var3;
    if(var2 == null) {
      var3 = var1;
    } else {
      Object var4 = new ArrayList();
      Iterator var5 = var2.iterator();

      while(var5.hasNext()) {
        android.hardware.Camera.Size var6 = (android.hardware.Camera.Size)var5.next();
        Iterator var7 = var1.iterator();

        while(var7.hasNext()) {
          android.hardware.Camera.Size var8 = (android.hardware.Camera.Size)var7.next();
          if(var8.width == var6.width && var8.height == var6.height) {
            ((List)var4).add(var8);
          }
        }
      }

      if(((List)var4).size() == 0) {
        var4 = var1;
      }

      var3 = var4;
    }

    if(Utils.isKindleFireHD67()) {
      Iterator var9 = ((List)var3).iterator();

      while(var9.hasNext()) {
        android.hardware.Camera.Size var11 = (android.hardware.Camera.Size)var9.next();
        if(var11.width == 1920 && var11.height == 1080) {
          var9.remove();
        }
      }
    }

    List var10 = a((List)var3);
    Collections.sort(var10, Collections.reverseOrder());
    return var10;
  }

  public List<Size> getSupportedPhotoSizes(int cameraFacing) {
    return !this.hasCamera(cameraFacing)?null:getSupportedPhotoSizes((Parameters)this.a.get(Integer.valueOf(cameraFacing)));
  }

  public static List<Size> getSupportedPhotoSizes(Parameters params) {
    List var1 = a(params.getSupportedPictureSizes());
    Collections.sort(var1, Collections.reverseOrder());
    return var1;
  }

  public List<Size> getPhotoSizesForVideoSize(int cameraFacing, Size videoSize) {
    return !this.hasCamera(cameraFacing)?null:getPhotoSizesForVideoSize((Parameters)this.a.get(Integer.valueOf(cameraFacing)), videoSize);
  }

  public static List<Size> getPhotoSizesForVideoSize(Parameters params, Size videoSize) {
    ArrayList var2 = new ArrayList();
    List var3 = getSupportedPhotoSizes(params);
    float var4 = videoSize.getAspectRatio();
    Iterator var5 = var3.iterator();

    Size var6;
    float var7;
    while(var5.hasNext()) {
      var6 = (Size)var5.next();
      var7 = var6.getAspectRatio();
      if(Math.abs(var7 - var4) < 0.001F) {
        var2.add(var6);
      }
    }

    if(var2.size() == 0) {
      var5 = var3.iterator();

      while(var5.hasNext()) {
        var6 = (Size)var5.next();
        var7 = var6.getAspectRatio();
        if(Math.abs(var7 - var4) < 0.1F) {
          var2.add(var6);
        }
      }
    }

    if(var2.size() == 0) {
      Size var10 = null;
      float var11 = 2.0F;
      Iterator var12 = var3.iterator();

      while(var12.hasNext()) {
        Size var8 = (Size)var12.next();
        float var9 = var8.getAspectRatio();
        if(Math.abs(var9 - var4) < var11) {
          var10 = var8;
          var11 = Math.abs(var9 - var4);
        }
      }

      var2.add(var10);
    }

    return var2;
  }

  public List<Size> getVideoSizesForPhotoSize(int cameraFacing, Size photoSize) {
    return !this.hasCamera(cameraFacing)?null:getVideoSizesForPhotoSize((Parameters)this.a.get(Integer.valueOf(cameraFacing)), photoSize);
  }

  public static List<Size> getVideoSizesForPhotoSize(Parameters params, Size photoSize) {
    ArrayList var2 = new ArrayList();
    List var3 = getSupportedVideoSizes(params);
    float var4 = photoSize.getAspectRatio();
    Iterator var5 = var3.iterator();

    Size var6;
    float var7;
    while(var5.hasNext()) {
      var6 = (Size)var5.next();
      var7 = var6.getAspectRatio();
      if(Math.abs(var7 - var4) < 0.001F) {
        var2.add(var6);
      }
    }

    if(var2.size() == 0) {
      var5 = var3.iterator();

      while(var5.hasNext()) {
        var6 = (Size)var5.next();
        var7 = var6.getAspectRatio();
        if(Math.abs(var7 - var4) < 0.1F) {
          var2.add(var6);
        }
      }
    }

    return var2;
  }

  public Size[] getDefaultVideoAndPhotoSize(int cameraFacing) {
    return this.getDefaultVideoAndPhotoSize(cameraFacing, false);
  }

  public Size[] getDefaultVideoAndPhotoSize(int cameraFacing, boolean priorityPhoto) {
    return !this.hasCamera(cameraFacing)?null:getDefaultVideoAndPhotoSize((Parameters)this.a.get(Integer.valueOf(cameraFacing)), priorityPhoto);
  }

  public static Size[] getDefaultVideoAndPhotoSize(Parameters params, boolean priorityPhoto) {
    List var2;
    Size var3;
    List var4;
    Size var5;
    if(priorityPhoto) {
      var2 = getSupportedPhotoSizes(params);
      var3 = (Size)var2.get(0);
      var4 = getVideoSizesForPhotoSize(params, var3);
      if(var4.size() == 0) {
        var4 = getSupportedVideoSizes(params);
      }

      var5 = (Size)var4.get(0);
      return new Size[]{var5, var3};
    } else {
      var2 = getSupportedVideoSizes(params);
      var3 = (Size)var2.get(0);
      var4 = getPhotoSizesForVideoSize(params, var3);
      if(var4.size() == 0) {
        var4 = getSupportedPhotoSizes(params);
      }

      var5 = (Size)var4.get(0);
      return new Size[]{var3, var5};
    }
  }

  public List<String> getSupportedFlashModes(int cameraFacing) {
    return !this.hasCamera(cameraFacing)?null:getSupportedFlashModes((Parameters)this.a.get(Integer.valueOf(cameraFacing)));
  }

  public static List<String> getSupportedFlashModes(Parameters params) {
    return params.getSupportedFlashModes();
  }

  public Parameters getCameraParameters(int cameraFacing) {
    return (Parameters)this.a.get(Integer.valueOf(cameraFacing));
  }
}
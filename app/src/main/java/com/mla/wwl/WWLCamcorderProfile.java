package com.mla.wwl;

import android.media.CamcorderProfile;

public class WWLCamcorderProfile {
  public int videoFrameWidth;
  public int videoFrameHeight;
  public int videoBitrate;
  public int videoIFrameInterval;
  public int audioChannels;
  public int audioSampleRate;
  public int audioBitrate;
  public static final int QUALITY_HIGH = 0;
  public static final int QUALITY_MEDIUM = 1;
  public static final int QUALITY_LOW = 2;

  public WWLCamcorderProfile() {
  }

  public WWLCamcorderProfile(CamcorderProfile profile) {
    this.videoFrameWidth = profile.videoFrameWidth;
    this.videoFrameHeight = profile.videoFrameHeight;
    this.videoBitrate = profile.videoBitRate;
    this.videoIFrameInterval = 3;
    this.audioChannels = profile.audioChannels;
    this.audioSampleRate = profile.audioSampleRate;
    this.audioBitrate = profile.audioBitRate;
  }

  public WWLCamcorderProfile(int width, int height) {
    this(width, height, QUALITY_MEDIUM);
  }

  public WWLCamcorderProfile(int width, int height, int videoQuality) {
    this.videoFrameWidth = width;
    this.videoFrameHeight = height;
    this.videoBitrate = calculateVideoBitrate(width, height, videoQuality);
    this.videoIFrameInterval = 3;
    CamcorderProfile var4 = CamcorderProfile.get(0, QUALITY_MEDIUM);
    this.audioChannels = var4.audioChannels;
    this.audioSampleRate = var4.audioSampleRate;
    this.audioBitrate = var4.audioBitRate;
  }

  public static int calculateVideoBitrate(int width, int height, int quality) {
    float var3;
    if(quality == 0) {
      var3 = 1.0F;
    } else if(quality == 1) {
      var3 = 0.7F;
    } else {
      var3 = 0.4F;
    }

    float var4 = 8.680555F;
    int var5 = (int)(var4 * (float)width * (float)height * var3);
    if(var5 > 20000000) {
      var5 = 20000000;
    }

    return var5;
  }
}

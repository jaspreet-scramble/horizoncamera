package com.mla.wwl;

import android.hardware.Camera.Parameters;

interface a {
  void onViewAttached(b var1);

  void onViewDetached();

  c getRenderer();

  void setCameraParams(Parameters var1);

  void onCameraReleased();

  void onAutoFocusSuccessful(boolean var1);

  void onFocusSetToDefault();
}

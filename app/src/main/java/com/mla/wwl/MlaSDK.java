package com.mla.wwl;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.mla.wwl.Utils.Utils;

import scrambleapps.live.R;

public final class MlaSDK {
    public MlaSDK() {
    }

    public static void init(Context context) {
        MlaSDKInt.WATERMARK_DRAWABLE = context.getResources().getDrawable(R.drawable.wwl_watermark_drawable);
        if (MlaSDKInt.WATERMARK_DRAWABLE == null) {
            throw new RuntimeException("The wwl_watermark property does not point to a valid drawable");
        } else {
            MlaSDKInt.WATERMARK_BTMP = BitmapFactory.decodeResource(context.getResources(), R.drawable.wwl_watermark_drawable);
            MlaSDKInt.WATERMARK_SCALE = Utils.getFloatFromDimension(context, R.dimen.wwl_watermark_scale);
            MlaSDKInt.WATERMARK_OPACITY = Utils.getFloatFromDimension(context, R.dimen.wwl_watermark_opacity);
            MlaSDKInt.SCALE_FILTER_FREQUENCY = Utils.getFloatFromDimension(context, R.dimen.wwl_scale_filter_frequency);
        }
    }

    protected static boolean initialized() {
        return true;
    }
}


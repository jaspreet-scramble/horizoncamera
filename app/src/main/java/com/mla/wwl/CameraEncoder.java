package com.mla.wwl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.hardware.Camera;
import android.hardware.Camera.Area;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.CameraProfile;
import android.media.ExifInterface;
import android.opengl.EGLContext;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import com.mla.wwl.WWLVars.CameraMode;
import com.mla.wwl.WWLView.ViewType;
import com.mla.wwl.Utils.BitmapUtils;
import com.mla.wwl.Utils.CameraUtils;
import com.mla.wwl.Utils.ExifUtils;
import com.mla.wwl.Utils.FileUtils;
import com.mla.wwl.Utils.Utils;
import com.mla.wwl.av.Muxer;
import com.mla.wwl.av.VideoEncoderCore;
import com.mla.wwl.events.FrameDroppedEvent;
import com.mla.wwl.events.PhotoCapturedEvent;
import com.mla.wwl.events.PreviewFailedToStartEvent;
import com.mla.wwl.events.PreviewHasBeenRunningEvent;
import com.mla.wwl.events.PreviewStartedEvent;
import com.mla.wwl.events.PreviewStoppedEvent;
import com.mla.wwl.events.PreviewWillStopEvent;
import com.mla.wwl.events.SnapshotCapturedEvent;
import com.mla.wwl.gles.EglCore;
import com.mla.wwl.gles.EglSurfaceBase;
import com.mla.wwl.gles.Filters;
import com.mla.wwl.gles.FullFrameRect;
import com.mla.wwl.gles.GlUtil;
import com.mla.wwl.gles.Texture2dProgram;
import com.mla.wwl.gles.Texture2dProgram.ProgramType;
import com.mla.wwl.gles.WindowSurface;
import com.mla.wwl.hEngine.ClockConverter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.List;

public class CameraEncoder implements OnFrameAvailableListener, com.mla.wwl.b, Runnable {
    public final int TAP_TO_FOCUS_TIMEOUT = 2000;
    public final int TAP_TO_FOCUS_FAIL_TIMEOUT = 3000;
    private com.mla.wwl.a b;
    private Parameters c;
    private int d;
    private Size e;
    private Size f;
    private Size g;
    private CameraMode h;
    private boolean i;
    private String j;
    private WWLCamera k;
    private Camera l;
    private int m;
    private int n;
    private boolean o;
    private Size p;
    private Size q;
    private int r;
    private Size s;
    private CameraMode t;
    private boolean u;
    private boolean v;
    private boolean w;
    private boolean x;
    private int y;
    private int z;
    private String A;
    private String B;
    private boolean C;
    private WindowSurface D;
    private EglCore E;
    private EglSurfaceBase F;
    private FullFrameRect G;
    private int H;
    private float I;
    private boolean J;
    private SurfaceTexture K;
    private int L;
    private com.mla.wwl.c M;
    private long N;
    private ClockConverter O;
    private VideoEncoderCore P;
    private float[] Q;
    private int R;
    private int S;
    private boolean T;
    private long U;
    private HandlerThread V;
    private Handler W;
    private Handler X;
    private boolean Y;
    private Handler Z;
    private boolean aa;
    private boolean ab;
    private boolean ac;
    private boolean ad;
    private volatile CameraEncoder.a ae;
    private CameraEncoder.c af;
    private final Object ag;
    private boolean ah;
    private boolean ai;
    private final Object aj;
    private boolean ak;
    private Context al;
    private EventBus am;
    public FrameParametersManager mFrameParametersManager;
    d a;
    private long an;
    private Rect ao;

    public CameraEncoder(Context context, EventBus eventBus, FrameParametersManager frameParameersManager, WWLCamera WWLCamera) {
        this.h = CameraMode.AUTO;
        this.Q = new float[16];
        this.ag = new Object();
        this.aj = new Object();
        this.a = new d() {
            public void a(com.mla.wwl.c var1) {
                CameraEncoder.this.c(var1);
            }

            public void b(com.mla.wwl.c var1) {
                var1.destroyGL();
            }
        };
        this.al = context;
        this.am = eventBus;
        this.k = WWLCamera;
        this.t = this.h;
        this.n = -1;
        this.m = -1;
        this.d = -1;
        this.A = "off";
        this.B = this.A;
        this.j = this.B;
        this.R = -1;
        this.S = 0;
        this.V = new HandlerThread("com.mla.wwl.CameraEncoder - Photo processing");
        this.V.start();
        this.W = new Handler(this.V.getLooper());
        this.mFrameParametersManager = frameParameersManager;
        this.X = new Handler(Looper.getMainLooper());
        this.a();
    }

    public void prepareRecording(Muxer muxer, WWLCamcorderProfile profile) {
        CameraEncoder.b var3 = new CameraEncoder.b(null);
        var3.a = muxer;
        var3.b = profile;
        this.ae.sendMessage(this.ae.obtainMessage(8, var3));
    }

    private void a(Muxer var1, WWLCamcorderProfile var2) {
        this.ab = false;
        this.ac = false;
        this.ad = false;
        Size var3 = new Size(var2.videoFrameWidth, var2.videoFrameHeight);
        if (!var3.equals(this.s)) {
            Log.w("TAG", "The video size of the recording profile is different than output movie size");
        }

        this.a(this.s, this.r / 1000, var2.videoBitrate, var2.videoIFrameInterval, var1);
    }

    public void setScreenAngle(int degrees) {
        this.ae.sendMessage(this.ae.obtainMessage(15, Integer.valueOf(degrees)));
    }

    private void a(int var1) {
        this.H = var1;
        if (this.m != -1) {
            int var2 = CameraUtils.getCameraToScreenAngle(this.m, this.H);
            this.I = (float) Math.toRadians((double) var2);
            if (this.M != null && this.M.isRendererReady()) {
                this.M.setCameraToScreenAngle(var2);
            }

        }
    }

    public void setWatermarkEnabled(boolean enabled) {
        this.ae.sendMessage(this.ae.obtainMessage(16, Boolean.valueOf(enabled)));
    }

    private void a(boolean var1) {
        this.J = var1;
        if (this.G != null) {
            this.G.setWatermarkEnabled(this.J);
        }

    }

    public void startRecording() {
        if (this.ae != null) {
            this.ae.sendMessage(this.ae.obtainMessage(1));
        }

    }

    public void capturePhoto(File photoFile) {
        if (this.ae != null) {
            this.ae.sendMessage(this.ae.obtainMessage(11, photoFile));
        }

    }

    public void captureSnapshot(File snapshotFile) {
        if (this.ae != null) {
            this.ae.sendMessage(this.ae.obtainMessage(12, snapshotFile));
        }

    }

    private void a() {
        Object var1 = this.ag;
        synchronized (this.ag) {
            if (this.ai) {
                Log.w("CameraEncoder", "Encoder thread running when start requested");
            } else {
                this.ai = true;
                Thread var2 = new Thread(this, "com.mla.wwl.CameraEncoder");
                var2.setPriority(7);
                var2.start();

                while (!this.ah) {
                    try {
                        this.ag.wait();
                    } catch (InterruptedException var5) {
                        ;
                    }
                }

            }
        }
    }

    public void stopRecording() {
        if (this.ae != null) {
            this.ae.sendMessage(this.ae.obtainMessage(2));
        }

    }

    private void b() {
        this.ab = true;
        this.l();
        this.mFrameParametersManager.setIsRecording(this.ab);
    }

    private void a(final File var1) {
        if (this.ab) {
            throw new RuntimeException("Can't capture photo while recording");
        } else {
            final float[] var2 = this.mFrameParametersManager.getPhotoFrameParamsForTimestamp(this.U);
            this.l.takePicture((ShutterCallback) null, (PictureCallback) null, (PictureCallback) null, new PictureCallback() {
                public void onPictureTaken(final byte[] data, Camera camera) {
                    CameraEncoder.this.W.post(new Runnable() {
                        public void run() {
                            FileOutputStream var1x = null;
                            boolean var2x = false;

                            try {
                                var1x = new FileOutputStream(var1);
                                Bitmap var3 = BitmapUtils.makeBitmap(data, 0);
                                if (var3 == null) {
                                    throw new NullPointerException();
                                }

                                float var4 = CameraEncoder.this.o ? 1.0F : -1.0F;
                                float var5 = CameraEncoder.this.s.getAspectRatio();
                                Bitmap var6 = BitmapUtils.levelBitmap(var3, var5, var4 * var2[0], var2[1]);
                                var3.recycle();
                                if (CameraEncoder.this.J) {
                                    BitmapUtils.addWatermark(var6);
                                }

                                var6.compress(CompressFormat.JPEG, 97, var1x);
                                var6.recycle();
                                var1x.flush();
                                var1x.close();

                                try {
                                    ExifInterface var7 = new ExifInterface(var1.getAbsolutePath());
                                    ExifUtils.copyExifFromData(data, var7, CameraEncoder.this.al);
                                } catch (IOException var8) {
                                    ;
                                }

                                var2x = true;
                            } catch (Exception var9) {
                                Log.e("CameraEncoder", "Failed to write data", var9);
                                var2x = false;
                            }

                            CameraEncoder.this.am.post(new PhotoCapturedEvent(var1, var2x));
                        }
                    });
                    camera.startPreview();
                }
            });
        }
    }

    private boolean c() {
        boolean var1 = this.d();
        boolean var2 = false;
        if (var1 == this.v) {
            return var2;
        } else {
            this.v = var1;
            if (!Utils.isNexus5()) {
                this.l.stopPreview();
            }

            Parameters var3 = this.l.getParameters();
            var3.setRecordingHint(this.v);
            int[] var4 = CameraUtils.getMaxSupportedPreviewFpsRange(var3, this.v);
            var3.setPreviewFpsRange(var4[0], var4[1]);

            try {
                this.l.setParameters(var3);
            } catch (Exception var6) {
                Log.e("CameraEncoder", "Failed to set recording hint", var6);
            }

            if (Utils.isNexus5()) {
                this.l.stopPreview();
            }

            this.l.startPreview();
            var2 = true;
            return var2;
        }
    }

    private boolean d() {
        boolean var1 = false;
        if (this.t == CameraMode.PICTURE) {
            var1 = false;
        } else if (this.t == CameraMode.VIDEO) {
            var1 = true;
        } else if (this.t == CameraMode.AUTO) {
            if (this.ab) {
                var1 = true;
            } else {
                var1 = false;
            }
        }

        if (this.u) {
            var1 = false;
        }

        return var1;
    }

    private void e() {
        this.ac = true;
    }

    public void release() {
        if (this.b != null) {
            this.b.onViewDetached();
            this.b = null;
        }

        Object var1 = this.ag;
        synchronized (this.ag) {
            if (this.ae != null) {
                this.ae.sendMessage(this.ae.obtainMessage(7));

                while (this.ai) {
                    try {
                        this.ag.wait();
                    } catch (InterruptedException var4) {
                        var4.printStackTrace();
                    }
                }
            }
        }

        if (this.M != null) {
            this.M.onRendererDetached();
            this.M = null;
        }

    }

    private void f() {
        this.g();
    }

    private void g() {
        if (this.P != null) {
            throw new RuntimeException("Encoder wasn't released");
        } else {
            if (this.M != null && this.M.isRendererReady()) {
                this.M.destroyGL();
            }

            this.i();
            this.W.removeCallbacksAndMessages((Object) null);
            this.V.quit();
            Looper.myLooper().quit();
        }
    }

    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        if (Thread.currentThread() == this.ae.getLooper().getThread()) {
            this.a(surfaceTexture);
        } else {
            this.ae.sendMessage(this.ae.obtainMessage(3, surfaceTexture));
        }

    }

    private void a(SurfaceTexture var1) {
        if (!this.C) {
            var1.updateTexImage();
        } else {
            ++this.N;
            if (this.N == 5L && this.aa) {
                this.am.post(new PreviewHasBeenRunningEvent(this.l.getParameters(), this.n));
            }

            if (!var1.equals(this.K)) {
                Log.w("CameraEncoder", "SurfaceTexture from OnFrameAvailable does not match saved SurfaceTexture!");
            }

            this.F.makeCurrent();
            var1.updateTexImage();
            this.K.getTransformMatrix(this.Q);
            long var2 = this.K.getTimestamp();
            var2 = this.O.convertToUnixTimeNano(var2);
            var2 -= 16666666L;
            if (var2 == 0L) {
                var2 = this.U;
            }

            long var4 = (var2 - this.U) / 1000L;
            if (var4 > 36000L && MlaSDKInt.SHOW_DROPPED_FRAMES) {
                this.am.post(new FrameDroppedEvent());
            }

            this.U = var2;
            float[] var6 = this.mFrameParametersManager.getFrameParamsForTimestamp(var2);
            float var7 = var6[0] + this.I;
            this.k.onAngleUpdated((float) Math.toDegrees((double) var7), 1.0F / var6[1]);
            if (this.M != null && this.M.isRendererReady()) {
                this.M.handleFrameAvailable(this.L, var2, this.Q, var6);
            }

            if (this.ab) {
                this.D.makeCurrent();
                if (this.R != this.S) {
                    Filters.updateFilter(this.G, this.S);
                    this.R = this.S;
                    this.T = true;
                }

                if (this.T) {
                    this.G.setSize(this.s.getWidth(), this.s.getHeight());
                    this.G.setSourceSize(this.p.getWidth(), this.p.getHeight(), this.o);
                    this.G.setOutputSize(this.s.getWidth(), this.s.getHeight());
                    this.T = false;
                }

                this.G.drawFrame(this.L, this.Q, var6[0], var6[1]);
                long var8 = this.O.convertUnixTimeToNanoTime(var2);
                this.D.setPresentationTime(var8);
                this.D.swapBuffers();
                if (this.ac) {
                    this.P.signalEOS();
                    this.ab = false;
                    this.ac = false;
                    this.ad = true;
                    this.P.waitUntilReleased();
                    this.P = null;
                    this.j();
                    this.l();
                    this.mFrameParametersManager.setIsRecording(this.ab);
                }
            }

            this.o();
        }
    }

    private void b(final File var1) {
        if (this.D != null) {
            this.D.makeCurrent();
            final ByteBuffer var2 = this.D.saveFrame();
            final int var3 = this.D.getWidth();
            final int var4 = this.D.getHeight();
            this.W.post(new Runnable() {
                public void run() {
                    File var1x = FileUtils.getSafeFile(var1);
                    BufferedOutputStream var2x = null;

                    try {
                        Bitmap var3x = BitmapUtils.bitmapFromGLBuffer(var2, var3, var4);
                        var2x = new BufferedOutputStream(new FileOutputStream(var1x));
                        var3x.compress(CompressFormat.JPEG, 90, var2x);
                        var3x.recycle();
                        if (CameraEncoder.this.am != null) {
                            CameraEncoder.this.am.post(new SnapshotCapturedEvent(var1x));
                        }
                    } catch (FileNotFoundException var12) {
                        var12.printStackTrace();
                    } finally {
                        if (var2x != null) {
                            try {
                                var2x.close();
                            } catch (IOException var11) {
                                var11.printStackTrace();
                            }
                        }

                    }

                }
            });
        }

    }

    public void stopRunning() {
        this.m();
        this.ae.sendMessage(this.ae.obtainMessage(6));
    }

    public void startRunning() {
        if (this.e == null) {
            throw new RuntimeException("startRunning() called before setting camera parameters");
        } else {
            CameraEncoder.WWLCameraParameters var1 = new CameraEncoder.WWLCameraParameters(this.d, this.e, this.f);
            this.ae.sendMessage(this.ae.obtainMessage(5, var1));
        }
    }

    public void run() {
        Looper.prepare();
        Object var1 = this.ag;
        synchronized (this.ag) {
            this.ae = new CameraEncoder.a(this);
            this.Z = new Handler();
            this.af = new CameraEncoder.c(this);
            this.h();
            this.ae.post(new Runnable() {
                public void run() {
                    synchronized (CameraEncoder.this.ag) {
                        CameraEncoder.this.ah = true;
                        CameraEncoder.this.ag.notify();
                    }
                }
            });
        }

        Looper.loop();
        this.k = null;
        this.mFrameParametersManager = null;
        var1 = this.ag;
        synchronized (this.ag) {
            this.ah = this.ai = false;
            this.ae = null;
            this.Z = null;
            this.W = null;
            this.af = null;
            this.ag.notify();
        }
    }

    private void h() {
        this.E = new EglCore((EGLContext) null, 1);
        this.F = new EglSurfaceBase(this.E);
        this.F.createOffscreenSurface(2, 2);
        this.F.makeCurrent();
        this.L = GlUtil.createTextureObject('赥');
        this.K = new SurfaceTexture(this.L);
        this.K.setOnFrameAvailableListener(this);
        this.C = true;
    }

    private void a(Size var1, int var2, int var3, int var4, Muxer var5) {
        if (this.P != null) {
            throw new RuntimeException("Encoder wasn't released");
        } else {
            this.P = new VideoEncoderCore(var1.getWidth(), var1.getHeight(), var2, var3, var4, var5);
            if (this.D == null && this.G == null) {
                this.D = new WindowSurface(this.E, this.P.getInputSurface(), true);
                this.D.makeCurrent();
                this.G = new FullFrameRect(new Texture2dProgram(ProgramType.TEXTURE_EXT), false, ViewType.LEVELED, 1.0F);
                this.G.setWatermarkEnabled(this.J);
                this.T = true;
            } else {
                throw new RuntimeException("Encoder EglResources were not released.");
            }
        }
    }

    private void i() {
        this.C = false;
        this.j();
        if (this.K != null) {
            this.F.makeCurrent();
            GlUtil.deleteTexture(this.L);
            this.K.release();
            this.K = null;
            this.F.releaseEglSurface();
            this.F = null;
            this.E.release();
            this.E = null;
        }

    }

    private void j() {
        if (this.D != null) {
            this.G.release();
            this.G = null;
            this.D.release();
            this.D = null;
        }

    }

    private void a(CameraEncoder.WWLCameraParameters var1) {
        Camera var2 = this.a(var1.cameraFacing, var1.videoSize, var1.pictureSize);
        if (var2 == null) {
            this.am.post(new PreviewFailedToStartEvent());
        } else {
            try {
                this.l.setPreviewTexture(this.K);
                int var3 = CameraUtils.getCameraToScreenAngle(this.m, this.H);
                this.I = (float) Math.toRadians((double) var3);
                if (this.M != null && this.M.isRendererReady()) {
                    this.M.setCameraToScreenAngle(var3);
                    this.M.setSourceSize(this.p.getWidth(), this.p.getHeight(), this.o);
                    this.M.setOutputSize(this.s.getWidth(), this.s.getHeight());
                }

                this.mFrameParametersManager.setCameraResolution(this.p);
                this.mFrameParametersManager.onCameraOpened(this.m);
                this.O = new ClockConverter();
                this.l.startPreview();
                this.N = 0L;
                this.aa = true;
                this.setCameraDefaultFocusMode(true);
                Parameters var4 = this.l.getParameters();
                this.b(var4);
                this.x = CameraUtils.isTapToFocusSupported(var4);
                this.w = var4.isZoomSupported();
                this.y = 0;
                this.z = 0;
                this.am.post(new PreviewStartedEvent(this.m, var4, this.n));
            } catch (IOException var5) {
                var5.printStackTrace();
                this.k();
                this.am.post(new PreviewFailedToStartEvent());
            }

        }
    }

    private Camera a(int var1, Size var2, Size var3) {
        if (this.l != null) {
            throw new RuntimeException("camera already initialized");
        } else {
            CameraInfo var4 = new CameraInfo();
            int var5 = Camera.getNumberOfCameras();
            int var6 = var1;
            boolean var7 = false;

            label57:
            while (!var7) {
                for (int var8 = 0; var8 < var5; ++var8) {
                    Camera.getCameraInfo(var8, var4);
                    if (var4.facing == var6) {
                        this.b(var8);
                        this.n = var6;
                        this.m = var8;
                        break label57;
                    }
                }

                if (this.l == null) {
                    if (var6 == var1) {
                        var6 = var1 == 0 ? 1 : 0;
                    } else {
                        var7 = true;
                    }
                }
            }

            if (this.l == null) {
                this.n = -1;
                this.m = -1;
                return null;
            } else {
                this.o = this.n == 0;
                Parameters var11 = this.l.getParameters();
                if (CameraUtils.isValidFlashMode(var11.getSupportedFlashModes(), this.B)) {
                    var11.setFlashMode(this.B);
                    this.A = this.B;
                }

                if (var11.isVideoStabilizationSupported()) {
                    var11.setVideoStabilization(true);
                }

                this.v = this.d();
                var11.setRecordingHint(this.v);
                int[] var9 = CameraUtils.getMaxSupportedPreviewFpsRange(var11, this.v);
                if (var9 == null) {
                    throw new RuntimeException("could not get a valid preview fps range");
                } else {
                    this.r = var9[1];
                    var11.setPreviewFpsRange(var9[0], var9[1]);
                    var11.setPictureSize(var3.getWidth(), var3.getHeight());
                    var11.setPreviewSize(var2.getWidth(), var2.getHeight());
                    this.p = var2;
                    this.q = var3;
                    int var10 = CameraProfile.getJpegEncodingQualityParameter(this.m, 2);
                    var11.setJpegQuality(var10);
                    this.l.setParameters(var11);
                    this.l.setDisplayOrientation(0);
                    return this.l;
                }
            }
        }
    }

    private void b(int var1) {
        int var2 = 0;

        while (var2 < 3) {
            try {
                this.l = Camera.open(var1);
                break;
            } catch (RuntimeException var6) {
                try {
                    Thread.sleep(500L);
                } catch (InterruptedException var5) {
                    Thread.currentThread().interrupt();
                }

                ++var2;
            }
        }

    }

    private void k() {
        if (this.l != null) {
            if (this.aa) {
                this.am.post(new PreviewWillStopEvent());
            }

            this.l.stopPreview();
            this.aa = false;
            this.l.release();
            this.l = null;
            this.m = -1;
            this.v = false;
            this.x = false;
            this.Z.removeCallbacksAndMessages((Object) null);
            this.w = false;
            this.af.removeCallbacksAndMessages((Object) null);
            this.am.post(new PreviewStoppedEvent());
        }

    }

    public void attachPreviewView(com.mla.wwl.a view) {
        Preconditions.checkNotNull(view);
        if (view == this.b) {
            throw new RuntimeException("PreviewView is already added.");
        } else {
            this.b = view;
            this.b.onViewAttached(this);
            if (this.c != null) {
                this.b.setCameraParams(this.c);
            }

            com.mla.wwl.c var2 = view.getRenderer();
            var2.onRendererWillBeAttached(this.a, this.ae.getLooper());
            this.ae.sendMessage(this.ae.obtainMessage(13, var2));
        }
    }

    private void a(com.mla.wwl.c var1) {
        if (var1 == this.M) {
            throw new RuntimeException("PreviewRenderer is already added.");
        } else {
            this.M = var1;
            if (this.M.isSurfaceReady()) {
                this.c(this.M);
            }

        }
    }

    public void detachPreviewView(WWLView view) {
        Preconditions.checkNotNull(view);
        if (view != this.b) {
            throw new RuntimeException("Preview was not added.");
        } else {
            this.b.onViewDetached();
            this.b = null;
            com.mla.wwl.c var2 = view.getRenderer();
            this.ak = false;
            this.ae.sendMessage(this.ae.obtainMessage(14, view.getRenderer()));
            Object var3 = this.aj;
            synchronized (this.aj) {
                while (!this.ak) {
                    try {
                        this.aj.wait();
                    } catch (InterruptedException var6) {
                        var6.printStackTrace();
                    }
                }
            }

            var2.onRendererDetached();
        }
    }

    private void b(com.mla.wwl.c var1) {
        if (var1 != this.M) {
            throw new RuntimeException("PreviewRenderer was not added.");
        } else {
            if (this.M.isRendererReady()) {
                this.M.destroyGL();
            }

            this.M = null;
            Object var2 = this.aj;
            synchronized (this.aj) {
                this.ak = true;
                this.aj.notifyAll();
            }
        }
    }

    private void c(com.mla.wwl.c var1) {
        var1.prepareGL(this.E);
        if (this.m != -1) {
            var1.setSourceSize(this.p.getWidth(), this.p.getHeight(), this.o);
            var1.setOutputSize(this.s.getWidth(), this.s.getHeight());
            var1.setCameraToScreenAngle(CameraUtils.getCameraToScreenAngle(this.m, this.H));
        }

    }

    public void setCamera(CameraEncoder.WWLCameraParameters params) {
        this.d = params.cameraFacing;
        this.e = params.videoSize;
        this.f = params.pictureSize;
        this.setOutputMovieSize(this.e);
        if (this.c != null) {
            this.m();
            this.ae.sendMessage(this.ae.obtainMessage(6));
            this.ae.sendMessage(this.ae.obtainMessage(5, params));
        }

    }

    public int getCameraFacing() {
        return this.d;
    }

    public Size getCameraVideoSize() {
        return this.e;
    }

    public Size getCameraPictureSize() {
        return this.f;
    }

    public void setOutputMovieSize(Size frameSize) {
        this.g = frameSize;
        this.ae.sendMessage(this.ae.obtainMessage(20, this.g));
    }

    public void handleSetOutputMovieSize(Size movieSize) {
        if (this.ab) {
            throw new RuntimeException("Can't change outputMovieSize while recording");
        } else {
            this.s = movieSize;
            this.mFrameParametersManager.setOutputSize(this.s);
            if (this.M != null && this.M.isRendererReady()) {
                this.M.setOutputSize(this.s.getWidth(), this.s.getHeight());
            }

        }
    }

    public Size getOutputMovieSize() {
        return this.g;
    }

    public void setCameraMode(CameraMode mode) {
        if (mode != this.h) {
            this.h = mode;
            this.ae.sendMessage(this.ae.obtainMessage(17, mode));
        }
    }

    public CameraMode getCameraMode() {
        return this.h;
    }

    private void a(CameraMode var1) {
        this.t = var1;
        if (this.aa) {
            this.l();
        }

    }

    public void setCompatibilityMode(boolean enabled) {
        if (enabled != this.i) {
            this.i = enabled;
            this.ae.sendMessage(this.ae.obtainMessage(18, Boolean.valueOf(enabled)));
        }
    }

    public boolean getCompatibilityMode() {
        return this.i;
    }

    private void b(boolean var1) {
        this.u = var1;
        if (this.aa) {
            this.l();
        }

    }

    private void l() {
        boolean var1 = this.c();
        if (var1) {
            this.setCameraDefaultFocusMode(false);
        } else if (!this.Y) {
            this.setCameraDefaultFocusMode(false);
        }

    }

    private void b(final Parameters var1) {
        this.X.post(new Runnable() {
            public void run() {
                CameraEncoder.this.c = var1;
                if (CameraEncoder.this.b != null) {
                    CameraEncoder.this.b.setCameraParams(var1);
                }

            }
        });
    }

    private void m() {
        this.c = null;
        if (this.b != null) {
            this.b.onCameraReleased();
        }

    }

    public void setCameraDefaultFocusMode(boolean initialize) {
        if (this.l == null) {
            this.Y = false;
        } else {
            Parameters var2 = this.l.getParameters();
            String var3 = this.a(var2);
            if (var3 == null) {
                this.Y = false;
            } else {
                if (!initialize) {
                    this.l.cancelAutoFocus();
                    var2.setFocusAreas((List) null);
                }

                var2.setFocusMode(var3);

                try {
                    this.l.setParameters(var2);
                } catch (RuntimeException var5) {
                    Log.e("CameraEncoder", "Unable to set focus mode" + var5);
                }

                this.Y = false;
                if (!initialize) {
                    this.n();
                }

            }
        }
    }

    String a(Parameters var1) {
        String var2 = null;
        List var3 = var1.getSupportedFocusModes();
        if (!var3.contains("auto")) {
            return var2;
        } else {
            if (this.t == CameraMode.PICTURE || this.t == CameraMode.AUTO && !this.ab) {
                if (var3.contains("continuous-picture")) {
                    var2 = "continuous-picture";
                } else if (var3.contains("auto")) {
                    var2 = "auto";
                }
            } else if (var3.contains("continuous-video")) {
                var2 = "continuous-video";
            } else if (var3.contains("auto")) {
                var2 = "auto";
            }

            return var2;
        }
    }

    private void c(final boolean var1) {
        this.X.post(new Runnable() {
            public void run() {
                if (CameraEncoder.this.b != null) {
                    CameraEncoder.this.b.onAutoFocusSuccessful(var1);
                }

            }
        });
    }

    private void n() {
        this.X.post(new Runnable() {
            public void run() {
                if (CameraEncoder.this.b != null) {
                    CameraEncoder.this.b.onFocusSetToDefault();
                }

            }
        });
    }

    public void requestTapToFocus(final Rect focusRect, final boolean lockedAutoFocus) {
        this.ae.post(new Runnable() {
            public void run() {
                CameraEncoder.this.an = System.currentTimeMillis();
                CameraEncoder.this.ao = focusRect;
                CameraEncoder.this.a(focusRect, lockedAutoFocus);
            }
        });
    }

    private void a(final Rect var1, final boolean var2) {
        if (this.l != null && this.x) {
            this.Y = true;
            this.Z.removeCallbacksAndMessages((Object) null);
            this.l.cancelAutoFocus();
            Parameters var3 = this.l.getParameters();
            var3.setFocusMode("auto");
            var3.setFocusAreas(Lists.newArrayList(new Area[]{new Area(var1, 1000)}));

            try {
                this.l.setParameters(var3);
            } catch (RuntimeException var6) {
                Log.e("CameraEncoder", "Unable to set tap to focus parameters" + var6);
            }

            this.Z.postDelayed(new Runnable() {
                public void run() {
                    Log.w("CameraEncoder", "Focus timeout reached");
                    CameraEncoder.this.setCameraDefaultFocusMode(false);
                }
            }, 3000L);

            try {
                this.l.autoFocus(new AutoFocusCallback() {
                    public void onAutoFocus(boolean focusSuccess, Camera camera) {
                        CameraEncoder.this.Z.removeCallbacksAndMessages((Object) null);
                        if (focusSuccess) {
                            CameraEncoder.this.c(var2);
                            if (!var2) {
                                CameraEncoder.this.Z.postDelayed(new Runnable() {
                                    public void run() {
                                        CameraEncoder.this.setCameraDefaultFocusMode(false);
                                    }
                                }, 2000L);
                            }
                        } else if (camera == CameraEncoder.this.l && var1 == CameraEncoder.this.ao && System.currentTimeMillis() - CameraEncoder.this.an < 500L) {
                            CameraEncoder.this.a(var1, var2);
                        } else {
                            CameraEncoder.this.setCameraDefaultFocusMode(false);
                        }

                    }
                });
            } catch (Exception var5) {
                Log.e("CameraEncoder", "Exception when setting focus", var5);
            }

        } else {
            if (this.l == null) {
                Log.w("CameraEncoder", "Called handleTapToFocus when camera is null");
            } else {
                Log.w("CameraEncoder", "Called handleTapToFocus in camera that does not support it.");
            }

        }
    }

    public void requestDefaultFocusMode() {
        this.ae.post(new Runnable() {
            public void run() {
                CameraEncoder.this.Z.removeCallbacksAndMessages((Object) null);
                CameraEncoder.this.setCameraDefaultFocusMode(false);
            }
        });
    }

    public void requestZoom(int zoomFactor) {
        this.af.sendMessage(this.af.obtainMessage(10, Integer.valueOf(zoomFactor)));
    }

    private void o() {
        if (this.y != this.z) {
            if (this.l != null && this.w) {
                Parameters var1 = this.l.getParameters();
                var1.setZoom(this.y);
                this.l.setParameters(var1);
                this.z = this.y;
            } else {
                if (this.l == null) {
                    Log.w("CameraEncoder", "Can't set zoom when camera is null");
                } else {
                    Log.w("CameraEncoder", "Can't set zoom in camera that does not support it.");
                }

                this.z = 0;
                this.y = 0;
            }
        }
    }

    public void setDesiredFlashMode(String desiredFlash) {
        if (!desiredFlash.equals(this.j)) {
            this.j = desiredFlash;
            this.ae.sendMessage(this.ae.obtainMessage(19, desiredFlash));
        }
    }

    private void a(String var1) {
        this.B = var1;
        if (!var1.equals(this.A)) {
            if (this.l != null) {
                try {
                    Parameters var2 = this.l.getParameters();
                    List var3 = var2.getSupportedFlashModes();
                    if (!CameraUtils.isValidFlashMode(var3, this.B)) {
                        Log.w("CameraEncoder", "Trying to set flash to: " + var1 + " modes available: " + var3);
                        return;
                    }

                    var2.setFlashMode(var1);
                    this.l.setParameters(var2);
                    this.A = var1;
                } catch (RuntimeException var4) {
                    Log.e("CameraEncoder", "Unable to set flash" + var4);
                }

            }
        }
    }

    public String getDesiredFlashMode() {
        return this.j;
    }

    private static class a extends Handler {
        private WeakReference<CameraEncoder> a;

        public a(CameraEncoder var1) {
            this.a = new WeakReference(var1);
        }

        public void handleMessage(Message inputMessage) {
            int var2 = inputMessage.what;
            Object var3 = inputMessage.obj;
            CameraEncoder var4 = (CameraEncoder) this.a.get();
            if (var4 == null) {
                Log.w("CameraEncoder", "EncoderHandler.handleMessage: encoder is null");
            } else {
                switch (var2) {
                    case 1:
                        var4.b();
                        break;
                    case 2:
                        var4.e();
                        break;
                    case 3:
                        var4.a((SurfaceTexture) var3);
                        break;
                    case 4:
                    case 9:
                    case 10:
                    default:
                        throw new RuntimeException("Unexpected msg what=" + var2);
                    case 5:
                        CameraEncoder.WWLCameraParameters var5 = (CameraEncoder.WWLCameraParameters) var3;
                        var4.a(var5);
                        break;
                    case 6:
                        var4.k();
                        break;
                    case 7:
                        var4.f();
                        break;
                    case 8:
                        CameraEncoder.b var6 = (CameraEncoder.b) var3;
                        var4.a(var6.a, var6.b);
                        break;
                    case 11:
                        var4.a((File) var3);
                        break;
                    case 12:
                        var4.b((File) var3);
                        break;
                    case 13:
                        var4.a((com.mla.wwl.c) var3);
                        break;
                    case 14:
                        var4.b((com.mla.wwl.c) var3);
                        break;
                    case 15:
                        var4.a(((Integer) var3).intValue());
                        break;
                    case 16:
                        var4.a(((Boolean) var3).booleanValue());
                        break;
                    case 17:
                        CameraMode var7 = (CameraMode) var3;
                        var4.a(var7);
                        break;
                    case 18:
                        boolean var8 = ((Boolean) var3).booleanValue();
                        var4.b(var8);
                        break;
                    case 19:
                        String var9 = (String) var3;
                        var4.a(var9);
                        break;
                    case 20:
                        Size var10 = (Size) var3;
                        var4.handleSetOutputMovieSize(var10);
                }

            }
        }
    }

    private static class b {
        Muxer a;
        WWLCamcorderProfile b;

        private b(WWLCamcorderProfile camcorderProfile) {
            this.b = camcorderProfile;
        }
    }

    /* renamed from: com.mla.wwl.CameraEncoder$c */
    private class c extends Handler {
        /* renamed from: a */
        final /* synthetic */ CameraEncoder a;

        private c(CameraEncoder cameraEncoder) {
            this.a = cameraEncoder;
        }

        public void handleMessage(Message inputMessage) {
            if (inputMessage.what == 10) {
                this.a.y = ((Integer) inputMessage.obj).intValue();
            }
        }
    }

    protected static class WWLCameraParameters {
        public int cameraFacing;
        public final Size videoSize;
        public final Size pictureSize;

        public WWLCameraParameters(int facing, Size videoSize, Size pictureSize) {
            this.cameraFacing = facing;
            this.videoSize = videoSize;
            this.pictureSize = pictureSize;
        }
    }
}

package com.mla.wwl.av;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaCodec.BufferInfo;
import android.util.Log;
import java.nio.ByteBuffer;

public abstract class AndroidEncoder implements Runnable {
    public Muxer mMuxer;
    public MediaCodec mMediaCodec;
    protected int mTrackIndex;
    protected int mBitrate;
    private final Object a = new Object();
    private boolean b;
    private boolean c;

    public AndroidEncoder(Muxer muxer) {
        this.mMuxer = muxer;
    }

    protected void startDrainingThread() {
        Object var1 = this.a;
        synchronized(this.a) {
            (new Thread(this, "Encoder - Draining")).start();
            this.c = true;

            while(!this.b) {
                try {
                    this.a.wait();
                } catch (InterruptedException var4) {
                    ;
                }
            }

        }
    }

    public void run() {
        Object var1 = this.a;
        synchronized(this.a) {
            this.b = true;
            this.a.notify();
        }

        this.a();
        this.b();
        var1 = this.a;
        synchronized(this.a) {
            this.a.notify();
            this.c = false;
        }
    }

    private void a() {
        boolean var1 = true;
        ByteBuffer[] var2 = this.mMediaCodec.getOutputBuffers();

        while(true) {
            while(true) {
                BufferInfo var3 = new BufferInfo();
                int var4 = this.mMediaCodec.dequeueOutputBuffer(var3, 20000L);
                if(var4 != -1) {
                    if(var4 == -3) {
                        var2 = this.mMediaCodec.getOutputBuffers();
                    } else if(var4 == -2) {
                        MediaFormat var5 = this.mMediaCodec.getOutputFormat();
                        this.addMissingFormatKeys(var5);
                        this.mTrackIndex = this.mMuxer.addTrack(var5);
                    } else if(var4 < 0) {
                        Log.w("AndroidEncoder", "Unexpected result from encoder.dequeueOutputBuffer: " + var4);
                    } else {
                        ByteBuffer var6 = var2[var4];
                        if(var6 == null) {
                            throw new RuntimeException("encoderOutputBuffer " + var4 + " was null");
                        }

                        if(var3.size >= 0) {
                            var6.position(var3.offset);
                            var6.limit(var3.offset + var3.size);
                            this.mMuxer.writeSampleData(this.mMediaCodec, this.mTrackIndex, var4, var6, var3);
                            if((var3.flags & 4) != 0) {
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    private void b() {
        this.mMuxer = null;
        if(this.mMediaCodec != null) {
            this.mMediaCodec.stop();
            this.mMediaCodec.release();
            this.mMediaCodec = null;
        }

    }

    protected void addMissingFormatKeys(MediaFormat format) {
        if(!format.containsKey("bitrate")) {
            format.setInteger("bitrate", this.mBitrate);
        }

    }

    public void waitUntilReleased() {
        Object var1 = this.a;
        synchronized(this.a) {
            while(this.c) {
                try {
                    this.a.wait();
                } catch (InterruptedException var4) {
                    ;
                }
            }

        }
    }

    public void signalEOS() {
        if(this.isSurfaceInputEncoder()) {
            this.mMediaCodec.signalEndOfInputStream();
        }

    }

    public MediaCodec getMediaCodec() {
        return this.mMediaCodec;
    }

    protected abstract boolean isSurfaceInputEncoder();
}


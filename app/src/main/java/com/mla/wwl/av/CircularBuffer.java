package com.mla.wwl.av;

import android.media.MediaFormat;
import android.media.MediaCodec.BufferInfo;
import android.util.Log;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class CircularBuffer {
    private ByteBuffer[] b = new ByteBuffer[1];
    int a = 1;
    private ByteOrder c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int[] h;
    private long[] i;
    private int[] j;
    private int[] k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private double q;

    public CircularBuffer(MediaFormat mediaFormat, int desiredSpanMs) {
        this.p = mediaFormat.getInteger("bitrate");
        this.f = (int)((long)this.p * (long)desiredSpanMs / 8000L);
        int var3 = this.p / 8;
        this.b[0] = ByteBuffer.allocateDirect(this.f);
        this.f = this.b[0].capacity();
        this.g = this.f;
        this.d = (int)(8000L * (long)this.f / (long)this.p);
        this.e = this.d;
        String var4 = mediaFormat.getString("mime");
        boolean var5 = var4.equals("video/avc");
        boolean var6 = var4.equals("audio/mp4a-latm");
        double var9;
        if(var5) {
            var9 = (double)mediaFormat.getInteger("frame-rate");
        } else {
            if(!var6) {
                throw new RuntimeException("Media format provided is neither AVC nor AAC");
            }

            double var11 = (double)mediaFormat.getInteger("sample-rate");
            var9 = var11 / 1024.0D;
        }

        this.q = 1000.0D / var9;
        double var7 = (double)this.p / var9 / 8.0D;
        int var13 = (int)((double)this.f / var7 + 1.0D);
        this.l = var13 * 2;
        this.m = this.l;
        this.h = new int[this.l];
        this.i = new long[this.l];
        this.j = new int[this.l];
        this.k = new int[this.l];
    }

    public boolean increaseSize() {
        this.b = (ByteBuffer[])a(this.b, this.b.length, ByteBuffer[].class, 1);
        int var1 = this.b.length - 1;

        try {
            this.b[var1] = ByteBuffer.allocateDirect(this.f);
        } catch (OutOfMemoryError var5) {
            Log.w("CircularBuffer", "Could not allocate memory to increase size.");
            return false;
        }

        if(this.b[var1].capacity() != this.f) {
            throw new RuntimeException("Allocated size can't be different.");
        } else {
            this.b[var1].order(this.c);
            this.g += this.f;
            this.e += this.d;
            this.h = (int[])a(this.h, this.l, int[].class, this.m, this.o, this.n);
            this.i = (long[])a(this.i, this.l, long[].class, this.m, this.o, this.n);
            this.j = (int[])a(this.j, this.l, int[].class, this.m, this.o, this.n);
            this.k = (int[])a(this.k, this.l, int[].class, this.m, this.o, this.n);
            int var2 = this.getPacketNum();
            this.l += this.m;
            this.n = (this.o + var2) % this.l;
            int var3 = this.getFirstIndex();
            var3 = this.getNextIndex(var3);

            for(boolean var4 = false; var3 >= 0; var3 = this.getNextIndex(var3)) {
                if(this.j[var3] == 0) {
                    var4 = true;
                }

                if(var4) {
                    this.c(var3);
                }
            }

            return true;
        }
    }

    private static <A> A a(A var0, int var1, Class<A> var2, int var3) {
        int var4 = var1 + var3;
        A var5 = var2.cast(Array.newInstance(var2.getComponentType(), var4));
        System.arraycopy(var0, 0, var5, 0, var1);
        return var5;
    }

    private static <A> A a(A var0, int var1, Class<A> var2, int var3, int var4, int var5) {
        int var6 = var1 + var3;
        A var7 = var2.cast(Array.newInstance(var2.getComponentType(), var6));
        if(var5 > var4) {
            System.arraycopy(var0, var4, var7, var4, var5 - var4);
        } else {
            System.arraycopy(var0, var4, var7, var4, var1 - var4);
            int var8 = var5 - 0;
            if(var8 <= var3) {
                System.arraycopy(var0, 0, var7, var1, var8);
            } else {
                System.arraycopy(var0, 0, var7, var1, var3);
                int var9 = var8 - var3;
                System.arraycopy(var0, var3, var7, 0, var9);
            }
        }

        return var7;
    }

    public boolean isEmpty() {
        return this.n == this.o;
    }

    public int getFirstIndex() {
        return this.isEmpty()?-1:this.o;
    }

    public int getNextIndex(int index) {
        int var2 = (index + 1) % this.l;
        if(var2 == this.n) {
            var2 = -1;
        }

        return var2;
    }

    public int getPreviousIndex(int index) {
        if(index == this.o) {
            return -1;
        } else {
            int var2 = (index - 1 + this.l) % this.l;
            return var2;
        }
    }

    public int getLastIndex() {
        return this.isEmpty()?-1:(this.n + this.l - 1) % this.l;
    }

    public int getPacketNum() {
        int var1 = (this.n + this.l - this.o) % this.l;
        return var1;
    }

    private int a() {
        if(this.isEmpty()) {
            return 0;
        } else {
            int var1 = this.getLastIndex();
            return (this.j[var1] + this.k[var1]) % this.g;
        }
    }

    private int a(int var1) {
        if(this.isEmpty()) {
            return this.g;
        } else {
            int var2 = this.j[this.o];
            int var3 = (var2 + this.g - var1) % this.g;
            return var3;
        }
    }

    public int add(ByteBuffer buf, BufferInfo info) {
        int var3 = info.size;
        int var4;
        if(this.c == null) {
            this.c = buf.order();

            for(var4 = 0; var4 < this.b.length; ++var4) {
                this.b[var4].order(this.c);
            }
        }

        if(this.c != buf.order()) {
            throw new RuntimeException("Byte ordering changed");
        } else if(!this.b(var3)) {
            return -1;
        } else {
            var4 = this.a();
            int var5 = var4 / this.f * this.f;
            int var6 = var5 + this.f - 1;
            if(var4 + var3 - 1 > var6) {
                var4 = (var5 + this.f) % this.g;
            }

            int var7 = var4 % this.f;
            int var8 = var4 / this.f;
            buf.limit(info.offset + info.size);
            buf.position(info.offset);
            this.b[var8].limit(var7 + info.size);
            this.b[var8].position(var7);
            this.b[var8].put(buf);
            this.h[this.n] = info.flags;
            this.i[this.n] = info.presentationTimeUs;
            this.j[this.n] = var4;
            this.k[this.n] = var3;
            int var9 = this.n;
            this.n = (this.n + 1) % this.l;
            return var9;
        }
    }

    private boolean b(int var1) {
        if(var1 > this.f) {
            throw new RuntimeException("Enormous packet: " + var1 + " vs. buffer " + this.f);
        } else if(this.isEmpty()) {
            return true;
        } else {
            int var2 = (this.n + 1) % this.l;
            if(var2 == this.o) {
                return false;
            } else {
                int var3 = this.a();
                int var4 = this.a(var3);
                if(var1 > var4) {
                    return false;
                } else {
                    int var5 = var3 / this.f * this.f;
                    int var6 = var5 + this.f - 1;
                    if(var3 + var1 - 1 > var6) {
                        var3 = (var5 + this.f) % this.g;
                        var4 = this.a(var3);
                        if(var1 > var4) {
                            return false;
                        }
                    }

                    return true;
                }
            }
        }
    }

    private void c(int var1) {
        int var2 = this.getPreviousIndex(var1);
        if(var2 == -1) {
            throw new RuntimeException("Can't move tail packet.");
        } else {
            int var3 = (this.j[var2] + this.k[var2]) % this.g;
            int var4 = this.k[var1];
            int var5 = var3 / this.f * this.f;
            int var6 = var5 + this.f - 1;
            if(var3 + var4 - 1 > var6) {
                var3 = (var5 + this.f) % this.g;
            }

            int var7 = var3 % this.f;
            int var8 = var3 / this.f;
            BufferInfo var9 = new BufferInfo();
            ByteBuffer var10 = this.getChunk(var1, var9);
            this.b[var8].limit(var7 + var4);
            this.b[var8].position(var7);
            this.b[var8].put(var10);
            this.j[var1] = var3;
        }
    }

    public ByteBuffer getChunk(int index, BufferInfo info) {
        if(this.isEmpty()) {
            throw new RuntimeException("Can't return chunk of empty buffer");
        } else {
            int var3 = this.j[index] % this.f;
            int var4 = this.j[index] / this.f;
            info.flags = this.h[index];
            info.presentationTimeUs = this.i[index];
            info.offset = var3;
            info.size = this.k[index];
            ByteBuffer var5 = this.b[var4].duplicate();
            var5.order(this.c);
            var5.limit(info.offset + info.size);
            var5.position(info.offset);
            return var5;
        }
    }

    public ByteBuffer getTailChunk(BufferInfo info) {
        int var2 = this.getFirstIndex();
        return this.getChunk(var2, info);
    }

    public void removeTail() {
        if(this.isEmpty()) {
            throw new RuntimeException("Can't removeTail() in empty buffer");
        } else {
            this.o = (this.o + 1) % this.l;
        }
    }
}

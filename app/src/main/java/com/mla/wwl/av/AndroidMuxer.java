package com.mla.wwl.av;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.mla.wwl.Utils.WWLHandler.ResultRunnable;

import java.io.IOException;
import java.nio.ByteBuffer;

public class AndroidMuxer extends Muxer {
    private MediaMuxer a;
    private MediaCodec.BufferInfo b = new MediaCodec.BufferInfo();
    private int[] c = new int[2];
    private MediaFormat[] d = new MediaFormat[2];
    private HandlerThread e = new HandlerThread("Muxer - writer");
    private AndroidMuxer.a f;
    private CircularBuffer[] g = new CircularBuffer[2];
    private CircularBuffer[] h = new CircularBuffer[2];
    private boolean i = true;
    private final Object j = new Object();
    private final Object k = new Object();

    private AndroidMuxer(String outputFile, FORMAT format) {
        super(outputFile, format);
        this.e.start();
        this.f = new AndroidMuxer.a(this.e.getLooper());

        try {
            switch (format) {
                case MPEG4:
                    this.a = new MediaMuxer(outputFile, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
                    return;
                default:
                    throw new IllegalArgumentException("Unrecognized format!");
            }
        } catch (IOException var4) {
            throw new RuntimeException("MediaMuxer creation failed", var4);
        }
    }

    public static AndroidMuxer create(String outputFile, FORMAT format) {
        return new AndroidMuxer(outputFile, format);
    }

    public int addTrack(final MediaFormat trackFormat) {
        ResultRunnable var2 = new ResultRunnable<Integer>() {
            public void doRun() {
                int var1 = AndroidMuxer.super.addTrack(trackFormat);
                if (AndroidMuxer.this.mStarted) {
                    throw new RuntimeException("format changed twice");
                } else {
                    AndroidMuxer.this.g[var1] = new CircularBuffer(trackFormat, 2000);
                    AndroidMuxer.this.d[var1] = trackFormat;
                    if (AndroidMuxer.this.c(var1)) {
                        AndroidMuxer.this.c[var1] = AndroidMuxer.this.a.addTrack(trackFormat);
                        if (AndroidMuxer.this.allTracksAdded()) {
                            int var2 = (var1 + 1) % 2;
                            AndroidMuxer.this.c[var2] = AndroidMuxer.this.a.addTrack(AndroidMuxer.this.d[var2]);
                        }
                    } else {
                        if (!AndroidMuxer.this.d(var1)) {
                            throw new RuntimeException("Media format provided is neither AVC nor AAC");
                        }

                        if (AndroidMuxer.this.allTracksAdded()) {
                            AndroidMuxer.this.c[var1] = AndroidMuxer.this.a.addTrack(trackFormat);
                        }
                    }

                    if (AndroidMuxer.this.allTracksAdded()) {
                        AndroidMuxer.this.start();
                    }

                    this.returnValue = Integer.valueOf(var1);
                }
            }
        };
        this.f.post(var2);
        int var3 = ((Integer) var2.get()).intValue();
        return var3;
    }

    protected void start() {
        super.start();
        this.a.start();
    }

    protected void stop() {
        this.a.stop();
        this.release();
        this.muxerFinished();
    }

    protected void release() {
        this.f.getLooper().quitSafely();
        this.mStarted = false;
        this.g = null;
        this.a.release();
    }

    public boolean isStarted() {
        return this.mStarted;
    }

    public void writeSampleData(MediaCodec mediaCodec, int trackIndex, int bufferIndex, ByteBuffer encodedData, MediaCodec.BufferInfo bufferInfo) {
        CircularBuffer var6 = this.g[trackIndex];
        int var7 = -1;
        Object var8 = this.j;
        synchronized (this.j) {
            while (var7 == -1) {
                var7 = var6.add(encodedData, bufferInfo);
                if (var7 == -1) {
                    if (this.i) {
                        Object var9 = this.k;
                        synchronized (this.k) {
                            this.i = var6.increaseSize();
                        }

                        if (this.i) {
                            continue;
                        }
                    }

                    Log.w("AndroidMuxer", "Blocked until free space is made for track index: " + trackIndex + " before adding package with ts: " + bufferInfo.presentationTimeUs);

                    try {
                        this.j.wait();
                    } catch (InterruptedException var12) {
                        ;
                    }
                }
            }
        }

        mediaCodec.releaseOutputBuffer(bufferIndex, false);
        this.f.obtainMessage(1, Integer.valueOf(trackIndex)).sendToTarget();
    }

    private void a(int var1) {
        CircularBuffer var2 = this.g[var1];
        Object var4 = this.k;
        ByteBuffer var3;
        synchronized (this.k) {
            var3 = var2.getTailChunk(this.b);
        }

        CircularBuffer var9 = this.h[var1];
        if (var9 == null) {
            this.a(var3, this.b, var1);
        } else {
            label44:
            while (true) {
                do {
                    int var5 = var9.add(var3, this.b);
                    if (var5 != -1) {
                        break label44;
                    }

                    if (!this.i) {
                        break;
                    }

                    this.i = var9.increaseSize();
                } while (this.i);

                Log.w("AndroidMuxer", "Removing tail from secondary buffer to add new packet.");
                var9.removeTail();
            }
        }

        Object var10 = this.j;
        synchronized (this.j) {
            var2.removeTail();
            this.j.notifyAll();
        }

        if (this.allTracksFinished()) {
            this.stop();
        }

    }

    private void b(int var1) {
        if (!this.allTracksStarted()) {
            throw new RuntimeException("Can't flush secondary buffer without all tracks started.");
        } else {
            CircularBuffer var2 = this.h[var1];
            MediaCodec.BufferInfo var3 = new MediaCodec.BufferInfo();

            while (!var2.isEmpty()) {
                ByteBuffer var4 = var2.getTailChunk(var3);
                this.a(var4, var3, var1);
                var2.removeTail();
            }

            this.h[var1] = null;
        }
    }

    private void a(ByteBuffer var1, MediaCodec.BufferInfo var2, int var3) {
        if ((var2.flags & 2) == 0) {
            if ((var2.flags & 4) != 0) {
                this.trackFinished();
            } else if (var2.size != 0) {
                boolean var4 = false;
                if (!this.mTrackStarted[var3]) {
                    this.trackStarted(var3, var2.presentationTimeUs, this.d(var3));
                    if (!this.allTracksStarted()) {
                        this.h[var3] = new CircularBuffer(this.d[var3], 2000);
                        this.h[var3].add(var1, var2);
                        return;
                    }

                    if (this.c(var3)) {
                        var4 = true;
                    } else {
                        int var5 = (var3 + 1) % 2;
                        this.b(var5);
                    }
                }

                long var8 = var2.presentationTimeUs;
                var2.presentationTimeUs -= this.mSyncStartPtsUs;
                var2.presentationTimeUs = this.getSafePts(var2.presentationTimeUs, var3);
                this.a.writeSampleData(this.c[var3], var1, var2);
                if (var4) {
                    int var7 = (var3 + 1) % 2;
                    this.b(var7);
                }

            }
        }
    }

    private boolean c(int var1) {
        String var2 = this.d[var1].getString("mime");
        return var2.equals("video/avc");
    }

    private boolean d(int var1) {
        String var2 = this.d[var1].getString("mime");
        return var2.equals("audio/mp4a-latm");
    }

    private class a extends Handler {
        public a(Looper var2) {
            super(var2);
        }

        public void handleMessage(Message msg) {
            int var2 = msg.what;
            Object var3 = msg.obj;
            switch (var2) {
                case 1:
                    AndroidMuxer.this.a(((Integer) var3).intValue());
                    return;
                default:
                    throw new RuntimeException("Unexpected msg what=" + var2);
            }
        }
    }
}

package com.mla.wwl.av;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.view.Surface;

import java.io.IOException;

public class VideoEncoderCore extends AndroidEncoder {
    private int a;
    private Surface b;

    public VideoEncoderCore(int width, int height, int frameRate, int bitRate, int iFrameInterval, Muxer muxer) {
        super(muxer);

        frameRate = Math.min(frameRate, 30);
        a = frameRate;
        mBitrate = bitRate;

        MediaFormat localMediaFormat = MediaFormat.createVideoFormat("video/avc", width, height);


        localMediaFormat.setInteger("color-format", 2130708361);
        localMediaFormat.setInteger("bitrate", bitRate);
        localMediaFormat.setInteger("frame-rate", frameRate);
        localMediaFormat.setInteger("i-frame-interval", iFrameInterval);


        try {
            mMediaCodec = MediaCodec.createEncoderByType("video/avc");
        } catch (IOException localIOException) {
            localIOException.printStackTrace();
        }

        mMediaCodec.configure(localMediaFormat, null, null, 1);
        b = mMediaCodec.createInputSurface();
        mMediaCodec.start();

        mTrackIndex = -1;

        startDrainingThread();
    }


    protected void addMissingFormatKeys(MediaFormat format) {
        super.addMissingFormatKeys(format);
        if (!format.containsKey("frame-rate")) {
            format.setInteger("frame-rate", a);
        }
    }


    public Surface getInputSurface() {
        return b;
    }

    protected boolean isSurfaceInputEncoder() {
        return true;
    }
}

package com.mla.wwl.av;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaCodec.BufferInfo;
import android.util.Log;
import com.google.common.base.Preconditions;
import com.google.common.eventbus.EventBus;
import com.mla.wwl.events.MuxerFinishedEvent;
import com.mla.wwl.events.MuxerStartedAllTracksEvent;
import java.io.File;
import java.nio.ByteBuffer;

public abstract class Muxer {
    protected static final int EXPECTED_TRACK_NUM = 2;
    protected Muxer.FORMAT mFormat;
    protected String mOutputPath;
    protected int mNumTracks;
    protected int mNumTracksStarted;
    protected int mNumTracksFinished;
    protected boolean[] mTrackStarted;
    protected MediaFormat[] mMediaFormat;
    protected long[] mLastPtsUs;
    protected long mSyncStartPtsUs;
    protected boolean mStarted;
    private EventBus a;

    protected Muxer(String outputPath, Muxer.FORMAT format) {
        Log.i("Muxer", "Created muxer for output: " + outputPath);
        this.mOutputPath = (String)Preconditions.checkNotNull(outputPath);
        this.mFormat = format;
        this.mNumTracks = 0;
        this.mNumTracksFinished = 0;
        this.mNumTracksStarted = 0;
        this.mTrackStarted = new boolean[2];
        this.mMediaFormat = new MediaFormat[2];
        this.mLastPtsUs = new long[2];

        for(int var3 = 0; var3 < 2; ++var3) {
            this.mLastPtsUs[var3] = -1L;
        }

        this.mSyncStartPtsUs = -1L;
    }

    public void setEventBus(EventBus eventBus) {
        this.a = eventBus;
    }

    public String getOutputPath() {
        return this.mOutputPath;
    }

    public int addTrack(MediaFormat trackFormat) {
        ++this.mNumTracks;
        return this.mNumTracks - 1;
    }

    protected void stop() {
    }

    protected void start() {
        this.mStarted = true;
    }

    protected void muxerFinished() {
        if(this.a != null) {
            this.a.post(new MuxerFinishedEvent(new File(this.mOutputPath)));
        }

    }

    protected void release() {
    }

    public boolean isStarted() {
        return false;
    }

    public void writeSampleData(MediaCodec mediaCodec, int trackIndex, int bufferIndex, ByteBuffer encodedData, BufferInfo bufferInfo) {
    }

    protected void trackStarted(int trackIndex, long timestampUs, boolean isAudio) {
        if(this.mTrackStarted[trackIndex]) {
            throw new RuntimeException("Track " + trackIndex + " added twice.");
        } else {
            if(isAudio) {
                this.mSyncStartPtsUs = timestampUs;
            }

            this.mTrackStarted[trackIndex] = true;
            ++this.mNumTracksStarted;
            if(this.allTracksStarted() && this.a != null) {
                this.a.post(new MuxerStartedAllTracksEvent());
            }

        }
    }

    protected boolean allTracksAdded() {
        return this.mNumTracks == 2;
    }

    protected boolean allTracksStarted() {
        return this.mNumTracksStarted == 2;
    }

    protected boolean allTracksFinished() {
        return this.allTracksAdded()?this.mNumTracks == this.mNumTracksFinished:false;
    }

    protected void trackFinished() {
        ++this.mNumTracksFinished;
    }

    protected boolean formatRequiresADTS() {
        switch(this.mFormat.ordinal()) {
            case 1:
                return true;
            default:
                return false;
        }
    }

    protected boolean formatRequiresBuffering() {
        switch(this.mFormat.ordinal()) {
            case 2:
                return true;
            default:
                return false;
        }
    }

    protected long getSafePts(long pts, int trackIndex) {
        if(pts < 0L) {
            Log.w("Muxer", "Timestamp: " + pts + " for track index: " + trackIndex + " is less than 0");
            pts = 0L;
        }

        if(pts <= this.mLastPtsUs[trackIndex]) {
            Log.w("Muxer", "Timestamp: " + pts + " for track index: " + trackIndex + " is older than the previous one: " + this.mLastPtsUs[trackIndex]);
            this.mLastPtsUs[trackIndex] += 9643L;
            return this.mLastPtsUs[trackIndex];
        } else {
            this.mLastPtsUs[trackIndex] = pts;
            return pts;
        }
    }

    public enum FORMAT {
        MPEG4,
        HLS,
        RTMP
    }
}
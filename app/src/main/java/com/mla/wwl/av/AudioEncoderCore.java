package com.mla.wwl.av;


import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.view.Surface;
import java.io.IOException;

public class AudioEncoderCore extends AndroidEncoder {
  protected static final String MIME_TYPE = "audio/mp4a-latm";
  public int mChannelConfig;
  public int mSampleRate;
  public int mNumChannels;
  public int mMax_input_size;
  public static final int MAX_INPUT_SIZE = 16384;

  public AudioEncoderCore(int numChannels, int bitRate, int sampleRate, Muxer muxer) {
    super(muxer);
    byte var5;
    switch(numChannels) {
      case 1:
        this.mChannelConfig = 16;
        this.mNumChannels = 1;
        var5 = 16;
        break;
      case 2:
        this.mChannelConfig = 12;
        this.mNumChannels = 2;
        var5 = 12;
        break;
      default:
        throw new IllegalArgumentException("Invalid channel count. Must be 1 or 2");
    }

    this.mSampleRate = sampleRate;
    this.mBitrate = bitRate;
    this.mMax_input_size = 16384 * this.mNumChannels;
    MediaFormat var6 = MediaFormat.createAudioFormat("audio/mp4a-latm", this.mSampleRate, this.mChannelConfig);
    var6.setInteger("aac-profile", 2);
    var6.setInteger("sample-rate", this.mSampleRate);
    var6.setInteger("channel-count", numChannels);
    var6.setInteger("bitrate", bitRate);
    var6.setInteger("channel-mask", var5);
    var6.setInteger("max-input-size", this.mMax_input_size);

    try {
      this.mMediaCodec = MediaCodec.createEncoderByType("audio/mp4a-latm");
    } catch (IOException var8) {
      var8.printStackTrace();
    }

    this.mMediaCodec.configure(var6, (Surface)null, (MediaCrypto)null, 1);
    this.mMediaCodec.start();
    this.mTrackIndex = -1;
    this.startDrainingThread();
  }

  public MediaCodec getMediaCodec() {
    return this.mMediaCodec;
  }

  protected boolean isSurfaceInputEncoder() {
    return false;
  }
}
package com.mla.wwl;

public final class BuildConfig
{
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.mla.wwl";
  public static final String BUILD_TYPE = "distribute";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 9;
  public static final String VERSION_NAME = "1.17";
  public static final boolean DEBUG_MODE = false;
  public static final boolean ENABLE_LOG = false;
  
  public BuildConfig() {}
}

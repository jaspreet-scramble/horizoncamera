package com.mla.wwl;

import android.hardware.Camera.Parameters;
import java.io.File;

public interface WWLCameraListener {
  void onFailedToStart();

  void onStartedRunning(Parameters var1, int var2);

  void onPreviewHasBeenRunning(Parameters var1, int var2);

  void onWillStopRunning();

  void onStoppedRunning();

  void onRecordingHasStarted();

  void onRecordingWillStop();

  void onRecordingFinished(File var1, boolean var2);

  void onPhotoCaptured(File var1, boolean var2);

  void onSnapshotCaptured(File var1);

  void onAngleUpdated(float var1, float var2);

  void onSensorNotResponding();

  void onSensorResponded();
}

package com.mla.wwl;

public class WWLVars {
  public WWLVars() {
  }

  public static enum WWLLevelerLockedOrientation {
    VERTICAL,
    HORIZONTAL,
    AUTO;

    private WWLLevelerLockedOrientation() {
    }
  }

  public static enum WWLLevelerFlexSpeed {
    SMOOTH,
    RESPONSIVE;

    private WWLLevelerFlexSpeed() {
    }
  }

  public static enum WWLLevelerCropMode {
    FLEX,
    ROTATE,
    LOCKED;

    private WWLLevelerCropMode() {
    }
  }

  public static enum CameraMode {
    PICTURE,
    VIDEO,
    AUTO;

    private CameraMode() {
    }
  }
}

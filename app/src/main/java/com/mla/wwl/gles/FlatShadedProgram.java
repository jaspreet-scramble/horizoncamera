package com.mla.wwl.gles;

import android.opengl.GLES20;
import android.util.Log;

public class FlatShadedProgram {
  private int a = -1;
  private int b = -1;
  private int c = -1;
  private int d = -1;

  public FlatShadedProgram() {
    this.a = GlUtil.createProgram("uniform mat4 uMVPMatrix;attribute vec4 aPosition;void main() {    gl_Position = uMVPMatrix * aPosition;}", "precision mediump float;uniform vec4 uColor;void main() {    gl_FragColor = uColor;}");
    if(this.a == 0) {
      throw new RuntimeException("Unable to create program");
    } else {
      Log.d("GlUtil", "Created program " + this.a);
      this.d = GLES20.glGetAttribLocation(this.a, "aPosition");
      GlUtil.checkLocation(this.d, "aPosition");
      this.c = GLES20.glGetUniformLocation(this.a, "uMVPMatrix");
      GlUtil.checkLocation(this.c, "uMVPMatrix");
      this.b = GLES20.glGetUniformLocation(this.a, "uColor");
      GlUtil.checkLocation(this.b, "uColor");
    }
  }

  public void release() {
    GLES20.glDeleteProgram(this.a);
    this.a = -1;
  }

  public void draw(float[] mvpMatrix, float[] color, Drawable2d[] drawables) {
    GlUtil.checkGlError("draw start");
    GLES20.glUseProgram(this.a);
    GlUtil.checkGlError("glUseProgram");
    GLES20.glUniformMatrix4fv(this.c, 1, false, mvpMatrix, 0);
    GlUtil.checkGlError("glUniformMatrix4fv");
    GLES20.glUniform4fv(this.b, 1, color, 0);
    GlUtil.checkGlError("glUniform4fv ");
    GLES20.glEnableVertexAttribArray(this.d);
    GlUtil.checkGlError("glEnableVertexAttribArray");
    Drawable2d[] var4 = drawables;
    int var5 = drawables.length;

    for(int var6 = 0; var6 < var5; ++var6) {
      Drawable2d var7 = var4[var6];
      GLES20.glVertexAttribPointer(this.d, var7.getCoordsPerVertex(), 5126, false, var7.getVertexStride(), var7.getVertexArray());
      GlUtil.checkGlError("glVertexAttribPointer");
      GLES20.glDrawArrays(5, 0, var7.getVertexCount());
      GlUtil.checkGlError("glDrawArrays");
    }

    GLES20.glDisableVertexAttribArray(this.d);
    GLES20.glUseProgram(0);
  }
}


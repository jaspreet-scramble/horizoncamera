package com.mla.wwl.gles;

import android.opengl.GLES20;
import android.view.MotionEvent;

public class Texture2dProgram {
    public static final int KERNEL_SIZE = 9;
    private Texture2dProgram.ProgramType a;
    private float b;
    private float c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private float[] o = new float[9];
    private float[] p = new float[2];
    private float[] q = new float[2];
    private float[] r;
    private float s;
    private float t = 1.0F;
    /* renamed from: com.mla.wwl.gles.Texture2dProgram$1 */
    static class C00611 {
        /* renamed from: a */
        static final /* synthetic */ int[] f441a = new int[ProgramType.values().length];

        static {
            try {
                f441a[ProgramType.TEXTURE_2D.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_BW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_NIGHT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_CHROMA_KEY.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_SQUEEZE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_TWIRL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_TUNNEL.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_BULGE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_FISHEYE.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_DENT.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_MIRROR.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_STRETCH.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                f441a[ProgramType.TEXTURE_EXT_FILT.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
        }
    }
    public Texture2dProgram(Texture2dProgram.ProgramType programType) {
        this.a = programType;
        switch (C00611.f441a[programType.ordinal()]) {
            case 1:
                this.n = 3553;
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "precision mediump float;\nuniform float opacity;\nvarying vec2 vTextureCoord;\nuniform sampler2D sTexture;\nvoid main() {\n    gl_FragColor = texture2D(sTexture, vTextureCoord) * opacity;\n}\n");
                break;
            case 2:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nvoid main() {\n    gl_FragColor = texture2D(sTexture, vTextureCoord);\n}\n");
                break;
            case 3:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nvoid main() {\n    vec4 tc = texture2D(sTexture, vTextureCoord);\n    float color = tc.r * 0.3 + tc.g * 0.59 + tc.b * 0.11;\n    gl_FragColor = vec4(color, color, color, 1.0);\n}\n");
                break;
            case 4:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nvoid main() {\n    vec4 tc = texture2D(sTexture, vTextureCoord);\n    float color = ((tc.r * 0.3 + tc.g * 0.59 + tc.b * 0.11) - 0.5 * 1.5) + 0.8;\n    gl_FragColor = vec4(color, color + 0.15, color, 1.0);\n}\n");
                break;
            case 5:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nvoid main() {\n    vec4 tc = texture2D(sTexture, vTextureCoord);\n    float color = ((tc.r * 0.3 + tc.g * 0.59 + tc.b * 0.11) - 0.5 * 1.5) + 0.8;\n    if(tc.g > 0.6 && tc.b < 0.6 && tc.r < 0.6){ \n        gl_FragColor = vec4(0, 0, 0, 0.0);\n    }else{ \n        gl_FragColor = texture2D(sTexture, vTextureCoord);\n    }\n}\n");
                break;
            case 6:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nuniform vec2 uPosition;\nvoid main() {\n    vec2 texCoord = vTextureCoord.xy;\n    vec2 normCoord = 2.0 * texCoord - 1.0;\n    float r = length(normCoord); // to polar coords \n    float phi = atan(normCoord.y + uPosition.y, normCoord.x + uPosition.x); // to polar coords \n    r = pow(r, 1.0/1.8) * 0.8;\n    normCoord.x = r * cos(phi); \n    normCoord.y = r * sin(phi); \n    texCoord = normCoord / 2.0 + 0.5;\n    gl_FragColor = texture2D(sTexture, texCoord);\n}\n");
                break;
            case 7:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nuniform vec2 uPosition;\nvoid main() {\n    vec2 texCoord = vTextureCoord.xy;\n    vec2 normCoord = 2.0 * texCoord - 1.0;\n    float r = length(normCoord); // to polar coords \n    float phi = atan(normCoord.y + uPosition.y, normCoord.x + uPosition.x); // to polar coords \n    phi = phi + (1.0 - smoothstep(-0.5, 0.5, r)) * 4.0;\n    normCoord.x = r * cos(phi); \n    normCoord.y = r * sin(phi); \n    texCoord = normCoord / 2.0 + 0.5;\n    gl_FragColor = texture2D(sTexture, texCoord);\n}\n");
                break;
            case 8:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nuniform vec2 uPosition;\nvoid main() {\n    vec2 texCoord = vTextureCoord.xy;\n    vec2 normCoord = 2.0 * texCoord - 1.0;\n    float r = length(normCoord); // to polar coords \n    float phi = atan(normCoord.y + uPosition.y, normCoord.x + uPosition.x); // to polar coords \n    if (r > 0.5) r = 0.5;\n    normCoord.x = r * cos(phi); \n    normCoord.y = r * sin(phi); \n    texCoord = normCoord / 2.0 + 0.5;\n    gl_FragColor = texture2D(sTexture, texCoord);\n}\n");
                break;
            case 9:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nuniform vec2 uPosition;\nvoid main() {\n    vec2 texCoord = vTextureCoord.xy;\n    vec2 normCoord = 2.0 * texCoord - 1.0;\n    float r = length(normCoord); // to polar coords \n    float phi = atan(normCoord.y + uPosition.y, normCoord.x + uPosition.x); // to polar coords \n    r = r * smoothstep(-0.1, 0.5, r);\n    normCoord.x = r * cos(phi); \n    normCoord.y = r * sin(phi); \n    texCoord = normCoord / 2.0 + 0.5;\n    gl_FragColor = texture2D(sTexture, texCoord);\n}\n");
                break;
            case 10:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nuniform vec2 uPosition;\nvoid main() {\n    vec2 texCoord = vTextureCoord.xy;\n    vec2 normCoord = 2.0 * texCoord - 1.0;\n    float r = length(normCoord); // to polar coords \n    float phi = atan(normCoord.y + uPosition.y, normCoord.x + uPosition.x); // to polar coords \n    r = r * r / sqrt(2.0);\n    normCoord.x = r * cos(phi); \n    normCoord.y = r * sin(phi); \n    texCoord = normCoord / 2.0 + 0.5;\n    gl_FragColor = texture2D(sTexture, texCoord);\n}\n");
                break;
            case 11:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nuniform vec2 uPosition;\nvoid main() {\n    vec2 texCoord = vTextureCoord.xy;\n    vec2 normCoord = 2.0 * texCoord - 1.0;\n    float r = length(normCoord); // to polar coords \n    float phi = atan(normCoord.y + uPosition.y, normCoord.x + uPosition.x); // to polar coords \n    r = 2.0 * r - r * smoothstep(0.0, 0.7, r);\n    normCoord.x = r * cos(phi); \n    normCoord.y = r * sin(phi); \n    texCoord = normCoord / 2.0 + 0.5;\n    gl_FragColor = texture2D(sTexture, texCoord);\n}\n");
                break;
            case 12:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nuniform vec2 uPosition;\nvoid main() {\n    vec2 texCoord = vTextureCoord.xy;\n    vec2 normCoord = 2.0 * texCoord - 1.0;\n    normCoord.x = normCoord.x * sign(normCoord.x + uPosition.x);\n    texCoord = normCoord / 2.0 + 0.5;\n    gl_FragColor = texture2D(sTexture, texCoord);\n}\n");
                break;
            case 13:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nuniform vec2 uPosition;\nvoid main() {\n    vec2 texCoord = vTextureCoord.xy;\n    vec2 normCoord = 2.0 * texCoord - 1.0;\n    vec2 s = sign(normCoord + uPosition);\n    normCoord = abs(normCoord);\n    normCoord = 0.5 * normCoord + 0.5 * smoothstep(0.25, 0.5, normCoord) * normCoord;\n    normCoord = s * normCoord;\n    texCoord = normCoord / 2.0 + 0.5;\n    gl_FragColor = texture2D(sTexture, texCoord);\n}\n");
                break;
            case 14:
                this.n = '赥';
                this.d = GlUtil.createProgram("uniform mat4 uMVPMatrix;\nuniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n    gl_Position = uMVPMatrix * aPosition;\n    vTextureCoord = (uTexMatrix * aTextureCoord).xy;\n}\n", "#extension GL_OES_EGL_image_external : require\n#define KERNEL_SIZE 9\nprecision highp float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nuniform float uKernel[KERNEL_SIZE];\nuniform vec2 uTexOffset[KERNEL_SIZE];\nuniform float uColorAdjust;\nvoid main() {\n    int i = 0;\n    vec4 sum = vec4(0.0);\n    for (i = 0; i < KERNEL_SIZE; i++) {\n            vec4 texc = texture2D(sTexture, vTextureCoord + uTexOffset[i]);\n            sum += texc * uKernel[i];\n    }\n    sum += uColorAdjust;\n    gl_FragColor = sum;\n}\n");
                break;
            default:
                throw new RuntimeException("Unhandled type " + programType);
        }

        if (this.d == 0) {
            throw new RuntimeException("Unable to create program");
        } else {
            this.j = GLES20.glGetAttribLocation(this.d, "aPosition");
            GlUtil.checkLocation(this.j, "aPosition");
            this.k = GLES20.glGetAttribLocation(this.d, "aTextureCoord");
            GlUtil.checkLocation(this.k, "aTextureCoord");
            this.e = GLES20.glGetUniformLocation(this.d, "uMVPMatrix");
            GlUtil.checkLocation(this.e, "uMVPMatrix");
            this.f = GLES20.glGetUniformLocation(this.d, "uTexMatrix");
            GlUtil.checkLocation(this.f, "uTexMatrix");
            this.m = GLES20.glGetUniformLocation(this.d, "opacity");
            if (this.m < 0) {
                this.m = -1;
            } else {
                GlUtil.checkLocation(this.m, "opacity");
            }

            this.g = GLES20.glGetUniformLocation(this.d, "uKernel");
            if (this.g < 0) {
                this.g = -1;
                this.h = -1;
                this.i = -1;
            } else {
                this.h = GLES20.glGetUniformLocation(this.d, "uTexOffset");
                GlUtil.checkLocation(this.h, "uTexOffset");
                this.i = GLES20.glGetUniformLocation(this.d, "uColorAdjust");
                GlUtil.checkLocation(this.i, "uColorAdjust");
                this.setKernel(new float[]{0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F}, 0.0F);
                this.setTexSize(256, 256);
            }

            this.l = GLES20.glGetUniformLocation(this.d, "uPosition");
            if (this.l < 0) {
                this.l = -1;
            }

        }
    }

    public void release() {
        GLES20.glDeleteProgram(this.d);
        this.d = -1;
    }

    public Texture2dProgram.ProgramType getProgramType() {
        return this.a;
    }

    public int createTextureObject() {
        int[] var1 = new int[1];
        GLES20.glGenTextures(1, var1, 0);
        GlUtil.checkGlError("glGenTextures");
        int var2 = var1[0];
        GLES20.glBindTexture(this.n, var2);
        GlUtil.checkGlError("glBindTexture " + var2);
        GLES20.glTexParameteri('赥', 10241, 9729);
        GLES20.glTexParameteri('赥', 10240, 9729);
        GLES20.glTexParameteri('赥', 10242, '脯');
        GLES20.glTexParameteri('赥', 10243, '脯');
        GlUtil.checkGlError("glTexParameter");
        return var2;
    }

    public void handleTouchEvent(MotionEvent ev) {
        if (ev.getAction() == 2) {
            if (this.c != 0.0F && this.b != 0.0F) {
                this.p[0] += 2.0F * (ev.getX() - this.q[0]) / this.b;
                this.p[1] += 2.0F * (ev.getY() - this.q[1]) / -this.c;
                this.q[0] = ev.getX();
                this.q[1] = ev.getY();
            }
        } else if (ev.getAction() == 0) {
            this.q[0] = ev.getX();
            this.q[1] = ev.getY();
        }

    }

    public void setKernel(float[] values, float colorAdj) {
        if (values.length != 9) {
            throw new IllegalArgumentException("Kernel size is " + values.length + " vs. " + 9);
        } else {
            System.arraycopy(values, 0, this.o, 0, 9);
            this.s = colorAdj;
        }
    }

    public void setOpacity(float opacity) {
        this.t = opacity;
    }

    public void setTexSize(int width, int height) {
        this.c = (float) height;
        this.b = (float) width;
        float var3 = 1.0F / (float) width;
        float var4 = 1.0F / (float) height;
        this.r = new float[]{-var3, -var4, 0.0F, -var4, var3, -var4, -var3, 0.0F, 0.0F, 0.0F, var3, 0.0F, -var3, var4, 0.0F, var4, var3, var4};
    }

    public void draw(float[] mvpMatrix, Drawable2d[] drawables, int firstVertex, float[] texMatrix, int textureId) {
        GlUtil.checkGlError("draw start");
        GLES20.glUseProgram(this.d);
        GlUtil.checkGlError("glUseProgram");
        GLES20.glActiveTexture('蓀');
        GLES20.glBindTexture(this.n, textureId);
        GlUtil.checkGlError("glBindTexture");
        GLES20.glUniformMatrix4fv(this.e, 1, false, mvpMatrix, 0);
        GlUtil.checkGlError("glUniformMatrix4fv");
        GLES20.glUniformMatrix4fv(this.f, 1, false, texMatrix, 0);
        GlUtil.checkGlError("glUniformMatrix4fv");
        GLES20.glEnableVertexAttribArray(this.j);
        GlUtil.checkGlError("glEnableVertexAttribArray");
        Drawable2d[] var6 = drawables;
        int var7 = drawables.length;

        int var8;
        Drawable2d var9;
        for (var8 = 0; var8 < var7; ++var8) {
            var9 = var6[var8];
            GLES20.glVertexAttribPointer(this.j, var9.getCoordsPerVertex(), 5126, false, var9.getVertexStride(), var9.getVertexArray());
            GlUtil.checkGlError("glVertexAttribPointer");
        }

        GLES20.glEnableVertexAttribArray(this.k);
        GlUtil.checkGlError("glEnableVertexAttribArray");
        var6 = drawables;
        var7 = drawables.length;

        for (var8 = 0; var8 < var7; ++var8) {
            var9 = var6[var8];
            GLES20.glVertexAttribPointer(this.k, 2, 5126, false, var9.getTexCoordStride(), var9.getTexCoordArray());
            GlUtil.checkGlError("glVertexAttribPointer");
        }

        if (this.g >= 0) {
            GLES20.glUniform1fv(this.g, 9, this.o, 0);
            GLES20.glUniform2fv(this.h, 9, this.r, 0);
            GLES20.glUniform1f(this.i, this.s);
        }

        if (this.m >= 0) {
            GLES20.glUniform1f(this.m, this.t);
        }

        if (this.l >= 0) {
            GLES20.glUniform2fv(this.l, 1, this.p, 0);
        }

        var6 = drawables;
        var7 = drawables.length;

        for (var8 = 0; var8 < var7; ++var8) {
            var9 = var6[var8];
            GLES20.glDrawArrays(5, firstVertex, var9.getVertexCount());
            GlUtil.checkGlError("glDrawArrays");
        }

        GLES20.glDisableVertexAttribArray(this.j);
        GLES20.glDisableVertexAttribArray(this.k);
        GLES20.glBindTexture(this.n, 0);
        GLES20.glUseProgram(0);
    }

    public enum ProgramType {
        TEXTURE_2D,
        TEXTURE_EXT,
        TEXTURE_EXT_BW,
        TEXTURE_EXT_NIGHT,
        TEXTURE_EXT_CHROMA_KEY,
        TEXTURE_EXT_SQUEEZE,
        TEXTURE_EXT_TWIRL,
        TEXTURE_EXT_TUNNEL,
        TEXTURE_EXT_BULGE,
        TEXTURE_EXT_DENT,
        TEXTURE_EXT_FISHEYE,
        TEXTURE_EXT_STRETCH,
        TEXTURE_EXT_MIRROR,
        TEXTURE_EXT_FILT;
    }
}


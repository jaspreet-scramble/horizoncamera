package com.mla.wwl.gles;

import android.graphics.PointF;

import com.mla.wwl.MlaSDKInt;

import java.nio.FloatBuffer;

public class Drawable2d {
    private static final float[] a = new float[]{-1.0F, -1.0F, 1.0F, -1.0F, -1.0F, 1.0F, 1.0F, 1.0F};
    private static final float[] b = new float[]{0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F};
    private static final FloatBuffer c;
    private static final FloatBuffer d;
    private PointF e;
    private PointF f;
    private PointF g;
    private PointF h;
    private FloatBuffer i;
    private FloatBuffer j;
    private FloatBuffer k;
    private int l;
    private int m;
    private int n;
    private int o;
    private Drawable2d.Prefab p;

    private static final float[] a(float var0, float var1) {
        float[] var2 = new float[]{-var0, -var1, var0, -var1, -var0, var1, var0, var1};
        return var2;
    }

    private static final float[] b(float var0, float var1) {
        float var2 = var1 > 1.0F ? 2.0F : 2.0F * var1;
        float var3 = 0.675F * var2 * 110.0F / 1080.0F;
        float var4 = var3 * var0;
        var4 *= MlaSDKInt.WATERMARK_SCALE;
        var3 *= MlaSDKInt.WATERMARK_SCALE;
        float[] var5 = new float[]{-var4, 0.0F, 0.0F, 0.0F, -var4, var3, 0.0F, var3};
        return var5;
    }

    private final float[] a(float var1, float var2, float var3) {
        float var4 = 16.0F * var1 * var3 / (var2 / 2.0F);
        float var5 = 3.2F * var1 * var3 / (var2 / 2.0F);
        float[] var6 = new float[]{var4 + this.e.x, this.e.y, var4 + this.e.x, var5 + this.e.y, this.e.x, this.e.y, var5 + this.e.x, var5 + this.e.y, this.e.x, var4 + this.e.y, var5 + this.e.x, var4 + this.e.y};
        return var6;
    }

    private final float[] b(float var1, float var2, float var3) {
        float var4 = 16.0F * var1 * var3 / (var2 / 2.0F);
        float var5 = 3.2F * var1 * var3 / (var2 / 2.0F);
        float[] var6 = new float[]{-var4 + this.h.x, this.h.y, -var4 + this.h.x, var5 + this.h.y, this.h.x, this.h.y, -var5 + this.h.x, var5 + this.h.y, this.h.x, var4 + this.h.y, -var5 + this.h.x, var4 + this.h.y};
        return var6;
    }

    private final float[] c(float var1, float var2, float var3) {
        float var4 = 16.0F * var1 * var3 / (var2 / 2.0F);
        float var5 = 3.2F * var1 * var3 / (var2 / 2.0F);
        float[] var6 = new float[]{this.f.x, -var4 + this.f.y, var5 + this.f.x, -var4 + this.f.y, this.f.x, this.f.y, var5 + this.f.x, -var5 + this.f.y, var4 + this.f.x, this.f.y, var4 + this.f.x, -var5 + this.f.y};
        return var6;
    }

    private final float[] d(float var1, float var2, float var3) {
        float var4 = 16.0F * var1 * var3 / (var2 / 2.0F);
        float var5 = 3.2F * var1 * var3 / (var2 / 2.0F);
        float[] var6 = new float[]{-var4 + this.g.x, this.g.y, -var4 + this.g.x, -var5 + this.g.y, this.g.x, this.g.y, -var5 + this.g.x, -var5 + this.g.y, this.g.x, -var4 + this.g.y, -var5 + this.g.x, -var4 + this.g.y};
        return var6;
    }

    static class C00601 {
        /* renamed from: a */
        static final /* synthetic */ int[] f362a = new int[Prefab.values().length];

        static {
            try {
                f362a[Prefab.FULL_RECTANGLE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f362a[Prefab.WATERMARK_RECTANGLE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f362a[Prefab.PREV_BOT_LEFT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f362a[Prefab.PREV_BOT_RIGHT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f362a[Prefab.PREV_TOP_LEFT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f362a[Prefab.PREV_TOP_RIGHT.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
        }
    }

    public Drawable2d(Drawable2d.Prefab shape, float w, float h) {
        this(shape, w, h, 1.0F);
    }

    public Drawable2d(Drawable2d.Prefab shape, float w, float h, float containerAspectRatio) {
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.m = 2;
        this.n = this.m * 4;
        this.o = 8;
        this.p = shape;
        switch (C00601.f362a[shape.ordinal()]) {
            case 1:
                this.j = GlUtil.createFloatBuffer(a(w, h));
                this.k = d;
                this.l = a.length / this.m;
                break;
            case 2:
                this.j = GlUtil.createFloatBuffer(b(w / h, containerAspectRatio));
                this.k = d;
                this.l = a.length / this.m;
                break;
            case 3:
                this.e = new PointF(-w, -h);
                this.i = GlUtil.createFloatBuffer(12);
                this.l = 12 / this.m;
                break;
            case 4:
                this.h = new PointF(w, -h);
                this.i = GlUtil.createFloatBuffer(12);
                this.l = 12 / this.m;
                break;
            case 5:
                this.f = new PointF(-w, h);
                this.i = GlUtil.createFloatBuffer(12);
                this.l = 12 / this.m;
                break;
            case 6:
                this.g = new PointF(w, h);
                this.i = GlUtil.createFloatBuffer(12);
                this.l = 12 / this.m;
                break;
            default:
                throw new RuntimeException("Unknown shape " + shape);
        }

    }

    public Drawable2d(Drawable2d.Prefab shape) {
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.m = 2;
        this.n = this.m * 4;
        this.o = 8;
        this.p = shape;
        switch (shape.ordinal()) {
            case 1:
                this.j = c;
                this.k = d;
                this.l = a.length / this.m;
                return;
            default:
                throw new RuntimeException("Unknown shape " + shape);
        }
    }

    public void updateCornerVertices(float scale, float height, float density) {
        switch (C00601.f362a[this.p.ordinal()]) {
            case 3:
                float[] var4 = this.a(scale, height, density);
                this.j = GlUtil.copyCoordsToFloatBuffer(this.i, var4);
                break;
            case 4:
                float[] var5 = this.b(scale, height, density);
                this.j = GlUtil.copyCoordsToFloatBuffer(this.i, var5);
                break;
            case 5:
                float[] var6 = this.c(scale, height, density);
                this.j = GlUtil.copyCoordsToFloatBuffer(this.i, var6);
                break;
            case 6:
                float[] var7 = this.d(scale, height, density);
                this.j = GlUtil.copyCoordsToFloatBuffer(this.i, var7);
        }

    }

    public FloatBuffer getVertexArray() {
        return this.j;
    }

    public FloatBuffer getTexCoordArray() {
        return this.k;
    }

    public int getVertexCount() {
        return this.l;
    }

    public int getVertexStride() {
        return this.n;
    }

    public int getTexCoordStride() {
        return this.o;
    }

    public int getCoordsPerVertex() {
        return this.m;
    }

    public String toString() {
        return this.p != null ? "[Drawable2d: " + this.p + "]" : "[Drawable2d: ...]";
    }

    static {
        c = GlUtil.createFloatBuffer(a);
        d = GlUtil.createFloatBuffer(b);
    }

    public enum Prefab {
        FULL_RECTANGLE,
        WATERMARK_RECTANGLE,
        PREV_BOT_LEFT,
        PREV_BOT_RIGHT,
        PREV_TOP_LEFT,
        PREV_TOP_RIGHT
    }
}
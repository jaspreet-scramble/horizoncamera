package com.mla.wwl.gles;

import com.google.common.base.Preconditions;
import com.mla.wwl.gles.Texture2dProgram.ProgramType;

public class Filters {
  public static final int FILTER_NONE = 0;
  public static final int FILTER_BLACK_WHITE = 1;
  public static final int FILTER_NIGHT = 2;
  public static final int FILTER_CHROMA_KEY = 3;
  public static final int FILTER_BLUR = 4;
  public static final int FILTER_SHARPEN = 5;
  public static final int FILTER_EDGE_DETECT = 6;
  public static final int FILTER_EMBOSS = 7;
  public static final int FILTER_SQUEEZE = 8;
  public static final int FILTER_TWIRL = 9;
  public static final int FILTER_TUNNEL = 10;
  public static final int FILTER_BULGE = 11;
  public static final int FILTER_DENT = 12;
  public static final int FILTER_FISHEYE = 13;
  public static final int FILTER_STRETCH = 14;
  public static final int FILTER_MIRROR = 15;

  public Filters() {
  }

  public static void checkFilterArgument(int filter) {
    Preconditions.checkArgument(filter >= 0 && filter <= 15);
  }

  public static void updateFilter(FullFrameRect rect, int newFilter) {
    float[] var3 = null;
    float var4 = 0.0F;
    ProgramType var2;
    switch(newFilter) {
      case 0:
        var2 = ProgramType.TEXTURE_EXT;
        break;
      case 1:
        var2 = ProgramType.TEXTURE_EXT_BW;
        break;
      case 2:
        var2 = ProgramType.TEXTURE_EXT_NIGHT;
        break;
      case 3:
        var2 = ProgramType.TEXTURE_EXT_CHROMA_KEY;
        break;
      case 4:
        var2 = ProgramType.TEXTURE_EXT_FILT;
        var3 = new float[]{0.0625F, 0.125F, 0.0625F, 0.125F, 0.25F, 0.125F, 0.0625F, 0.125F, 0.0625F};
        break;
      case 5:
        var2 = ProgramType.TEXTURE_EXT_FILT;
        var3 = new float[]{0.0F, -1.0F, 0.0F, -1.0F, 5.0F, -1.0F, 0.0F, -1.0F, 0.0F};
        break;
      case 6:
        var2 = ProgramType.TEXTURE_EXT_FILT;
        var3 = new float[]{-1.0F, -1.0F, -1.0F, -1.0F, 8.0F, -1.0F, -1.0F, -1.0F, -1.0F};
        break;
      case 7:
        var2 = ProgramType.TEXTURE_EXT_FILT;
        var3 = new float[]{2.0F, 0.0F, 0.0F, 0.0F, -1.0F, 0.0F, 0.0F, 0.0F, -1.0F};
        var4 = 0.5F;
        break;
      case 8:
        var2 = ProgramType.TEXTURE_EXT_SQUEEZE;
        break;
      case 9:
        var2 = ProgramType.TEXTURE_EXT_TWIRL;
        break;
      case 10:
        var2 = ProgramType.TEXTURE_EXT_TUNNEL;
        break;
      case 11:
        var2 = ProgramType.TEXTURE_EXT_BULGE;
        break;
      case 12:
        var2 = ProgramType.TEXTURE_EXT_DENT;
        break;
      case 13:
        var2 = ProgramType.TEXTURE_EXT_FISHEYE;
        break;
      case 14:
        var2 = ProgramType.TEXTURE_EXT_STRETCH;
        break;
      case 15:
        var2 = ProgramType.TEXTURE_EXT_MIRROR;
        break;
      default:
        throw new RuntimeException("Unknown filter mode " + newFilter);
    }

    if(var2 != rect.getProgram().getProgramType()) {
      rect.changeProgram(new Texture2dProgram(var2));
    }

    if(var3 != null) {
      rect.getProgram().setKernel(var3, var4);
    }

  }
}


package com.mla.wwl.gles;

import android.graphics.SurfaceTexture;
import android.view.Surface;

public class WindowSurface extends EglSurfaceBase {
    private Surface a;
    private boolean b;

    public WindowSurface(EglCore eglCore, Surface surface, boolean releaseSurface) {
        super(eglCore);
        this.createWindowSurface(surface);
        this.a = surface;
        this.b = releaseSurface;
    }

    public WindowSurface(EglCore eglCore, SurfaceTexture surfaceTexture) {
        super(eglCore);
        this.createWindowSurface(surfaceTexture);
    }

    public void release() {
        this.releaseEglSurface();
        if (this.a != null) {
            if (this.b) {
                this.a.release();
            }

            this.a = null;
        }

    }

    public void recreate(EglCore newEglCore) {
        if (this.a == null) {
            throw new RuntimeException("not yet implemented for SurfaceTexture");
        } else {
            this.mEglCore = newEglCore;
            this.createWindowSurface(this.a);
        }
    }
}
package com.mla.wwl.gles;

import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLExt;
import android.opengl.EGLSurface;
import android.util.Log;
import android.view.Surface;

public final class EglCore {
  public static final int FLAG_RECORDABLE = 1;
  public static final int FLAG_TRY_GLES3 = 2;
  private EGLDisplay a;
  private EGLContext b;
  private EGLConfig c;
  private int d;

  public EglCore() {
    this((EGLContext)null, 0);
  }

  public EglCore(EGLContext sharedContext, int flags) {
    this.a = EGL14.EGL_NO_DISPLAY;
    this.b = EGL14.EGL_NO_CONTEXT;
    this.c = null;
    this.d = -1;
    if(this.a != EGL14.EGL_NO_DISPLAY) {
      throw new RuntimeException("EGL already set up");
    } else {
      if(sharedContext == null) {
        sharedContext = EGL14.EGL_NO_CONTEXT;
      }

      this.a = EGL14.eglGetDisplay(0);
      if(this.a == EGL14.EGL_NO_DISPLAY) {
        throw new RuntimeException("unable to get EGL14 display");
      } else {
        int[] var3 = new int[2];
        if(!EGL14.eglInitialize(this.a, var3, 0, var3, 1)) {
          this.a = null;
          throw new RuntimeException("unable to initialize EGL14");
        } else {
          EGLConfig var4;
          int[] var5;
          EGLContext var6;
          if((flags & 2) != 0) {
            var4 = this.a(flags, 3);
            if(var4 != null) {
              var5 = new int[]{12440, 3, 12344};
              var6 = EGL14.eglCreateContext(this.a, var4, sharedContext, var5, 0);
              if(EGL14.eglGetError() == 12288) {
                this.c = var4;
                this.b = var6;
                this.d = 3;
              }
            }
          }

          if(this.b == EGL14.EGL_NO_CONTEXT) {
            var4 = this.a(flags, 2);
            if(var4 == null) {
              throw new RuntimeException("Unable to find a suitable EGLConfig");
            }

            var5 = new int[]{12440, 2, 12344};
            var6 = EGL14.eglCreateContext(this.a, var4, sharedContext, var5, 0);
            this.a("eglCreateContext");
            this.c = var4;
            this.b = var6;
            this.d = 2;
          }

          int[] var7 = new int[1];
          EGL14.eglQueryContext(this.a, this.b, 12440, var7, 0);
        }
      }
    }
  }

  private EGLConfig a(int var1, int var2) {
    int var3 = 4;
    if(var2 >= 3) {
      var3 |= 64;
    }

    int[] var4 = new int[]{12324, 8, 12323, 8, 12322, 8, 12321, 8, 12352, var3, 12344, 0, 12344};
    if((var1 & 1) != 0) {
      var4[var4.length - 3] = 12610;
      var4[var4.length - 2] = 1;
    }

    EGLConfig[] var5 = new EGLConfig[1];
    int[] var6 = new int[1];
    if(!EGL14.eglChooseConfig(this.a, var4, 0, var5, 0, var5.length, var6, 0)) {
      Log.w("EglCore", "unable to find RGB8888 / " + var2 + " EGLConfig");
      return null;
    } else {
      return var5[0];
    }
  }

  public void release() {
    if(this.a != EGL14.EGL_NO_DISPLAY) {
      EGL14.eglMakeCurrent(this.a, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
      EGL14.eglDestroyContext(this.a, this.b);
      EGL14.eglReleaseThread();
      EGL14.eglTerminate(this.a);
    }

    this.a = EGL14.EGL_NO_DISPLAY;
    this.b = EGL14.EGL_NO_CONTEXT;
    this.c = null;
  }

  protected void finalize() throws Throwable {
    try {
      if(this.a != EGL14.EGL_NO_DISPLAY) {
        Log.w("EglCore", "WARNING: EglCore was not explicitly released -- state may be leaked");
        this.release();
      }
    } finally {
      super.finalize();
    }

  }

  public void releaseSurface(EGLSurface eglSurface) {
    EGL14.eglDestroySurface(this.a, eglSurface);
  }

  public EGLSurface createWindowSurface(Object surface) {
    if(!(surface instanceof Surface) && !(surface instanceof SurfaceTexture)) {
      throw new RuntimeException("invalid surface: " + surface);
    } else {
      int[] var2 = new int[]{12344};
      EGLSurface var3 = EGL14.eglCreateWindowSurface(this.a, this.c, surface, var2, 0);
      this.a("eglCreateWindowSurface");
      if(var3 == null) {
        throw new RuntimeException("surface was null");
      } else {
        return var3;
      }
    }
  }

  public EGLSurface createOffscreenSurface(int width, int height) {
    int[] var3 = new int[]{12375, width, 12374, height, 12344};
    EGLSurface var4 = EGL14.eglCreatePbufferSurface(this.a, this.c, var3, 0);
    this.a("eglCreatePbufferSurface");
    if(var4 == null) {
      throw new RuntimeException("surface was null");
    } else {
      return var4;
    }
  }

  public void makeCurrent(EGLSurface eglSurface) {
    if(this.a == EGL14.EGL_NO_DISPLAY) {
      Log.w("EglCore", "NOTE: makeCurrent w/o display");
    }

    if(!EGL14.eglMakeCurrent(this.a, eglSurface, eglSurface, this.b)) {
      GlUtil.checkGlError("eglMakeCurrent(surface)");
      throw new RuntimeException("eglMakeCurrent failed");
    }
  }

  public void makeCurrent(EGLSurface drawSurface, EGLSurface readSurface) {
    if(this.a == EGL14.EGL_NO_DISPLAY) {
      Log.w("EglCore", "NOTE: makeCurrent w/o display");
    }

    if(!EGL14.eglMakeCurrent(this.a, drawSurface, readSurface, this.b)) {
      throw new RuntimeException("eglMakeCurrent(draw,read) failed");
    }
  }

  public void makeNothingCurrent() {
    if(!EGL14.eglMakeCurrent(this.a, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT)) {
      throw new RuntimeException("eglMakeCurrent failed");
    }
  }

  public boolean swapBuffers(EGLSurface eglSurface) {
    return EGL14.eglSwapBuffers(this.a, eglSurface);
  }

  public void setPresentationTime(EGLSurface eglSurface, long nsecs) {
    EGLExt.eglPresentationTimeANDROID(this.a, eglSurface, nsecs);
  }

  public boolean isCurrent(EGLSurface eglSurface) {
    return this.b.equals(EGL14.eglGetCurrentContext()) && eglSurface.equals(EGL14.eglGetCurrentSurface(12377));
  }

  public int querySurface(EGLSurface eglSurface, int what) {
    int[] var3 = new int[1];
    EGL14.eglQuerySurface(this.a, eglSurface, what, var3, 0);
    return var3[0];
  }

  public int getGlVersion() {
    return this.d;
  }

  public static void logCurrent(String msg) {
    EGLDisplay var1 = EGL14.eglGetCurrentDisplay();
    EGLContext var2 = EGL14.eglGetCurrentContext();
    EGLSurface var3 = EGL14.eglGetCurrentSurface(12377);
    Log.i("EglCore", "Current EGL (" + msg + "): display=" + var1 + ", context=" + var2 + ", surface=" + var3);
  }

  private void a(String var1) {
    int var2;
    if((var2 = EGL14.eglGetError()) != 12288) {
      throw new RuntimeException(var1 + ": EGL error: 0x" + Integer.toHexString(var2));
    }
  }
}

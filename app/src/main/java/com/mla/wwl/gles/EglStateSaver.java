package com.mla.wwl.gles;

import android.opengl.EGL14;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;

public class EglStateSaver {
    private EGLContext a;
    private EGLSurface b;
    private EGLSurface c;
    private EGLDisplay d;

    public EglStateSaver() {
        this.a = EGL14.EGL_NO_CONTEXT;
        this.b = EGL14.EGL_NO_SURFACE;
        this.c = EGL14.EGL_NO_SURFACE;
        this.d = EGL14.EGL_NO_DISPLAY;
    }

    public void saveEGLState() {
        this.a = EGL14.eglGetCurrentContext();
        this.b = EGL14.eglGetCurrentSurface(12378);
        this.c = EGL14.eglGetCurrentSurface(12377);
        this.d = EGL14.eglGetCurrentDisplay();
    }

    public EGLContext getSavedEGLContext() {
        return this.a;
    }

    public void makeSavedStateCurrent() {
        EGL14.eglMakeCurrent(this.d, this.b, this.c, this.a);
    }

    public void makeNothingCurrent() {
        EGL14.eglMakeCurrent(this.d, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
    }

    public void logState() {
        if (!this.a.equals(EGL14.eglGetCurrentContext())) {
            ;
        }

        if (!this.b.equals(EGL14.eglGetCurrentSurface(12378)) && this.b.equals(EGL14.EGL_NO_SURFACE)) {
            ;
        }

        if (!this.c.equals(EGL14.eglGetCurrentSurface(12377)) && this.c.equals(EGL14.EGL_NO_SURFACE)) {
            ;
        }

        if (!this.d.equals(EGL14.eglGetCurrentDisplay())) {
            ;
        }

    }
}


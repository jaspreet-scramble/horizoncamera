package com.mla.wwl.gles;

import android.opengl.EGL14;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class EglSurfaceBase {
  protected static final String TAG = "EglSurfaceBase";
  protected EglCore mEglCore;
  private EGLSurface a;
  private int b;
  private int c;

  public EglSurfaceBase(EglCore eglBase) {
    this.a = EGL14.EGL_NO_SURFACE;
    this.b = -1;
    this.c = -1;
    this.mEglCore = eglBase;
  }

  public void createWindowSurface(Object surface) {
    if(this.a != EGL14.EGL_NO_SURFACE) {
      throw new IllegalStateException("surface already created");
    } else {
      this.a = this.mEglCore.createWindowSurface(surface);
      this.b = this.mEglCore.querySurface(this.a, 12375);
      this.c = this.mEglCore.querySurface(this.a, 12374);
    }
  }

  public void createOffscreenSurface(int width, int height) {
    if(this.a != EGL14.EGL_NO_SURFACE) {
      throw new IllegalStateException("surface already created");
    } else {
      this.a = this.mEglCore.createOffscreenSurface(width, height);
      this.b = width;
      this.c = height;
    }
  }

  public int getWidth() {
    return this.b;
  }

  public int getHeight() {
    return this.c;
  }

  public void releaseEglSurface() {
    this.mEglCore.releaseSurface(this.a);
    this.a = EGL14.EGL_NO_SURFACE;
    this.b = this.c = -1;
  }

  public void makeCurrent() {
    this.mEglCore.makeCurrent(this.a);
  }

  public void makeCurrentReadFrom(EglSurfaceBase readSurface) {
    this.mEglCore.makeCurrent(this.a, readSurface.a);
  }

  public boolean swapBuffers() {
    boolean var1 = this.mEglCore.swapBuffers(this.a);
    if(!var1) {
      Log.d("EglSurfaceBase", "WARNING: swapBuffers() failed");
    }

    return var1;
  }

  public void setPresentationTime(long nsecs) {
    this.mEglCore.setPresentationTime(this.a, nsecs);
  }

  public ByteBuffer saveFrame() {
    if(!this.mEglCore.isCurrent(this.a)) {
      throw new RuntimeException("Expected EGL context/surface is not current");
    } else {
      ByteBuffer var1 = ByteBuffer.allocateDirect(this.b * this.c * 4);
      var1.order(ByteOrder.LITTLE_ENDIAN);
      GLES20.glReadPixels(0, 0, this.b, this.c, 6408, 5121, var1);
      var1.rewind();
      var1.clear();
      return var1;
    }
  }
}

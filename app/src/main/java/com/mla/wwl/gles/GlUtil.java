package com.mla.wwl.gles;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GlUtil {
  public static final int SIZEOF_FLOAT = 4;
  public static final float[] IDENTITY_MATRIX = new float[16];

  private GlUtil() {
  }

  public static int createProgram(String vertexSource, String fragmentSource) {
    int var2 = loadShader('謱', vertexSource);
    if(var2 == 0) {
      return 0;
    } else {
      int var3 = loadShader('謰', fragmentSource);
      if(var3 == 0) {
        return 0;
      } else {
        int var4 = GLES20.glCreateProgram();
        checkGlError("glCreateProgram");
        if(var4 == 0) {
          Log.e("GlUtil", "Could not create program");
        }

        GLES20.glAttachShader(var4, var2);
        checkGlError("glAttachShader");
        GLES20.glAttachShader(var4, var3);
        checkGlError("glAttachShader");
        GLES20.glLinkProgram(var4);
        int[] var5 = new int[1];
        GLES20.glGetProgramiv(var4, '讂', var5, 0);
        if(var5[0] != 1) {
          Log.e("GlUtil", "Could not link program: ");
          Log.e("GlUtil", GLES20.glGetProgramInfoLog(var4));
          GLES20.glDeleteProgram(var4);
          var4 = 0;
        }

        return var4;
      }
    }
  }

  public static int loadShader(int shaderType, String source) {
    int var2 = GLES20.glCreateShader(shaderType);
    checkGlError("glCreateShader type=" + shaderType);
    GLES20.glShaderSource(var2, source);
    GLES20.glCompileShader(var2);
    int[] var3 = new int[1];
    GLES20.glGetShaderiv(var2, '讁', var3, 0);
    if(var3[0] == 0) {
      Log.e("GlUtil", "Could not compile shader " + shaderType + ":");
      Log.e("GlUtil", " " + GLES20.glGetShaderInfoLog(var2));
      GLES20.glDeleteShader(var2);
      var2 = 0;
    }

    return var2;
  }

  public static void checkGlError(String op) {
    int var1 = GLES20.glGetError();
    if(var1 != 0) {
      String var2 = op + ": glError 0x" + Integer.toHexString(var1);
      Log.e("GlUtil", var2);
    }

  }

  public static void checkLocation(int location, String label) {
    if(location < 0) {
      throw new RuntimeException("Unable to locate '" + label + "' in program");
    }
  }

  public static FloatBuffer createFloatBuffer(float[] coords) {
    ByteBuffer var1 = ByteBuffer.allocateDirect(coords.length * 4);
    var1.order(ByteOrder.nativeOrder());
    FloatBuffer var2 = var1.asFloatBuffer();
    var2.put(coords);
    var2.position(0);
    return var2;
  }

  public static FloatBuffer createFloatBuffer(int length) {
    ByteBuffer var1 = ByteBuffer.allocateDirect(length * 4);
    var1.order(ByteOrder.nativeOrder());
    FloatBuffer var2 = var1.asFloatBuffer();
    return var2;
  }

  public static FloatBuffer copyCoordsToFloatBuffer(FloatBuffer fb, float[] coords) {
    fb.position(0);
    fb.put(coords);
    fb.position(0);
    return fb;
  }

  public static void logVersionInfo() {
    Log.i("GlUtil", "vendor  : " + GLES20.glGetString(7936));
    Log.i("GlUtil", "renderer: " + GLES20.glGetString(7937));
    Log.i("GlUtil", "version : " + GLES20.glGetString(7938));
  }

  public static int createTextureWithTextContent(String text) {
    Bitmap var1 = Bitmap.createBitmap(256, 256, Config.ARGB_8888);
    Canvas var2 = new Canvas(var1);
    var2.drawARGB(0, 0, 255, 0);
    Paint var3 = new Paint();
    var3.setTextSize(32.0F);
    var3.setAntiAlias(true);
    var3.setARGB(255, 255, 255, 255);
    var2.drawText(text, 16.0F, 112.0F, var3);
    int[] var4 = new int[1];
    GLES20.glGenTextures(1, var4, 0);
    GLES20.glBindTexture(3553, var4[0]);
    GLES20.glTexParameterf(3553, 10241, 9728.0F);
    GLES20.glTexParameterf(3553, 10240, 9729.0F);
    GLES20.glTexParameterf(3553, 10242, 10497.0F);
    GLES20.glTexParameterf(3553, 10243, 10497.0F);
    GLUtils.texImage2D(3553, 0, var1, 0);
    var1.recycle();
    return var4[0];
  }

  public static int loadGLTextureFromBitmap(Bitmap bitmap) {
    int[] var1 = new int[1];
    GLES20.glGenTextures(1, var1, 0);
    checkGlError("glGenTextures");
    GLES20.glBindTexture(3553, var1[0]);
    checkGlError("glBindTexture " + var1[0]);
    GLES20.glTexParameterf(3553, 10241, 9985.0F);
    GLES20.glTexParameterf(3553, 10240, 9729.0F);
    GLES20.glTexParameterf(3553, 10242, 33071.0F);
    GLES20.glTexParameterf(3553, 10243, 33071.0F);
    checkGlError("glTexParameter");
    GLUtils.texImage2D(3553, 0, bitmap, 0);
    GLES20.glGenerateMipmap(3553);
    checkGlError("glGenerateMipmap");
    return var1[0];
  }

  public static int loadGLTextureFromResource(Drawable image, boolean scaleToPO2) {
    Bitmap var2 = null;
    int var3 = image.getIntrinsicWidth();
    int var4 = image.getIntrinsicHeight();
    int var5 = getNextHighestPO2(var3);
    int var6 = getNextHighestPO2(var4);
    if(scaleToPO2) {
      image.setBounds(0, 0, var5, var6);
    } else {
      image.setBounds(0, 0, var3, var4);
    }

    if(scaleToPO2) {
      var2 = Bitmap.createBitmap(var5, var6, Config.ARGB_8888);
    } else {
      var2 = Bitmap.createBitmap(var3, var4, Config.ARGB_8888);
    }

    Canvas var7 = new Canvas(var2);
    var2.eraseColor(0);
    var7.save();
    var7.scale(1.0F, -1.0F);
    float var8 = scaleToPO2?(float)var6:(float)var4;
    var7.translate(0.0F, -var8);
    image.draw(var7);
    var7.restore();
    int var9 = loadGLTextureFromBitmap(var2);
    var2.recycle();
    return var9;
  }

  public static int createTextureObject(int target) {
    int[] var1 = new int[1];
    GLES20.glGenTextures(1, var1, 0);
    checkGlError("glGenTextures");
    int var2 = var1[0];
    GLES20.glBindTexture(target, var2);
    checkGlError("glBindTexture " + var2);
    GLES20.glTexParameteri(target, 10241, 9729);
    GLES20.glTexParameteri(target, 10240, 9729);
    GLES20.glTexParameteri(target, 10242, '脯');
    GLES20.glTexParameteri(target, 10243, '脯');
    checkGlError("glTexParameter");
    return var2;
  }

  public static void deleteTexture(int textureId) {
    int[] var1 = new int[]{textureId};
    GLES20.glDeleteTextures(1, var1, 0);
    checkGlError("deleteTexture");
  }

  public static int getNextHighestPO2(int n) {
    --n;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n |= n >> 32;
    return n + 1;
  }

  static {
    Matrix.setIdentityM(IDENTITY_MATRIX, 0);
  }
}
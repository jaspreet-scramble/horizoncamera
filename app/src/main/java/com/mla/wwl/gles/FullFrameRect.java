package com.mla.wwl.gles;

import android.graphics.Color;
import android.graphics.Point;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.view.MotionEvent;
import android.view.animation.DecelerateInterpolator;

import com.mla.wwl.WWLView.FillMode;
import com.mla.wwl.WWLView.ViewType;
import com.mla.wwl.MlaSDKInt;
import com.mla.wwl.gles.Drawable2d.Prefab;
import com.mla.wwl.gles.Texture2dProgram.ProgramType;
import com.mla.wwl.hEngine.ValueInterpolator;

public class FullFrameRect {
    private Drawable2d[] c = null;
    private Drawable2d[] d = null;
    private Drawable2d[] e = null;
    private Drawable2d[] f = null;
    private Drawable2d[] g = null;
    private int h = 1;
    private int i = 1;
    private float j = 1.0F;
    private int k = 1;
    private int l = 1;
    private boolean m = true;
    private float n = 1.0F;
    private float o = 1.0F;
    private float p = 1.0F;
    private float q = 1.0F;
    private float r = 1.0F;
    private float s = 1.0F;
    private float t = 1.0F;
    private final float u;
    private final float v;
    private ValueInterpolator w = new ValueInterpolator(new DecelerateInterpolator(), 400.0F);
    private boolean x = false;
    private boolean y = false;
    private ViewType z;
    private boolean A = true;
    private boolean B = false;
    private boolean C = false;
    private float D = 1.0F;
    private float E = 0.0F;
    private float F = 1.0F;
    private final float[] G = new float[16];
    private final float[] H = new float[16];
    private final float[] I = new float[16];
    private final float[] J = new float[16];
    private final float[] K = new float[16];
    private final float[] L = new float[16];
    private Texture2dProgram M;
    private Texture2dProgram N;
    private FlatShadedProgram O;
    private static final float[] P = new float[]{1.0F, 1.0F, 1.0F, 1.0F};
    private float[] Q = new float[]{0.5F, 0.5F, 0.5F, 1.0F};
    private int R;
    private float S = 0.0F;
    private boolean T = false;
    final float[] a = new float[16];
    final float[] b = new float[16];

    public FullFrameRect(Texture2dProgram program, boolean isInPreview, ViewType viewType, float screenDensity) {
        this.M = program;
        this.N = new Texture2dProgram(ProgramType.TEXTURE_2D);
        this.O = new FlatShadedProgram();
        this.y = isInPreview;
        this.z = viewType;
        this.v = screenDensity;
        this.R = GlUtil.loadGLTextureFromResource(MlaSDKInt.WATERMARK_DRAWABLE, true);
        this.u = (float) MlaSDKInt.WATERMARK_DRAWABLE.getIntrinsicWidth() / (float) MlaSDKInt.WATERMARK_DRAWABLE.getIntrinsicHeight();
    }

    public void setSize(int width, int height) {
        this.h = width;
        this.i = height;
        this.j = (float) this.h / (float) this.i;
        Matrix.setIdentityM(this.H, 0);
        Matrix.scaleM(this.H, 0, 1.0F / this.j, 1.0F, 1.0F);
        GLES20.glViewport(0, 0, this.h, this.i);
        this.a();
    }

    public void setSourceSize(int width, int height, boolean cameraFacingBack) {
        this.k = width;
        this.l = height;
        this.p = (float) this.k / (float) this.l;
        if (this.M != null) {
            this.M.setTexSize(width, height);
        }

        this.m = cameraFacingBack;
        this.a();
    }

    public void setOutputSize(int width, int height) {
        this.t = (float) width / (float) height;
        this.a();
    }

    private void a() {
        if (this.z == ViewType.NORMAL && this.T) {
            this.q = 1.0F / this.p;
            this.n = 1.0F;
            this.o = 1.0F / this.p;
            this.r = this.t * 1.0F / this.p;
            this.s = 1.0F / this.p;
        } else {
            this.q = this.z == ViewType.NORMAL ? this.p : this.t;
            this.n = this.p;
            this.o = 1.0F;
            this.r = this.t;
            this.s = 1.0F;
        }

        this.c = new Drawable2d[]{new Drawable2d(Prefab.FULL_RECTANGLE, this.n, this.o)};
        this.d = new Drawable2d[]{new Drawable2d(Prefab.FULL_RECTANGLE, this.r, this.s)};
        this.g = new Drawable2d[]{new Drawable2d(Prefab.PREV_BOT_LEFT, this.r, this.s), new Drawable2d(Prefab.PREV_BOT_RIGHT, this.r, this.s), new Drawable2d(Prefab.PREV_TOP_RIGHT, this.r, this.s), new Drawable2d(Prefab.PREV_TOP_LEFT, this.r, this.s)};
        this.f = new Drawable2d[]{new Drawable2d(Prefab.WATERMARK_RECTANGLE, this.u, 1.0F, this.j)};
    }

    public void release() {
        if (this.M != null) {
            this.M.release();
            this.M = null;
        }

        if (this.O != null) {
            this.O.release();
            this.O = null;
        }

        if (this.N != null) {
            this.N.release();
            this.N = null;
        }

        if (this.R != 0) {
            GlUtil.deleteTexture(this.R);
        }

    }

    public Texture2dProgram getProgram() {
        return this.M;
    }

    public void changeProgram(Texture2dProgram program) {
        this.M.release();
        this.M = program;
    }

    public int createTextureObject() {
        return this.M.createTextureObject();
    }

    public void drawFrame(int textureId, float[] texMatrix, float angle, float scaleFactor) {
        long var5 = System.nanoTime();
        this.E = angle;
        this.F = scaleFactor;
        Matrix.setIdentityM(this.G, 0);
        float var7 = 1.0F;
        if (this.x && this.q < this.j) {
            var7 = this.j / this.q;
        } else if (!this.x && this.q > this.j) {
            var7 = this.j / this.q;
        }

        if (this.y) {
            var7 = this.w.getValueForInput(var7, var5);
        }

        this.D = var7;
        Matrix.scaleM(this.G, 0, var7, var7, 1.0F);
        if (this.z == ViewType.NORMAL && this.S != 0.0F) {
            float var8 = !this.m && !this.B ? -1.0F : 1.0F;
            Matrix.rotateM(this.G, 0, var8 * this.S, 0.0F, 0.0F, 1.0F);
        }

        Matrix.multiplyMM(this.I, 0, this.H, 0, this.G, 0);
        this.a(this.J, angle, scaleFactor);
        Matrix.multiplyMM(this.L, 0, this.I, 0, this.J, 0);
        System.arraycopy(this.L, 0, this.J, 0, 16);
        if (this.z == ViewType.NORMAL) {
            this.b(this.K, angle, scaleFactor);
            Matrix.multiplyMM(this.L, 0, this.I, 0, this.K, 0);
            System.arraycopy(this.L, 0, this.K, 0, 16);
            this.a(this.J, this.K, textureId, texMatrix, var7, scaleFactor);
        } else {
            this.a(this.J, this.I, textureId, texMatrix, scaleFactor);
        }

    }

    private void a(float[] var1, float[] var2, int var3, float[] var4, float var5, float var6) {
        GLES20.glViewport(0, 0, this.h, this.i);
        GLES20.glEnable(3042);
        GLES20.glClear(16640);
        GLES20.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
        if (this.A) {
            GLES20.glBlendFuncSeparate(1, 0, 1, 771);
            this.O.draw(var1, this.Q, this.c);
            this.O.draw(var2, P, this.d);
            GLES20.glBlendFuncSeparate(774, 0, 1, 771);
        } else {
            GLES20.glBlendFuncSeparate(1, 771, 1, 771);
        }

        this.M.draw(var1, this.c, 0, var4, var3);
        if (this.A) {
            float var7 = var6 / var5;
            this.g[0].updateCornerVertices(var7, (float) this.i, this.v);
            this.g[1].updateCornerVertices(var7, (float) this.i, this.v);
            this.g[2].updateCornerVertices(var7, (float) this.i, this.v);
            this.g[3].updateCornerVertices(var7, (float) this.i, this.v);
            GLES20.glBlendFuncSeparate(1, 771, 1, 771);
            this.O.draw(var2, P, this.g);
        }

        if (this.C) {
            this.a(this.R);
        }

    }

    private void a(float[] var1, float[] var2, int var3, float[] var4, float var5) {
        GLES20.glViewport(0, 0, this.h, this.i);
        GLES20.glEnable(3042);
        GLES20.glClear(16640);
        if (this.t != this.j) {
            GLES20.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
            GLES20.glBlendFuncSeparate(1, 0, 1, 771);
            this.O.draw(var2, P, this.d);
            GLES20.glBlendFuncSeparate(774, 0, 1, 771);
        } else {
            GLES20.glBlendFuncSeparate(1, 771, 1, 771);
        }

        this.M.draw(var1, this.c, 0, var4, var3);
        if (this.C) {
            this.a(this.R);
        }

    }

    private void a(int var1) {
        float[] var2 = new float[16];
        Matrix.setIdentityM(var2, 0);
        float var3 = this.j;
        float var4 = -1.0F;
        float var5 = this.j > 1.0F ? 2.0F : 2.0F * this.j;
        float var6 = 0.06F * var5;
        float var7 = -0.048F * var5;
        Matrix.translateM(var2, 0, var3 - var6, var4 - var7, 0.0F);
        Matrix.multiplyMM(this.L, 0, this.H, 0, var2, 0);
        System.arraycopy(this.L, 0, var2, 0, 16);
        GLES20.glBlendFuncSeparate(1, 771, 1, 771);
        this.N.setOpacity(MlaSDKInt.WATERMARK_OPACITY);
        this.N.draw(var2, this.f, 0, GlUtil.IDENTITY_MATRIX, var1);
    }

    private void a(float[] var1, float var2, float var3) {
        if (this.z == ViewType.NORMAL) {
            Matrix.setIdentityM(var1, 0);
            float var4 = !this.m && !this.B ? -1.0F : 1.0F;
            if (var4 != 1.0F) {
                Matrix.scaleM(var1, 0, var4, 1.0F, 1.0F);
            }
        } else if (this.z == ViewType.LEVELED) {
            this.c(var1, var2, var3);
        }

    }

    private void b(float[] var1, float var2, float var3) {
        var3 = 1.0F / var3;
        var2 = -var2;
        this.c(var1, var2, var3);
    }

    private void c(float[] var1, float var2, float var3) {
        float var4 = !this.m && !this.B ? -1.0F : 1.0F;
        Matrix.setIdentityM(this.a, 0);
        Matrix.scaleM(this.a, 0, var4 * var3, var3, 1.0F);
        Matrix.setRotateM(this.b, 0, var4 * (float) Math.toDegrees((double) var2), 0.0F, 0.0F, 1.0F);
        Matrix.multiplyMM(var1, 0, this.b, 0, this.a, 0);
    }

    public void setCameraToScreenAngle(int degrees) {
        float var2 = -((float) degrees);
        if (this.S != var2) {
            this.S = var2;
            if (degrees != 90 && degrees != 270) {
                this.T = false;
            } else {
                this.T = true;
            }

            this.a();
        }
    }

    public void setViewType(ViewType viewType) {
        if (this.z != viewType) {
            this.z = viewType;
            this.a();
        }
    }

    public void setHUDVisible(boolean visible) {
        this.A = visible;
    }

    public void setFillMode(FillMode fillMode) {
        boolean var2 = fillMode == FillMode.ASPECT_FILL;
        if (this.x != var2) {
            this.x = var2;
            if (this.q != this.j) {
                this.w.startInterpolating();
            }
        }
    }

    public void setTintColor(int color) {
        this.Q = new float[]{(float) Color.red(color) / 255.0F, (float) Color.green(color) / 255.0F, (float) Color.blue(color) / 255.0F, 1.0F};
    }

    public void setMirroredFrontFace(boolean mirrored) {
        this.B = mirrored;
    }

    public void setWatermarkEnabled(boolean enabled) {
        this.C = enabled;
    }

    public void handleTouchEvent(MotionEvent ev) {
        this.M.handleTouchEvent(ev);
    }

    public float getVideoScale() {
        return this.D;
    }

    public float[] getOutputFramePlacement() {
        float var1 = this.n * (float) this.i;
        float var2 = this.o * (float) this.i;
        float var3;
        float var4;
        float var5;
        if (this.z == ViewType.NORMAL) {
            var3 = 1.0F / this.F * this.D;
            var5 = !this.m && !this.B ? -1.0F : 1.0F;
            var4 = var5 * (-((float) Math.toDegrees((double) this.E)) + this.S);
        } else {
            var3 = this.D;
            var4 = 0.0F;
        }

        var5 = var1 * var3;
        float var6 = var2 * var3;
        float[] var7 = new float[]{var5, var6, -var4};
        return var7;
    }

    public boolean mapTouchToCamera(Point touchPoint, Point mappedPoint) {
        float[] var3 = new float[]{(float) touchPoint.x, (float) touchPoint.y, 0.0F, 1.0F};
        float[] var4 = new float[16];
        float[] var5 = new float[16];
        Matrix.setIdentityM(var5, 0);
        Matrix.scaleM(var5, 0, 2.0F / (float) this.h, -2.0F / (float) this.i, 1.0F);
        float[] var6 = new float[16];
        Matrix.setIdentityM(var6, 0);
        Matrix.translateM(var6, 0, -1.0F, 1.0F, 0.0F);
        Matrix.multiplyMM(var4, 0, var6, 0, var5, 0);
        float[] var7 = new float[16];
        boolean var8 = Matrix.invertM(var7, 0, this.J, 0);
        if (!var8) {
            return false;
        } else {
            float[] var9 = new float[16];
            Matrix.multiplyMM(var9, 0, var7, 0, var4, 0);
            float[] var10 = new float[4];
            Matrix.multiplyMV(var10, 0, var9, 0, var3, 0);
            if (var10[0] >= -this.n && var10[0] <= this.n && var10[1] >= -this.o && var10[1] <= this.o) {
                float var11 = this.m ? 1.0F : -1.0F;
                float[] var12 = new float[16];
                Matrix.setIdentityM(var12, 0);
                Matrix.scaleM(var12, 0, var11 * 1000.0F / this.n, -1000.0F / this.o, 1.0F);
                float[] var13 = new float[4];
                Matrix.multiplyMV(var13, 0, var12, 0, var10, 0);
                mappedPoint.x = (int) var13[0];
                mappedPoint.y = (int) var13[1];
                return true;
            } else {
                return false;
            }
        }
    }
}

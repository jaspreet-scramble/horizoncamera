package com.mla.wwl.hEngine;

import android.view.animation.AccelerateDecelerateInterpolator;
import com.mla.wwl.WWLVars.WWLLevelerCropMode;

public class SnapFilter {
  private ValueInterpolator a;
  private float b;
  private long c;
  private boolean d = false;
  private long e = 0L;
  private LowPassFilter f;

  public SnapFilter(float yawRange, float minDuration, float interpolationDuration) {
    this.b = yawRange;
    this.c = (long)minDuration * 1000000L;
    this.a = new ValueInterpolator(new AccelerateDecelerateInterpolator(), interpolationDuration);
    this.a.setAreAngleData(true);
    this.f = new LowPassFilter(1.5D);
    this.f.setAreAngleData(true);
  }

  public boolean isSnapped() {
    return this.d;
  }

  private float a(float var1) {
    float var2 = -1.0F;
    var1 = MathUtils.normalizedAngle2(var1);
    float var3 = this.b;
    float var4 = this.b * 1.3F;
    if(var1 < var3 && var1 > -var3) {
      var2 = 0.0F;
    } else if(var1 < MathUtils.M_PI_2 + var4 && var1 > MathUtils.M_PI_2 - var4) {
      var2 = MathUtils.M_PI_2;
    } else if(var1 >= -MathUtils.M_PI + var3 && var1 <= MathUtils.M_PI - var3) {
      if(var1 > -MathUtils.M_PI_2 - var4 && var1 < -MathUtils.M_PI_2 + var4) {
        var2 = -MathUtils.M_PI_2;
      }
    } else {
      var2 = -MathUtils.M_PI;
    }

    return var2;
  }

  private boolean a(long var1, float var3) {
    boolean var4 = false;
    if(var1 != 0L && var3 != -1.0F) {
      if(this.e == 0L) {
        this.e = var1;
      }

      float var5 = (float)(var1 - this.e);
      if(var5 > (float)this.c) {
        var4 = true;
      }

      return var4;
    } else {
      this.e = 0L;
      return false;
    }
  }

  public float resultForInput(long timestamp, float yaw, float pitch, WWLLevelerCropMode cropMode) {
    float var6 = 0.0F;
    float var7 = this.a(yaw);
    boolean var8 = this.a(timestamp, var7);
    if(cropMode == WWLLevelerCropMode.FLEX && var8) {
      if(!this.d) {
        this.d = true;
        this.f.reset();
      }

      float var9 = (float)this.f.filterInput(timestamp, (double)yaw);
      var6 = this.a.getValueForInput(var9, timestamp);
    } else {
      if(this.d) {
        this.d = false;
        this.a.startInterpolating();
      }

      var6 = this.a.getValueForInput(yaw, timestamp);
    }

    return var6;
  }
}

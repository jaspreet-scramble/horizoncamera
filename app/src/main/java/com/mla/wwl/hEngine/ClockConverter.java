package com.mla.wwl.hEngine;

import android.os.SystemClock;
import android.util.Log;

public class ClockConverter {
  int a;
  long b;
  long c;
  long d = 0L;
  int e;
  int f;

  public ClockConverter() {
    this.reset();
  }

  public void reset() {
    this.a = 0;
    this.e = 0;
    this.f = 0;
    this.b = System.currentTimeMillis() * 1000000L - System.nanoTime();
    this.c = System.currentTimeMillis() * 1000000L - SystemClock.elapsedRealtimeNanos();
    this.d = 0L;
  }

  private int a(long var1) {
    long var3 = 39420000000000000L;
    long var5 = 400000000L;
    long var7 = 50000000L;
    long var9 = 20000000L;
    boolean var11 = false;
    byte var24;
    if(var1 > var3) {
      var24 = 1;
    } else {
      long var12 = System.currentTimeMillis() * 1000000L;
      long var14 = this.b + var1;
      long var16 = Math.abs(var14 - var12);
      long var18 = this.c + var1;
      long var20 = Math.abs(var18 - var12);
      if(var16 >= var5 && var20 >= var5) {
        var24 = 4;
        this.d = var12 - var1;
        long var22 = var1 + this.d;
        if(Math.abs(var22 - var12) > var7) {
          this.d = var12 - var1;
        }
      } else if(Math.abs(this.c - this.b) < var9) {
        var24 = 2;
      } else if(var16 < var20) {
        var24 = 2;
      } else {
        var24 = 3;
      }
    }

    return var24;
  }

  private void a(int var1) {
    if(var1 == 4) {
      Log.w("ClockConverter", "Unknown clock type selected");
    }

    this.a = var1;
  }

  private boolean b(int var1) {
    if(var1 == 2 && this.a == 3 || var1 == 3 && this.a == 2) {
      long var2 = 30000000L;
      if(Math.abs(this.c - this.b) < var2) {
        return false;
      }
    }

    return true;
  }

  public long convertToUnixTimeNano(long timestamp) {
    if(this.a == 0 || this.e == 0) {
      int var3 = this.a(timestamp);
      if(this.a == 0) {
        this.a(var3);
      } else if(var3 != this.a) {
        ++this.f;
        if(this.f == 5) {
          this.f = 0;
          if(this.b(var3)) {
            Log.w("ClockConverter", "Clock type changed from " + this.a + " to " + var3);
            this.a(var3);
          }
        }
      } else {
        this.f = 0;
      }
    }

    this.e = (this.e + 1) % 5;
    long var7;
    if(this.a == 1) {
      var7 = 0L;
    } else if(this.a == 2) {
      var7 = this.b;
    } else if(this.a == 3) {
      var7 = this.c;
    } else {
      var7 = this.d;
    }

    long var5 = timestamp + var7;
    return var5;
  }

  public long convertUnixTimeToNanoTime(long unixTimestamp) {
    long var3 = unixTimestamp - this.b;
    return var3;
  }
}
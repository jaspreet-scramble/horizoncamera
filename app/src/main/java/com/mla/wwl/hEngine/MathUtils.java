package com.mla.wwl.hEngine;


public class MathUtils {
  public static float M_PI = 3.1415927F;
  public static float M_2PI = 6.2831855F;
  public static float M_PI_2 = 1.5707964F;
  public static float M_PI_4 = 0.7853982F;

  public MathUtils() {
  }

  public static float normalizedAngle(float angle) {
    int var1 = (int)(angle / (2.0F * M_PI));
    angle -= (float)var1 * 2.0F * M_PI;
    if(angle < 0.0F) {
      angle += 2.0F * M_PI;
    }

    return angle;
  }

  public static float normalizedAngle2(float angle) {
    int var1 = (int)(angle / M_2PI);
    angle -= (float)var1 * M_2PI;
    if(angle < -M_PI) {
      angle += M_2PI;
    } else if(angle >= M_PI) {
      angle -= M_2PI;
    }

    return angle;
  }

  public static double truncate(double d) {
    return d > 0.0D?Math.floor(d):Math.ceil(d);
  }

  public static double correctAngleToPreviousAngle(double previousAngle, double newAngle) {
    double var4 = newAngle - previousAngle;
    int var6 = (int)truncate(var4 / 6.283185307179586D);
    double var7 = var4 - (double)var6 * 2.0D * 3.141592653589793D;
    if(var7 < -3.141592653589793D) {
      newAngle += 6.283185307179586D;
    } else if(var7 > 3.141592653589793D) {
      newAngle -= 6.283185307179586D;
    }

    newAngle -= (double)var6 * 2.0D * 3.141592653589793D;
    return newAngle;
  }

  public static float correctAngleToPreviousAngle(float previousAngle, float newAngle) {
    float var2 = newAngle - previousAngle;
    float var3 = normalizedAngle2(var2);
    newAngle = previousAngle + var3;
    return newAngle;
  }

  public static float quantizeAngle(float angle) {
    angle = normalizedAngle(angle);
    float var1 = 0.0F;
    int var2 = (int)(angle / M_PI_4);
    if(var2 != 0 && var2 != 7) {
      if(var2 != 1 && var2 != 2) {
        if(var2 != 3 && var2 != 4) {
          if(var2 == 5 || var2 == 6) {
            var1 = 3.0F * M_PI_2;
          }
        } else {
          var1 = M_PI;
        }
      } else {
        var1 = M_PI_2;
      }
    } else {
      var1 = 0.0F;
    }

    return var1;
  }

  public static int orientationFromAngle(float angle) {
    float var1 = quantizeAngle(angle);
    return var1 != 0.0F && var1 != M_PI?1:2;
  }

  public static int roundUp(int numToRound, int multiple) {
    if(multiple == 0) {
      return numToRound;
    } else {
      int var2 = numToRound % multiple;
      return var2 == 0?numToRound:numToRound + (multiple - var2);
    }
  }

  public static float calculateYLinear(float x, float x1, float y1, float x2, float y2) {
    float var5 = (y2 - y1) / (x2 - x1);
    float var6 = (x - x1) * var5 + y1;
    return var6;
  }

  public static float clipToRange(float x, float min, float max) {
    if(x < min) {
      x = min;
    } else if(x > max) {
      x = max;
    }

    return x;
  }
}

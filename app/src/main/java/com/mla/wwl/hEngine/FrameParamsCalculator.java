package com.mla.wwl.hEngine;

import com.mla.wwl.WWLVars.WWLLevelerCropMode;
import com.mla.wwl.WWLVars.WWLLevelerLockedOrientation;

import static com.mla.wwl.hEngine.FrameParamsCalculator.a.LEFT_RIGHT;
import static com.mla.wwl.hEngine.FrameParamsCalculator.a.TOP_BOTTOM;


public class FrameParamsCalculator {
    private GimbalFilter g = new GimbalFilter(0.9F, 600.0F);
    private SnapFilter h = new SnapFilter((float) Math.toRadians(8.0D), 500.0F, 700.0F);
    private LockedYawFilter i = new LockedYawFilter(500.0F, 600.0F);
    private ScaleFilter j;
    private CropModeInterpolator k = new CropModeInterpolator();
    float a;
    float b;
    float c;
    float d;
    float e;
    float f;

    public FrameParamsCalculator(float scaleFilterFrequency) {
        this.j = new ScaleFilter(scaleFilterFrequency);
    }

    private static FrameParamsCalculator.a a(float var0, float var1, float var2, float var3) {
        boolean var4 = var1 <= var2;
        if ((double) var3 > 0.0D && Math.abs(var0 - MathUtils.M_PI_2) < MathUtils.M_PI_2 - var3) {
            var4 = !var4;
        }

        return var4 ? TOP_BOTTOM : LEFT_RIGHT;
    }

    private static float a(float var0, float var1, float var2, float var3, float var4, float var5) {
        var0 = Math.abs(var0 % MathUtils.M_PI);
        FrameParamsCalculator.a var7 = a(var0, var1, var2, var3);
        float var6;
        if (var7 == TOP_BOTTOM) {
            if (Math.abs(var0 - MathUtils.M_PI_2) > MathUtils.M_PI_2 - var4) {
                var6 = (float) (1.0D / Math.abs(Math.cos((double) var0)) + Math.abs(Math.sin((double) var0)) * ((double) var1 - Math.abs(Math.tan((double) var0))));
            } else {
                var6 = (float) ((double) var1 * 1.0D / Math.abs(Math.sin((double) var0)) + Math.abs(Math.cos((double) var0)) * (1.0D - Math.abs(Math.tan((double) (var0 - MathUtils.M_PI_2))) * (double) var1));
            }
        } else if (Math.abs(var0 - MathUtils.M_PI_2) > MathUtils.M_PI_2 - var5) {
            var6 = (float) ((double) (var1 / var2) * (1.0D / Math.abs(Math.cos((double) var0)) + Math.abs(Math.sin((double) var0)) * (1.0D / (double) var1 - Math.abs(Math.tan((double) var0)))));
        } else {
            var6 = (float) (1.0D / (double) var2 * (1.0D / Math.abs(Math.sin((double) var0)) + Math.abs(Math.cos((double) var0)) * ((double) var1 - Math.abs(Math.tan((double) (var0 - MathUtils.M_PI_2))))));
        }

        return var6;
    }

    private static float a(float var0, float var1) {
        float var2;
        if ((double) var1 > 1.0D) {
            var2 = (float) Math.sqrt((double) (var0 * var0) + 1.0D);
        } else {
            var2 = (float) ((double) (1.0F / var1) * Math.sqrt((double) (var0 * var0 + 1.0F)));
        }

        return var2;
    }

    private void b(float var1, float var2) {
        this.a = var1;
        this.b = var2;
        this.c = (float) Math.atan((double) ((var1 - var2) / (var1 * var2 - 1.0F)));
        this.d = (float) Math.atan((double) var1);
        this.e = (float) Math.atan(1.0D / (double) var1);
        this.f = a(var1, var2);
    }

    public float[] getFrameParamsForData(long timestamp, float yaw, float pitch, float outputAspectRatio, float sourceAspectRatio, WWLLevelerCropMode cropMode, WWLLevelerLockedOrientation lockedOrientation, boolean enanbleSnapFilter, boolean enableScaleFilter, boolean isRecording) {
        if (outputAspectRatio != this.a || sourceAspectRatio != this.b) {
            this.b(outputAspectRatio, sourceAspectRatio);
        }

        yaw = this.g.resultForInput(timestamp, yaw, pitch);
        if (enanbleSnapFilter) {
            yaw = this.h.resultForInput(timestamp, yaw, pitch, cropMode);
        }

        boolean var12 = this.k.setCropMode(cropMode);
        float var13 = this.i.resultForInput(timestamp, yaw, lockedOrientation, var12, isRecording);
        yaw = this.k.yawForInput(timestamp, yaw, yaw, var13);
        float var14 = a(yaw, outputAspectRatio, sourceAspectRatio, this.c, this.d, this.e);
        float var15 = var14;
        float var16 = this.f;
        if (enableScaleFilter) {
            boolean var18 = enanbleSnapFilter ? this.h.isSnapped() : false;
            var15 = this.j.resultForInput(timestamp, yaw, var14, var18);
        }

        float var19 = this.k.scaleForInput(timestamp, var15, var16, var14, var14);
        return new float[]{yaw, var19};
    }

    public float[] getPhotoFrameParamsForData(long timestamp, float yaw, float pitch, float outputAspectRatio, float sourceAspectRatio, WWLLevelerCropMode cropMode, WWLLevelerLockedOrientation lockedMode, boolean enanbleSnapFilter, boolean enableScaleFilter, boolean isRecording) {
        if (outputAspectRatio != this.a || sourceAspectRatio != this.b) {
            this.b(outputAspectRatio, sourceAspectRatio);
        }

        yaw = this.g.resultForInput(timestamp, yaw, pitch);
        if (enanbleSnapFilter) {
            yaw = this.h.resultForInput(timestamp, yaw, pitch, cropMode);
        }

        boolean var12 = false;
        float var13 = this.i.resultForInput(timestamp, yaw, lockedMode, var12, isRecording);
        yaw = this.k.yawForInput(timestamp, yaw, yaw, var13);
        float var14 = a(yaw, outputAspectRatio, sourceAspectRatio, this.c, this.d, this.e);
        float var16 = this.f;
        float var18 = this.k.photoScaleForInput(timestamp, var14, var16, var14);
        return new float[]{yaw, var18};
    }

    protected enum a {
        TOP_BOTTOM,
        LEFT_RIGHT
    }
}
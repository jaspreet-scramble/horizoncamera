package com.mla.wwl.hEngine;

import android.view.animation.AccelerateDecelerateInterpolator;

public class GimbalFilter {
  private float a;
  private float b;
  private float c = 0.05F;
  private float d;
  private boolean e = false;
  private ValueInterpolator f;

  public GimbalFilter(float pitchThreshold, float interpolationDuration) {
    this.a = pitchThreshold;
    this.b = this.a;
    this.f = new ValueInterpolator(new AccelerateDecelerateInterpolator(), interpolationDuration);
    this.f.setAreAngleData(true);
  }

  public float resultForInput(long timestamp, float yaw, float pitch) {
    float var5 = 0.0F;
    if(Math.abs(pitch) > this.b) {
      if(!this.e) {
        this.e = true;
        this.d = yaw;
        this.b = this.a - this.c;
      }

      var5 = this.f.getValueForInput(this.d, timestamp);
    } else {
      if(this.e) {
        this.e = false;
        this.f.startInterpolating();
        this.b = this.a;
      }

      var5 = this.f.getValueForInput(yaw, timestamp);
    }

    return var5;
  }
}

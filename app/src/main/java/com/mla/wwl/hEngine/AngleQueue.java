package com.mla.wwl.hEngine;

import android.util.Log;
import java.util.ListIterator;

public class AngleQueue {
  private static int a = 100;
  private BoundedLinkedList<AcceleSensorEvent> b;
  private LowPassFilter c;
  private LowPassFilter d;
  private LowPassFilter e;
  private CalibrationData f;

  public AngleQueue(CalibrationData calibrationData) {
    this.b = new BoundedLinkedList(a);
    this.c = new LowPassFilter(5.0D);
    this.d = new LowPassFilter(5.0D);
    this.e = new LowPassFilter(5.0D);
    this.f = calibrationData;
  }

  private void a() {
    this.c.reset();
    this.d.reset();
    this.e.reset();
    this.b.clear();
  }

  public void test() {
    for(int var1 = 0; var1 < 2000; var1 += 10) {
      AcceleSensorEvent var2 = new AcceleSensorEvent();
      var2.timestamp = (long)var1;
      this.enqueueEvent(var2);
    }

    this.printQueue();
    long var3 = System.currentTimeMillis();
    System.out.printf("timestamp found: %d%n", new Object[]{Long.valueOf(((AcceleSensorEvent)this.a(1026L).next()).timestamp)});
    System.out.printf("aaa %f%n", new Object[]{Float.valueOf((float)(System.currentTimeMillis() - var3) / 1000.0F)});
  }

  public void printQueue() {
    ListIterator var1 = this.b.listIterator();

    while(var1.hasNext()) {
      System.out.printf("lala %d%n", new Object[]{Long.valueOf(((AcceleSensorEvent)var1.next()).timestamp)});
    }

  }

  private float a(float[] var1) {
    float var2 = 0.0F;
    float var3 = var1[0];
    float var4 = var1[1];
    var2 = -((float)Math.atan2((double)var4, (double)var3));
    return var2;
  }

  private void a(AcceleSensorEvent var1) {
    this.f.transformSensorData(var1.values);
    if(var1.sensorType == 1) {
      var1.values[0] = (float)this.c.filterInput(var1.timestamp, (double)var1.values[0]);
      var1.values[1] = (float)this.d.filterInput(var1.timestamp, (double)var1.values[1]);
      var1.values[2] = (float)this.e.filterInput(var1.timestamp, (double)var1.values[2]);
    }

  }

  private ListIterator<AcceleSensorEvent> a(long var1) {
    ListIterator var3 = null;
    if(this.b.size() == 0) {
      return var3;
    } else {
      boolean var4 = false;
      ListIterator var5 = this.b.listIterator(this.b.size());

      while(var5.hasPrevious()) {
        AcceleSensorEvent var6 = (AcceleSensorEvent)var5.previous();
        long var7 = var1 - var6.timestamp;
        if(var7 > 0L) {
          var4 = true;
          if(var5.nextIndex() + 1 == this.b.size()) {
            var3 = var5;
          } else {
            var5.next();
            AcceleSensorEvent var9 = (AcceleSensorEvent)var5.next();
            var5.previous();
            long var10 = var9.timestamp - var1;
            if(var7 < var10) {
              var5.previous();
              var3 = var5;
            } else {
              var3 = var5;
            }
          }
          break;
        }
      }

      if(!var4) {
        var3 = var5;
      }

      return var3;
    }
  }

  private float a(ListIterator<AcceleSensorEvent> var1, long var2) {
    float var4 = 0.0F;
    AcceleSensorEvent var5 = null;
    AcceleSensorEvent var6 = null;
    AcceleSensorEvent var7 = null;
    var7 = (AcceleSensorEvent)var1.next();
    if(var2 > var7.timestamp) {
      if(var1.hasNext()) {
        var5 = var7;
        var6 = (AcceleSensorEvent)var1.next();
      }
    } else {
      var1.previous();
      if(var1.hasPrevious()) {
        var5 = (AcceleSensorEvent)var1.previous();
        var6 = var7;
      }
    }

    if(var5 != null && var6 != null) {
      float var8 = (float)(var6.timestamp - var5.timestamp);
      float var9 = (float)(var2 - var5.timestamp) / var8;
      float var10 = this.a(var5.values);
      float var11 = this.a(var6.values);
      var11 = MathUtils.correctAngleToPreviousAngle(var10, var11);
      float var12 = var11 - var10;
      var4 = var10 + var9 * var12;
    } else {
      var4 = this.a(var7.values);
    }

    return var4;
  }

  public void enqueueEvent(AcceleSensorEvent newEvent) {
    this.a(newEvent);
    if(this.b.size() == 0) {
      this.b.add(newEvent);
    } else {
      boolean var2 = false;
      ListIterator var3 = this.b.listIterator(this.b.size());

      while(var3.hasPrevious()) {
        AcceleSensorEvent var4 = (AcceleSensorEvent)var3.previous();
        if(newEvent.timestamp > var4.timestamp) {
          if(var3.nextIndex() + 1 == this.b.size()) {
            this.b.addLast(newEvent);
          } else {
            this.b.add(var3.nextIndex() + 1, newEvent);
          }

          var2 = true;
          break;
        }
      }

      if(!var2) {
        this.b.addFirst(newEvent);
      }
    }

  }

  public float[] anglesForTimestamp(long inputTimestamp) {
    float[] var3 = new float[2];
    if(inputTimestamp == 0L) {
      return new float[]{0.0F, 0.0F};
    } else {
      long var4 = 0L;
      inputTimestamp += var4;
      ListIterator var6 = this.a(inputTimestamp);
      if(var6 == null) {
        return new float[]{0.0F, 0.0F};
      } else {
        AcceleSensorEvent var7 = (AcceleSensorEvent)var6.next();
        var6.previous();
        var3[0] = this.a(var6, inputTimestamp);
        var3[1] = var7.values[2] / 9.80665F;
        return var3;
      }
    }
  }

  public void setCameraToSensorAngle(float sensorAngle) {
    float var2 = this.f.getCameraToSensorAngle();
    if(var2 != sensorAngle) {
      Log.i("AngleQueue", "Updating sensor angle from " + var2 + " to " + sensorAngle + ".");
      this.a();
      this.f.setCameraToSensorAngle(sensorAngle);
    }
  }
}


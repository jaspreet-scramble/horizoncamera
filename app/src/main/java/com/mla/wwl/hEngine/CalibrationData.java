package com.mla.wwl.hEngine;

import com.mla.wwl.Utils.FileUtils;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class CalibrationData {
  private float[][] a;
  public float beta_X;
  public float alpha_X;
  public float beta_Y;
  public float alpha_Y;
  private float b;
  private float c;
  private float[][] d;
  private float e;
  private float[][] f;

  public CalibrationData(float[][] measurements) {
    if(areMeasurementsValid(measurements)) {
      this.a = this.a(measurements);
      this.a();
    } else {
      this.a = (float[][])null;
      this.d = (float[][])null;
      this.beta_X = 0.0F;
      this.beta_Y = 0.0F;
    }

    this.setCameraToSensorAngle(MathUtils.M_PI_2);
  }

  public CalibrationData(File file) {
    this(a(file));
  }

  private float[][] a(float[][] var1) {
    if(var1 == null) {
      return (float[][])null;
    } else {
      float[][] var2 = new float[var1.length][];

      for(int var3 = 0; var3 < var1.length; ++var3) {
        var2[var3] = (float[])var1[var3].clone();
      }

      return var2;
    }
  }

  public static boolean areMeasurementsValid(float[][] input) {
    boolean var1 = false;
    if(input != null && input.length == 4) {
      CalibrationData.SensorPosition[] var2 = CalibrationData.SensorPosition.values();
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
        CalibrationData.SensorPosition var5 = var2[var4];
        float[] var6 = input[var5.index];
        if(!isMeasurementValid(var6, var5)) {
          var1 = false;
          break;
        }

        var1 = true;
      }
    }

    return var1;
  }

  public static boolean isMeasurementValid(float[] input, CalibrationData.SensorPosition position) {
    boolean var2 = false;
    if(input.length == 3 && Math.abs(input[2]) < 0.980665F) {
      switch(position.ordinal()) {
        case 1:
          if(input[1] > 6.864655F) {
            var2 = true;
          }
          break;
        case 2:
          if(input[0] > 6.864655F) {
            var2 = true;
          }
          break;
        case 3:
          if(-input[1] > 6.864655F) {
            var2 = true;
          }
          break;
        case 4:
          if(-input[0] > 6.864655F) {
            var2 = true;
          }
      }
    }

    return var2;
  }

  private void a() {
    this.beta_X = (this.a[CalibrationData.SensorPosition.Y_POS.index][0] + this.a[CalibrationData.SensorPosition.Y_NEG.index][0]) / 2.0F;
    float var1 = 0.5F * (float)Math.sqrt(Math.pow((double)(this.a[CalibrationData.SensorPosition.Y_POS.index][0] - this.a[CalibrationData.SensorPosition.Y_NEG.index][0]), 2.0D) + Math.pow((double)(this.a[CalibrationData.SensorPosition.X_POS.index][0] - this.a[CalibrationData.SensorPosition.X_NEG.index][0]), 2.0D));
    this.b = (float)Math.asin((double)(this.a[CalibrationData.SensorPosition.Y_POS.index][0] - this.beta_X) / (double)(var1 * Math.signum(this.a[CalibrationData.SensorPosition.X_POS.index][0])));
    this.alpha_X = var1 / 9.80665F;
    this.beta_Y = (this.a[CalibrationData.SensorPosition.X_POS.index][1] + this.a[CalibrationData.SensorPosition.X_NEG.index][1]) / 2.0F;
    float var2 = 0.5F * (float)Math.sqrt(Math.pow((double)(this.a[CalibrationData.SensorPosition.X_POS.index][1] - this.a[CalibrationData.SensorPosition.X_NEG.index][1]), 2.0D) + Math.pow((double)(this.a[CalibrationData.SensorPosition.Y_POS.index][1] - this.a[CalibrationData.SensorPosition.Y_NEG.index][1]), 2.0D));
    this.c = (float)Math.asin((double)(this.a[CalibrationData.SensorPosition.X_NEG.index][1] - this.beta_Y) / (double)(var2 * Math.signum(this.a[CalibrationData.SensorPosition.Y_POS.index][1])));
    this.alpha_Y = var2 / 9.80665F;
    if(this.beta_X != 0.0F / 0.0 && this.alpha_X != 0.0F / 0.0 && this.b != 0.0F / 0.0 && this.beta_Y != 0.0F / 0.0 && this.alpha_Y != 0.0F / 0.0 && this.c != 0.0F / 0.0) {
      float[][] var3 = new float[][]{{(float)Math.cos((double)this.b), (float)Math.sin((double)this.b)}, {(float)(-Math.sin((double)this.c)), (float)Math.cos((double)this.c)}};
      float var4 = var3[0][0];
      float var5 = var3[1][0];
      float var6 = var3[0][1];
      float var7 = var3[1][1];
      float var8 = 1.0F / (var4 * var7 - var5 * var6);
      if(var8 != 0.0F) {
        this.d = new float[][]{{var8 * var7, var8 * -var6}, {var8 * -var5, var8 * var4}};
      }

    } else {
      this.d = (float[][])null;
    }
  }

  public void setCameraToSensorAngle(float sensorAngle) {
    this.e = sensorAngle;
    float var2 = sensorAngle - MathUtils.M_PI_2;
    this.f = new float[][]{{(float)Math.cos((double)var2), (float)(-Math.sin((double)var2))}, {(float)Math.sin((double)var2), (float)Math.cos((double)var2)}};
  }

  public float getCameraToSensorAngle() {
    return this.e;
  }

  public void transformSensorData(float[] data) {
    if(this.d != null) {
      this.a(data);
    }

    this.b(data);
  }

  private void a(float[] var1) {
    float var2 = var1[0];
    float var3 = var1[1];
    var2 = (var2 - this.beta_X) / this.alpha_X;
    var3 = (var3 - this.beta_Y) / this.alpha_Y;
    var1[0] = this.d[0][0] * var2 + this.d[0][1] * var3;
    var1[1] = this.d[1][0] * var2 + this.d[1][1] * var3;
  }

  private void b(float[] var1) {
    float var2 = var1[0];
    float var3 = var1[1];
    var1[0] = this.f[0][0] * var2 + this.f[0][1] * var3;
    var1[1] = this.f[1][0] * var2 + this.f[1][1] * var3;
  }

  public static void serializeData(float[][] data, int sensorType, File file) {
    HashMap var3 = new HashMap();
    var3.put("data", data);
    var3.put("sensor", Integer.valueOf(sensorType));
    FileUtils.saveObjectToFile(var3, file);
  }

  private static float[][] a(File var0) {
    if(var0 != null && var0.exists()) {
      Map var1 = (Map)FileUtils.loadObjectFromFile(var0);
      if(var1 == null) {
        throw new RuntimeException("Calibration file could not be loaded");
      } else {
        float[][] var2 = (float[][])((float[][])var1.get("data"));
        return var2;
      }
    } else {
      return (float[][])null;
    }
  }

  public static enum SensorPosition {
    Y_POS(0, 90),
    X_POS(1, 0),
    Y_NEG(2, 270),
    X_NEG(3, 180);

    public final int index;
    public final int degrees;

    private SensorPosition(int index, int degrees) {
      this.index = index;
      this.degrees = degrees;
    }

    public static CalibrationData.SensorPosition positionForDegrees(int degrees) {
      CalibrationData.SensorPosition var1;
      switch(degrees) {
        case 0:
          var1 = X_POS;
          break;
        case 90:
          var1 = Y_POS;
          break;
        case 180:
          var1 = X_NEG;
          break;
        case 270:
          var1 = Y_NEG;
          break;
        default:
          var1 = Y_POS;
      }

      return var1;
    }
  }
}

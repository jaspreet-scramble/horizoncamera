package com.mla.wwl.hEngine;



public class ScaleFilter {
  private float b;
  private float c;
  private LowPassFilter d;
  private LowPassFilter e;
  private LowPassFilter f;
  static long a = 0L;

  public ScaleFilter(float frequency) {
    this.b = frequency;
    this.d = new LowPassFilter(this.b, false);
    this.e = new LowPassFilter(this.b / 1.5F, true);
    this.f = new LowPassFilter(4.5F, false);
    this.c = 0.1F;
  }

  private float a(long var1, float var3, float var4) {
    float var5 = 0.0F;
    float var6 = var3 - var4;
    if(var6 > this.c) {
      var5 = var6;
    } else {
      var5 = this.c;
    }

    float var7 = (float)this.f.filterInput(var1, (double)var5, (double)var5);
    return var7;
  }

  public float resultForInput(long timestamp, float yaw, float inputScale, boolean isSnapped) {
    float var6 = 1.0F;
    float var7 = (float)this.d.filterInput(timestamp, (double)inputScale);
    float var8 = this.a(timestamp, inputScale, var7);
    if(isSnapped) {
      var8 = 0.0F;
    }

    var6 = (float)this.e.filterInput(timestamp, (double)(inputScale + var8), (double)inputScale);
    if(var6 < inputScale) {
      var6 = inputScale;
    }

    return var6;
  }
}

package com.mla.wwl.hEngine;

import android.view.animation.AccelerateDecelerateInterpolator;
import com.mla.wwl.WWLVars.WWLLevelerCropMode;

public class CropModeInterpolator {
  private WWLLevelerCropMode a = null;
  private ValueInterpolator b = new ValueInterpolator(new AccelerateDecelerateInterpolator(), 800.0F);
  private ValueInterpolator c;

  public CropModeInterpolator() {
    this.b.setAreAngleData(true);
    this.c = new ValueInterpolator(new AccelerateDecelerateInterpolator(), 700.0F);
  }

  public boolean setCropMode(WWLLevelerCropMode cropMode) {
    boolean var2 = false;
    if(this.a != null && this.a != cropMode) {
      var2 = true;
      if(this.a == WWLLevelerCropMode.LOCKED || cropMode == WWLLevelerCropMode.LOCKED) {
        this.b.startInterpolating();
      }

      this.c.startInterpolating();
    }

    this.a = cropMode;
    return var2;
  }

  public float yawForInput(long timestamp, float yawFlex, float yawRotate, float yawLocked) {
    float var6 = 0.0F;
    switch(this.a.ordinal()) {
      case 1:
      case 2:
        var6 = yawFlex;
        break;
      case 3:
        var6 = yawLocked;
    }

    var6 = this.b.getValueForInput(var6, timestamp);
    return var6;
  }

  public float scaleForInput(long timestamp, float scaleFlex, float scaleRotate, float scaleLocked, float scaleFit) {
    float var7 = 1.0F;
    switch(this.a.ordinal()) {
      case 1:
        var7 = scaleFlex;
        break;
      case 2:
        var7 = scaleRotate;
        break;
      case 3:
        var7 = scaleLocked;
    }

    var7 = this.c.getValueForInput(var7, timestamp);
    var7 = var7 > scaleFit?var7:scaleFit;
    return var7;
  }

  public float photoScaleForInput(long timestamp, float scaleFlex, float scaleRotate, float scaleLocked) {
    float var6 = 1.0F;
    switch(this.a.ordinal()) {
      case 1:
        var6 = scaleFlex;
        break;
      case 2:
        var6 = scaleRotate;
        break;
      case 3:
        var6 = scaleLocked;
    }

    return var6;
  }
}
package com.mla.wwl.hEngine;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class BoundedLinkedList<E> extends LinkedList<E> {
  private final int a;

  public BoundedLinkedList(int maxSize) {
    this.a = maxSize;
  }

  public boolean add(E object) {
    if(this.size() == this.a) {
      this.removeFirst();
    }

    return super.add(object);
  }

  public void add(int location, E object) {
    if(this.size() == this.a) {
      this.removeFirst();
    }

    super.add(location, object);
  }

  public boolean addAll(Collection<? extends E> collection) {
    int var2 = this.size() + collection.size();
    int var3 = var2 - this.a;
    if(var3 > 0) {
      this.removeRange(0, var3);
    }

    return super.addAll(collection);
  }

  public boolean addAll(int location, Collection<? extends E> collection) {
    throw new UnsupportedOperationException();
  }

  public void addFirst(E object) {
    if(this.size() != this.a) {
      super.addFirst(object);
    }

  }

  public void addLast(E object) {
    this.add(object);
  }

  public String toString() {
    StringBuilder var1 = new StringBuilder();
    Iterator var2 = this.iterator();

    while(var2.hasNext()) {
      Object var3 = var2.next();
      var1.append(var3.toString());
    }

    return var1.toString();
  }
}


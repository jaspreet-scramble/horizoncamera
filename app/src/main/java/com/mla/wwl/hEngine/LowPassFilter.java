package com.mla.wwl.hEngine;

public class LowPassFilter {
  private double a;
  private long b;
  private double c = 0.0D;
  private boolean d = true;
  private boolean e = false;
  private boolean f = false;

  public LowPassFilter(double frequency) {
    this.a = 1.0D / frequency;
  }

  public LowPassFilter(float w, boolean shouldThreshold) {
    this.a = (double)(1.0F / w);
    this.e = shouldThreshold;
  }

  public void reset() {
    this.d = true;
  }

  public void setAngularFrequency(float w) {
    this.a = (double)(1.0F / w);
  }

  public void setAreAngleData(boolean angleData) {
    this.f = true;
  }

  public double filterInput(long timestamp, double input) {
    if(this.d) {
      if(timestamp != 0L) {
        this.d = false;
        this.b = timestamp;
        this.c = input;
      }

      return input;
    } else {
      double var5 = (double)(timestamp - this.b) / 1.0E9D;
      double var7 = var5 / (var5 + this.a);
      if(this.f) {
        input = MathUtils.correctAngleToPreviousAngle(this.c, input);
      }

      this.c = var7 * input + (1.0D - var7) * this.c;
      this.b = timestamp;
      return this.c;
    }
  }

  public double filterInput(long timestamp, double input, double lowThreshold) {
    if(this.d) {
      if(timestamp != 0L) {
        this.d = false;
        this.b = timestamp;
        this.c = input;
      }

      return input;
    } else {
      double var7 = (double)(timestamp - this.b) / 1.0E9D;
      double var9 = var7 / (var7 + this.a);
      if(this.f) {
        input = MathUtils.correctAngleToPreviousAngle(this.c, input);
      }

      double var11 = var9 * input + (1.0D - var9) * this.c;
      if(this.e && var11 < lowThreshold) {
        var11 = lowThreshold;
      }

      this.c = var11;
      this.b = timestamp;
      return this.c;
    }
  }
}

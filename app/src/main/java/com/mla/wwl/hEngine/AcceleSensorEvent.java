package com.mla.wwl.hEngine;

import android.hardware.SensorEvent;

public class AcceleSensorEvent implements Cloneable {
  public long timestamp;
  public float[] values;
  public int sensorType;

  public AcceleSensorEvent(SensorEvent event) {
    this.timestamp = event.timestamp;
    this.values = (float[])event.values.clone();
    this.sensorType = event.sensor.getType();
  }

  public AcceleSensorEvent() {
  }

  public AcceleSensorEvent clone() {
    try {
      AcceleSensorEvent var1 = (AcceleSensorEvent)super.clone();
      var1.values = (float[])this.values.clone();
      return var1;
    } catch (CloneNotSupportedException var2) {
      var2.printStackTrace();
      return null;
    }
  }
}


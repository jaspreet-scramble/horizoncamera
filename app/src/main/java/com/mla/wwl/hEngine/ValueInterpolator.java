package com.mla.wwl.hEngine;

import android.view.animation.Interpolator;

public class ValueInterpolator {
  private float a;
  private Interpolator b;
  private boolean c;
  private boolean d = false;
  private long e = 0L;
  private float f = 0.0F;
  private float g = 0.0F;
  private long h = 0L;

  public ValueInterpolator(Interpolator interpolator, float duration) {
    this.b = interpolator;
    this.a = duration;
    this.c = false;
  }

  public void setDuration(float duration) {
    this.a = duration;
  }

  public void setAreAngleData(boolean angleData) {
    this.c = angleData;
  }

  public void startInterpolating() {
    this.d = true;
    this.e = this.h;
    this.f = this.g;
  }

  public void stopInterpolating() {
    this.d = false;
  }

  public boolean isInterpolating() {
    return this.d;
  }

  public float getValueForInput(float inputValue, long timestamp) {
    float var4;
    if(this.d) {
      float var5 = (float)(timestamp - this.e) / 1000000.0F;
      if(var5 >= this.a) {
        this.d = false;
        var4 = inputValue;
      } else {
        if(this.c) {
          inputValue = MathUtils.correctAngleToPreviousAngle(this.f, inputValue);
        }

        var4 = this.f + (inputValue - this.f) * this.b.getInterpolation(var5 / this.a);
      }
    } else {
      var4 = inputValue;
    }

    this.g = var4;
    this.h = timestamp;
    return var4;
  }
}

package com.mla.wwl.hEngine;

import android.view.animation.AccelerateDecelerateInterpolator;
import com.mla.wwl.WWLVars.WWLLevelerLockedOrientation;

public class LockedYawFilter {
  private float a = 0.0F;
  private float b = 0.0F;
  private long c = 0L;
  private float d;
  private boolean e = false;
  private boolean f = false;
  private ValueInterpolator g;

  public LockedYawFilter(float minDuration, float interpolationDuration) {
    this.b = 0.0F;
    this.d = 1000000.0F * minDuration;
    this.g = new ValueInterpolator(new AccelerateDecelerateInterpolator(), interpolationDuration);
    this.g.setAreAngleData(true);
  }

  private float a(WWLLevelerLockedOrientation var1, float var2) {
    var2 = MathUtils.normalizedAngle(var2);
    float var3 = 0.0F;
    int var4;
    switch(var1.ordinal()) {
      case 1:
        var4 = (int)(var2 / MathUtils.M_PI_2);
        if(var4 != 0 && var4 != 3) {
          var3 = -MathUtils.M_PI;
        } else {
          var3 = 0.0F;
        }
        break;
      case 2:
        var4 = (int)(var2 / MathUtils.M_PI_2);
        if(var4 != 0 && var4 != 1) {
          var3 = -MathUtils.M_PI_2;
        } else {
          var3 = MathUtils.M_PI_2;
        }
        break;
      case 3:
        var4 = (int)(var2 / MathUtils.M_PI_4);
        if(var4 != 0 && var4 != 7) {
          if(var4 != 1 && var4 != 2) {
            if(var4 != 3 && var4 != 4) {
              if(var4 == 5 || var4 == 6) {
                var3 = -MathUtils.M_PI_2;
              }
            } else {
              var3 = -MathUtils.M_PI;
            }
          } else {
            var3 = MathUtils.M_PI_2;
          }
        } else {
          var3 = 0.0F;
        }
    }

    return var3;
  }

  private boolean a(long var1, float var3) {
    boolean var4 = false;
    if(var1 != 0L && this.b == var3) {
      if(!this.e) {
        if(this.c == 0L) {
          this.c = var1;
        }

        float var5 = (float)(var1 - this.c);
        if(var5 > this.d) {
          var4 = true;
          this.e = true;
        }
      }

      return var4;
    } else {
      this.c = 0L;
      this.b = var3;
      this.e = false;
      return false;
    }
  }

  private void a(float var1) {
    this.b = var1;
    this.e = true;
  }

  public float resultForInput(long timestamp, float yaw, WWLLevelerLockedOrientation lockedOrientation, boolean didCropModeChange, boolean isRecording) {
    float var7 = this.a(lockedOrientation, yaw);
    if(!this.f) {
      this.f = true;
      this.a = var7;
      this.a(var7);
    }

    if(didCropModeChange) {
      this.a = var7;
      this.a(var7);
      this.g.stopInterpolating();
    } else if(!isRecording && this.a(timestamp, var7)) {
      this.g.startInterpolating();
      this.a = var7;
    }

    float var8 = this.g.getValueForInput(this.a, timestamp);
    return var8;
  }
}

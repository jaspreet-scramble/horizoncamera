package com.mla.wwl.state_machine;

public abstract class SimpleEventHandler<T> implements EventHandler<T> {
  public SimpleEventHandler() {
  }

  public StateEnum handleEvent(StateEnum fromState, StateEnum toState, T message) {
    return toState;
  }

  public void onFail(StateEnum fromState, StateEnum toState, T message) {
  }
}

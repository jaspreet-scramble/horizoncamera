package com.mla.wwl.state_machine;

public class Event<T> {
  String a;

  public Event(String eventName) {
    this.a = eventName;
  }

  public String name() {
    return this.a;
  }

  public String toString() {
    return this.a;
  }
}

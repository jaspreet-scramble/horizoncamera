package com.mla.wwl.state_machine;

import android.util.Log;
import com.mla.wwl.state_machine.Flow.EventInvocation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class WWLStateMachine {
  private HashMap<String, WWLStateMachine.a> a = new HashMap();
  private List<Flow> b = new ArrayList();
  private StateEnum c;
  private boolean d = false;

  public WWLStateMachine(StateEnum startingState) {
    this.c = startingState;
  }

  public <T> void addStateTransition(StateEnum fromState, Event<T> onEvent, StateEnum toState, EventHandler<T> eventHandler) {
    String var5 = fromState.name() + onEvent.name();
    this.a.put(var5, new WWLStateMachine.a(eventHandler, toState));
  }

  public <T> void addStateTransitions(StateEnum[] fromStates, Event<T> onEvent, StateEnum toState, EventHandler<T> eventHandler) {
    StateEnum[] var5 = fromStates;
    int var6 = fromStates.length;

    for(int var7 = 0; var7 < var6; ++var7) {
      StateEnum var8 = var5[var7];
      this.addStateTransition(var8, onEvent, toState, eventHandler);
    }

  }

  public void processEvent(Event event) {
    this.processEvent(event, (Object)null);
  }

  public <T> void processEvent(Event<T> event, T message) {
    this.a(event, message);
    this.a();
  }

  private <T> void a(Event<T> var1, T var2) {
    if(this.d) {
      throw new RuntimeException("Illegal recursion");
    } else {
      String var3 = this.c.name() + var1.name();
      WWLStateMachine.a var4 = (WWLStateMachine.a)this.a.get(var3);
      if(var4 == null) {
        Log.w("WWLStateMachine", "Event " + var1 + " can't be processed under state " + this.c);
      } else {
        this.d = true;
        StateEnum var5 = this.c;
        if(var4.a != null) {
          StateEnum var6 = var4.a.handleEvent(var5, var4.b, var2);
          boolean var7 = var6 != null;
          if(var7) {
            this.c = var6;
            var4.a.onSuccess(var5, var6, var2);
          } else {
            var4.a.onFail(var5, var4.b, var2);
          }
        } else {
          this.c = var4.b;
        }

        this.d = false;
      }
    }
  }

  public void enqueueFlow(Flow flow) {
    List var2 = flow.getStateEventCombinations();
    Iterator var3 = var2.iterator();

    while(var3.hasNext()) {
      String var4 = (String)var3.next();
      if(!this.a.containsKey(var4)) {
        Log.w("WWLStateMachine", "No state transition exists for the state-event combination " + var4 + " of flow " + flow);
      }
    }

    this.b.add(flow);
    if(this.b.size() == 1) {
      this.a();
    }

  }

  private void a() {
    StateEnum var1 = null;
    Flow var2 = null;

    while(this.b.size() != 0) {
      Flow var3 = (Flow)this.b.get(0);
      if(var3.isCompletionState(this.c)) {
        this.b.remove(0);
      } else {
        EventInvocation var4 = var3.getEventInvocation(this.c);
        if(var4 == null) {
          break;
        }

        if(var1 == this.c && var2 == var3) {
          Log.w("WWLStateMachine", "Flow " + var3 + " triggered event " + var4.a + " under state " + this.c + "that failed to change the state and wants to trigger it again. Will stop processing the flow to avoid deadlock.");
          break;
        }

        var1 = this.c;
        var2 = var3;
        this.a(var4.a, var4.b);
        if(var4.c) {
          this.b.remove(0);
        }
      }
    }

  }

  public void setCurrentState(StateEnum state) {
    this.c = state;
  }

  public StateEnum currentState() {
    return this.c;
  }

  public String currentStateName() {
    return this.c.name();
  }

  private static class a<T> {
    EventHandler<T> a;
    StateEnum b;

    a(EventHandler<T> var1, StateEnum var2) {
      this.a = var1;
      this.b = var2;
    }
  }
}

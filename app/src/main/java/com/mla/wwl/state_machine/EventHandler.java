package com.mla.wwl.state_machine;

public interface EventHandler<T> {
  StateEnum handleEvent(StateEnum var1, StateEnum var2, T var3);

  void onSuccess(StateEnum var1, StateEnum var2, T var3);

  void onFail(StateEnum var1, StateEnum var2, T var3);
}

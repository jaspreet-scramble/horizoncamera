package com.mla.wwl.state_machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Flow {
    public HashMap<String, Flow.EventInvocation> a = new HashMap();
    public List<String> b = new ArrayList();
    public List<StateEnum> c = new ArrayList();
    public String d;

    public Flow(String name) {
        this.d = name;
    }

    public <T> Flow addEventTrigger(StateEnum onState, Event<T> event, T message, boolean shouldComplete) {
        String var5 = onState.name();
        this.a.put(var5, new Flow.EventInvocation(event, message, shouldComplete));
        this.b.add(onState.name() + event.name());
        return this;
    }

    public Flow addCompletionState(StateEnum state) {
        this.c.add(state);
        return this;
    }

    public String name() {
        return this.d;
    }

    public String toString() {
        return this.d;
    }

    public <T> Flow.EventInvocation<T> getEventInvocation(StateEnum onState) {
        String var2 = onState.name();
        return (Flow.EventInvocation) this.a.get(var2);
    }

    public List<String> getStateEventCombinations() {
        return this.b;
    }

    public boolean isCompletionState(StateEnum state) {
        return this.c.contains(state);
    }

    public static class EventInvocation<T> {
        Event<T> a;
        T b;
        boolean c;

        EventInvocation(Event<T> event, T message, boolean isFinalEvent) {
            this.a = event;
            this.b = message;
            this.c = isFinalEvent;
        }
    }
}

package com.mla.wwl;

import android.graphics.Rect;

interface b {
  void requestZoom(int var1);

  void requestTapToFocus(Rect var1, boolean var2);

  void requestDefaultFocusMode();
}

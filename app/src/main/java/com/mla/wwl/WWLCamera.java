package com.mla.wwl;

import android.content.Context;
import android.hardware.Camera.Parameters;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.mla.wwl.CameraEncoder.WWLCameraParameters;
import com.mla.wwl.WWLVars.CameraMode;
import com.mla.wwl.WWLVars.WWLLevelerCropMode;
import com.mla.wwl.WWLVars.WWLLevelerFlexSpeed;
import com.mla.wwl.WWLVars.WWLLevelerLockedOrientation;
import com.mla.wwl.Utils.Utils;
import com.mla.wwl.av.AndroidMuxer;
import com.mla.wwl.av.Muxer.FORMAT;
import com.mla.wwl.events.FrameDroppedEvent;
import com.mla.wwl.events.MuxerFinishedEvent;
import com.mla.wwl.events.MuxerStartedAllTracksEvent;
import com.mla.wwl.events.PhotoCapturedEvent;
import com.mla.wwl.events.PreviewFailedToStartEvent;
import com.mla.wwl.events.PreviewHasBeenRunningEvent;
import com.mla.wwl.events.PreviewStartedEvent;
import com.mla.wwl.events.PreviewStoppedEvent;
import com.mla.wwl.events.PreviewWillStopEvent;
import com.mla.wwl.events.SensorNotRespondingEvent;
import com.mla.wwl.events.SensorRespondedEvent;
import com.mla.wwl.events.SnapshotCapturedEvent;
import com.mla.wwl.state_machine.Event;
import com.mla.wwl.state_machine.Flow;
import com.mla.wwl.state_machine.WWLStateMachine;
import com.mla.wwl.state_machine.SimpleEventHandler;
import com.mla.wwl.state_machine.StateEnum;

import java.io.File;
import java.util.Arrays;

import static com.mla.wwl.WWLCamera.c.HOT_SWAPPING_CAMERA;
import static com.mla.wwl.WWLCamera.c.INITIALIZED;
import static com.mla.wwl.WWLCamera.c.PHOTO_CAPTURING;
import static com.mla.wwl.WWLCamera.c.RECORDING;
import static com.mla.wwl.WWLCamera.c.RUNNING_IDLE;
import static com.mla.wwl.WWLCamera.c.STARTING;
import static com.mla.wwl.WWLCamera.c.STARTING_RECORDING;
import static com.mla.wwl.WWLCamera.c.STOPPING;
import static com.mla.wwl.WWLCamera.c.STOPPING_RECORDING;
import static com.mla.wwl.WWLCamera.c.UNINITIALIZED;

public final class WWLCamera {
    private final Flow a = new Flow("START_RUNNING");
    private final Flow b = new Flow("STOP_RUNNING");
    private final Flow c = new Flow("DESTROY");
    private final Event d = new Event("SHOULD_START_RUNNING");
    private final Event e = new Event("SHOULD_STOP_RUNNING");
    private final Event f = new Event("SHOULD_RELEASE");
    private final Event<WWLCameraParameters> g = new Event("SHOULD_CHANGE_CAMERA");
    private final Event<WWLCamera.b> h = new Event("SHOULD_START_RECORDING");
    private final Event i = new Event("SHOULD_STOP_RECORDING");
    private final Event<File> j = new Event("SHOULD_CAPTURE_PHOTO");
    private final Event<PreviewStartedEvent> k = new Event("ON_PREVIEW_STARTED");
    private final Event l = new Event("ON_PREVIEW_FAILED_TO_START");
    private final Event m = new Event("ON_PREVIEW_STOPPED");
    private final Event n = new Event("ON_MUXER_STARTED");
    private final Event<File> o = new Event("ON_MUXER_FINISHED");
    private final Event<PhotoCapturedEvent> p = new Event("ON_PHOTO_CAPTURED");
    private CameraEncoder q;
    private MicrophoneEncoder r;
    private FrameParametersManager s;
    private EventBus t;
    private WWLCamera.a u;
    private WWLStateMachine v;
    private WWLCameraListener w = null;
    private WWLCameraPrivateListener x = null;
    private WWLCameraListener y = new WWLCameraListener() {
        public void onFailedToStart() {
            if (WWLCamera.this.w != null) {
                WWLCamera.this.w.onFailedToStart();
            }

        }

        public void onStartedRunning(Parameters params, int facing) {
            if (WWLCamera.this.w != null) {
                WWLCamera.this.w.onStartedRunning(params, facing);
            }

        }

        public void onPreviewHasBeenRunning(final Parameters params, final int facing) {
            WWLCamera.this.u.post(new Runnable() {
                public void run() {
                    if (WWLCamera.this.w != null) {
                        WWLCamera.this.w.onPreviewHasBeenRunning(params, facing);
                    }

                }
            });
        }

        public void onWillStopRunning() {
            WWLCamera.this.u.post(new Runnable() {
                public void run() {
                    if (WWLCamera.this.w != null) {
                        WWLCamera.this.w.onWillStopRunning();
                    }

                }
            });
        }

        public void onStoppedRunning() {
            if (WWLCamera.this.w != null) {
                WWLCamera.this.w.onStoppedRunning();
            }

        }

        public void onRecordingHasStarted() {
            if (WWLCamera.this.w != null) {
                WWLCamera.this.w.onRecordingHasStarted();
            }

        }

        public void onRecordingWillStop() {
            if (WWLCamera.this.w != null) {
                WWLCamera.this.w.onRecordingWillStop();
            }

        }

        public void onRecordingFinished(File file, boolean success) {
            if (WWLCamera.this.w != null) {
                WWLCamera.this.w.onRecordingFinished(file, success);
            }

        }

        public void onPhotoCaptured(final File file, final boolean success) {
            WWLCamera.this.u.post(new Runnable() {
                public void run() {
                    if (WWLCamera.this.w != null) {
                        WWLCamera.this.w.onPhotoCaptured(file, success);
                    }

                }
            });
        }

        public void onSnapshotCaptured(File file) {
            if (WWLCamera.this.w != null) {
                WWLCamera.this.w.onSnapshotCaptured(file);
            }

        }

        public void onAngleUpdated(float rotation, float scale) {
            if (WWLCamera.this.w != null) {
                WWLCamera.this.w.onAngleUpdated(rotation, scale);
            }

        }

        public void onSensorNotResponding() {
            WWLCamera.this.u.post(new Runnable() {
                public void run() {
                    if (WWLCamera.this.w != null) {
                        WWLCamera.this.w.onSensorNotResponding();
                    }

                }
            });
        }

        public void onSensorResponded() {
            WWLCamera.this.u.post(new Runnable() {
                public void run() {
                    if (WWLCamera.this.w != null) {
                        WWLCamera.this.w.onSensorResponded();
                    }

                }
            });
        }
    };

    static enum c implements StateEnum {
        UNINITIALIZED,
        INITIALIZED,
        STARTING,
        RUNNING_IDLE,
        STOPPING,
        HOT_SWAPPING_CAMERA,
        STARTING_RECORDING,
        RECORDING,
        STOPPING_RECORDING,
        PHOTO_CAPTURING;

        private c() {
        }
    }

    private WWLCamera.c a() {
        return (WWLCamera.c) this.v.currentState();
    }

    public WWLCamera(Context context) {
        if (!MlaSDK.initialized()) {
            throw new RuntimeException("MlaSDK was not initialized");
        } else {
            this.v = new WWLStateMachine(INITIALIZED);
            SimpleEventHandler var2 = new SimpleEventHandler() {
                public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                    WWLCamera.this.q.release();
                    WWLCamera.this.q = null;
                    WWLCamera.this.s.release();
                    WWLCamera.this.s = null;
                    WWLCamera.this.t.unregister(this);
                    WWLCamera.this.w = null;
                    WWLCamera.this.x = null;
                    Log.i("WWLCamera", "Release completed");
                }
            };
            this.v.addStateTransition(INITIALIZED, this.f, UNINITIALIZED, var2);
            this.v.addStateTransition(INITIALIZED, this.d, STARTING, new SimpleEventHandler() {
                public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                    WWLCamera.this.q.startRunning();
                    WWLCamera.this.s.startRunning();
                }
            });
            this.v.addStateTransition(STARTING, this.k, RUNNING_IDLE, new SimpleEventHandler<PreviewStartedEvent>() {
                public void onSuccess(StateEnum var1, StateEnum var2, PreviewStartedEvent var3) {
                    WWLCamera.this.y.onStartedRunning(var3.params, var3.facing);
                }
            });
            this.v.addStateTransition(STARTING, this.l, INITIALIZED, new SimpleEventHandler() {
                public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                    WWLCamera.this.s.stopRunning();
                    WWLCamera.this.y.onFailedToStart();
                }
            });
            this.v.addStateTransition(RUNNING_IDLE, this.e, STOPPING, new SimpleEventHandler() {
                public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                    WWLCamera.this.q.stopRunning();
                    WWLCamera.this.s.stopRunning();
                }
            });
            this.v.addStateTransition(STOPPING, this.m, INITIALIZED, new SimpleEventHandler() {
                public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                    WWLCamera.this.y.onStoppedRunning();
                }
            });
            this.v.addStateTransition(INITIALIZED, this.g, INITIALIZED, new SimpleEventHandler<WWLCameraParameters>() {
                public void onSuccess(StateEnum var1, StateEnum var2, WWLCameraParameters var3) {
                    WWLCamera.this.q.setCamera(var3);
                }
            });
            this.v.addStateTransition(RUNNING_IDLE, this.g, HOT_SWAPPING_CAMERA, new SimpleEventHandler<WWLCameraParameters>() {
                public void onSuccess(StateEnum var1, StateEnum var2, WWLCameraParameters var3) {
                    WWLCamera.this.q.setCamera(var3);
                }
            });
            this.v.addStateTransition(HOT_SWAPPING_CAMERA, this.m, STARTING, new SimpleEventHandler() {
                public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                    WWLCamera.this.y.onStoppedRunning();
                }
            });
            this.v.addStateTransition(RUNNING_IDLE, this.h, STARTING_RECORDING, new SimpleEventHandler<WWLCamera.b>() {
                public void onSuccess(StateEnum var1, StateEnum var2, WWLCamera.b var3) {
                    File var4 = var3.a;
                    WWLCamcorderProfile var5 = var3.b;
                    if (var5 == null) {
                        Size var6 = WWLCamera.this.q.getOutputMovieSize();
                        var5 = new WWLCamcorderProfile(var6.getWidth(), var6.getHeight());
                    }

                    AndroidMuxer var7 = AndroidMuxer.create(var4.getAbsolutePath(), FORMAT.MPEG4);
                    var7.setEventBus(WWLCamera.this.t);
                    WWLCamera.this.q.prepareRecording(var7, var5);
                    WWLCamera.this.r.prepareRecording(var7, var5);
                    WWLCamera.this.r.startRecording();
                    WWLCamera.this.q.startRecording();
                }
            });
            this.v.addStateTransition(STARTING_RECORDING, this.n, RECORDING, new SimpleEventHandler() {
                public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                    WWLCamera.this.y.onRecordingHasStarted();
                }
            });
            this.v.addStateTransition(RECORDING, this.i, STOPPING_RECORDING, new SimpleEventHandler() {
                public void onSuccess(StateEnum fromState, StateEnum toState, Object message) {
                    WWLCamera.this.r.stopRecording();
                    WWLCamera.this.q.stopRecording();
                    WWLCamera.this.y.onRecordingWillStop();
                }
            });
            this.v.addStateTransition(STOPPING_RECORDING, this.o, RUNNING_IDLE, new SimpleEventHandler<File>() {
                public void onSuccess(StateEnum var1, StateEnum var2, File var3) {
                    WWLCamera.this.y.onRecordingFinished(var3, true);
                }
            });
            this.v.addStateTransition(RUNNING_IDLE, this.j, PHOTO_CAPTURING, new SimpleEventHandler<File>() {
                public void onSuccess(StateEnum var1, StateEnum var2, File var3) {
                    WWLCamera.this.q.capturePhoto(var3);
                }
            });
            this.v.addStateTransition(PHOTO_CAPTURING, this.p, RUNNING_IDLE, new SimpleEventHandler<PhotoCapturedEvent>() {
                public void onSuccess(StateEnum var1, StateEnum var2, PhotoCapturedEvent var3) {
                    WWLCamera.this.y.onPhotoCaptured(var3.file, var3.success);
                }
            });
            this.a.addEventTrigger(INITIALIZED, this.d, (Object) null, true);
            this.b.addEventTrigger(RUNNING_IDLE, this.e, (Object) null, true).addEventTrigger(RECORDING, this.i, (Object) null, false).addCompletionState(INITIALIZED);
            this.c.addEventTrigger(INITIALIZED, this.f, (Object) null, true).addEventTrigger(RUNNING_IDLE, this.e, (Object) null, false).addEventTrigger(RECORDING, this.i, (Object) null, false);
            this.u = new WWLCamera.a(this);
            this.t = new EventBus("WWLCamera Bus");
            this.t.register(this);
            this.s = new FrameParametersManager(context, this, this.t);
            this.q = new CameraEncoder(context, this.t, this.s, this);
            this.r = new MicrophoneEncoder();
        }
    }

    public void setListener(WWLCameraListener listener) {
        this.w = listener;
    }

    public void setPrivateListener(WWLCameraPrivateListener listner) {
        this.x = listner;
    }

    public WWLCameraListener getListener() {
        return this.w;
    }

    public void setScreenRotation(int rotation) {
        int var2 = Utils.degreesFromRotation(rotation);
        this.q.setScreenAngle(var2);
    }

    public void startRunning() {
        this.v.enqueueFlow(this.a);
    }

    public void stopRunning() {
        this.v.enqueueFlow(this.b);
    }

    public void destroy() {
        this.v.enqueueFlow(this.c);
    }

    public boolean isRunning() {
        WWLCamera.c[] var1 = new WWLCamera.c[]{UNINITIALIZED, INITIALIZED, STARTING, STOPPING};
        return !Arrays.asList(var1).contains(this.a());
    }

    public boolean isRunningIdle() {
        return this.a().equals(RUNNING_IDLE);
    }

    public boolean isStopped() {
        return this.a().equals(INITIALIZED);
    }

    public void attachPreviewView(WWLView view) {
        this.q.attachPreviewView(view);
    }

    public void detachPreviewView(WWLView view) {
        this.q.detachPreviewView(view);
    }

    public void startRecording(File outputFile, WWLCamcorderProfile profile) {
        WWLCamera.b var3 = new WWLCamera.b(null);
        var3.a = outputFile;
        var3.b = profile;
        this.v.processEvent(this.h, var3);
    }

    public void stopRecording() {
        this.v.processEvent(this.i);
    }

    public boolean isRecording() {
        WWLCamera.c[] var1 = new WWLCamera.c[]{STARTING_RECORDING, RECORDING, STOPPING_RECORDING};
        return Arrays.asList(var1).contains(this.a());
    }

    public boolean isStartingRecording() {
        return this.a().equals(STARTING_RECORDING);
    }

    public boolean isStoppingRecording() {
        return this.a().equals(STOPPING_RECORDING);
    }

    public void capturePhoto(File photoFile) {
        this.v.processEvent(this.j, photoFile);
    }

    public void captureSnapshot(File photoFile) {
        if (this.a().equals(RECORDING)) {
            this.q.captureSnapshot(photoFile);
        }

    }

    public void setCamera(int facing, Size videoSize, Size pictureSize) {
        WWLCameraParameters var4 = new WWLCameraParameters(facing, videoSize, pictureSize);
        this.v.processEvent(this.g, var4);
    }

    public int getCameraFacing() {
        return this.q.getCameraFacing();
    }

    public Size getCameraVideoSize() {
        return this.q.getCameraVideoSize();
    }

    public Size getCameraPictureSize() {
        return this.q.getCameraPictureSize();
    }

    public void setOutputMovieSize(Size frameSize) {
        if (!this.isRecording() && !this.a().equals(PHOTO_CAPTURING)) {
            this.q.setOutputMovieSize(frameSize);
        }

    }

    public Size getOutputMovieSize() {
        return this.q.getOutputMovieSize();
    }

    public void setCameraMode(CameraMode mode) {
        this.q.setCameraMode(mode);
    }

    public CameraMode getCameraMode() {
        return this.q.getCameraMode();
    }

    public void setCompatibilityMode(boolean enabled) {
        this.q.setCompatibilityMode(enabled);
    }

    public boolean compatibilityModeEnabled() {
        return this.q.getCompatibilityMode();
    }

    public void setFlashMode(String flashMode) {
        this.q.setDesiredFlashMode(flashMode);
    }

    public String getFlashMode() {
        return this.q.getDesiredFlashMode();
    }

    public void setSensor(int sensorType, File calibrationFile) {
        this.s.setSensor(sensorType, calibrationFile);
    }

    public void setLevelerCropMode(WWLLevelerCropMode cropMode) {
        this.s.setCropMode(cropMode);
    }

    public WWLLevelerCropMode getLevelerCropMode() {
        return this.s.getCropMode();
    }

    public void setLevelerFlexSpeed(WWLLevelerFlexSpeed flexSpeed) {
        this.s.setFlexSpeed(flexSpeed);
    }

    public WWLLevelerFlexSpeed getLevelerFlexSpeed() {
        return this.s.getFlexSpeed();
    }

    public void setLevelerLockedOrientation(WWLLevelerLockedOrientation orientation) {
        this.s.setLockedOrientation(orientation);
    }

    public WWLLevelerLockedOrientation getLevelerLockedOrientation() {
        return this.s.getLockedOrientation();
    }

    public void setWatermarkEnabled(boolean enabled) {
        this.q.setWatermarkEnabled(enabled);
    }

    private void b() {
        if (this.x != null) {
            this.x.onFrameDropped();
        }

    }

    @Subscribe
    public void onDeadEvent(DeadEvent e) {
    }

    @Subscribe
    public void onPreviewStarted(final PreviewStartedEvent e) {
        this.u.post(new Runnable() {
            public void run() {
                WWLCamera.this.v.processEvent(WWLCamera.this.k, e);
            }
        });
    }

    @Subscribe
    public void onPreviewHasBeenRunning(PreviewHasBeenRunningEvent e) {
        this.y.onPreviewHasBeenRunning(e.params, e.facing);
    }

    @Subscribe
    public void onPreviewFailedToStart(PreviewFailedToStartEvent e) {
        this.u.post(new Runnable() {
            public void run() {
                WWLCamera.this.v.processEvent(WWLCamera.this.l);
            }
        });
    }

    @Subscribe
    public void onPreviewWillStop(PreviewWillStopEvent e) {
        this.y.onWillStopRunning();
    }

    @Subscribe
    public void onPreviewStoppedEvent(PreviewStoppedEvent e) {
        this.u.post(new Runnable() {
            public void run() {
                WWLCamera.this.v.processEvent(WWLCamera.this.m);
            }
        });
    }

    @Subscribe
    public void onMuxerStartedAllTracks(MuxerStartedAllTracksEvent e) {
        this.u.post(new Runnable() {
            public void run() {
                WWLCamera.this.v.processEvent(WWLCamera.this.n);
            }
        });
    }

    @Subscribe
    public void onMuxerFinished(final MuxerFinishedEvent e) {
        this.u.post(new Runnable() {
            public void run() {
                WWLCamera.this.v.processEvent(WWLCamera.this.o, e.file);
            }
        });
    }

    @Subscribe
    public void onPhotoCaptured(final PhotoCapturedEvent e) {
        this.u.post(new Runnable() {
            public void run() {
                WWLCamera.this.v.processEvent(WWLCamera.this.p, e);
            }
        });
    }

    @Subscribe
    public void onSnapshotCaptured(SnapshotCapturedEvent e) {
        this.y.onSnapshotCaptured(e.file);
    }

    public void onAngleUpdated(float angle, float scale) {
        float[] var3 = new float[]{angle, scale};
        this.u.obtainMessage(1, var3).sendToTarget();
    }

    @Subscribe
    public void onSensorResponded(SensorRespondedEvent e) {
        this.y.onSensorResponded();
    }

    @Subscribe
    public void onSensorNotResponding(SensorNotRespondingEvent e) {
        this.y.onSensorNotResponding();
    }

    @Subscribe
    public void onFrameDropped(FrameDroppedEvent e) {
        this.u.obtainMessage(2).sendToTarget();
    }

    /* renamed from: com.mla.wwl.WWLCamera$a */
    private class a extends Handler {
        /* renamed from: a */
        final /* synthetic */ WWLCamera a;

        private a(WWLCamera wWLCamera) {
            this.a = wWLCamera;
        }

        public void handleMessage(Message msg) {
            int i = msg.what;
            switch (i) {
                case 1:
                    float[] fArr = (float[]) msg.obj;
                    this.a.y.onAngleUpdated(fArr[0], fArr[1]);
                    return;
                case 2:
                    this.a.b();
                    return;
                default:
                    throw new RuntimeException("Unexpected msg what=" + i);
            }
        }
    }

    private static class b {
        File a;
        WWLCamcorderProfile b;

        private b(WWLCamcorderProfile camcorderProfile) {
            this.b = camcorderProfile;
        }
    }

}

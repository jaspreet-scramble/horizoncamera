package com.mla.wwl;

import android.os.Looper;
import com.mla.wwl.gles.EglCore;

interface c {
  void onRendererWillBeAttached(d var1, Looper var2);

  void onRendererDetached();

  void prepareGL(EglCore var1);

  void destroyGL();

  boolean isSurfaceReady();

  boolean isRendererReady();

  void setSourceSize(int var1, int var2, boolean var3);

  void setOutputSize(int var1, int var2);

  void setCameraToScreenAngle(int var1);

  void handleFrameAvailable(int var1, long var2, float[] var4, float[] var5);
}
